#pragma once

/* Scattering selection */

enum { escatterXRAY, escatterNEUTRON, escatterNR };

enum { ewaxsPotentialLINEAR, ewaxsPotentialLOG, ewaxsPotentialNR };

enum { ewaxsIexpFit_NO, ewaxsIexpFit_SCALE_AND_OFFSET, ewaxsIexpFit_SCALE, ewaxsIexpFitNR };

namespace swaxs {

enum class EnsembleType {
    None,
    BayesianOneRefined,
    MaxEnt,
    Count
};

}

enum {
    ewaxsWeightsUNIFORM,
    ewaxsWeightsEXPERIMENT,
    ewaxsWeightsEXP_plus_CALC,
    ewaxsWeightsEXP_plus_SOLVDENS,
    ewaxsWeightsEXP_plus_CALC_plus_SOLVDENS,
    ewaxsWeightsNR
};

enum {
    ewaxsSolvdensUncertBayesian_NO,
    ewaxsSolvdensUncertBayesian_YES,
    ewaxsSolvdensUncertBayesianNR
};

/* Scaling I(q=0) to target */
enum { ewaxsscalei0NO, ewaxsscalei0YES, ewaxsscalei0NR };

/* Adding back oversubtracted buffer */
enum { waxscorrectbuffNO, waxscorrectbuffYES, waxscorrectbuffNR };
