#include "./sim_util.h"

#include <cstring>  // memcpy

#include <gromacs/domdec/collect.h>  // dd_collect_vec
#include <gromacs/domdec/domdec_struct.h>  // gmx_domdec_t
#include <gromacs/domdec/ga2la.h>  // gmx_ga2la_t
#include <gromacs/mdlib/stat.h>  // do_per_step
#include <gromacs/mdtypes/mdatom.h>  // t_mdatoms
#include <gromacs/timing/wallcycle.h>  // gmx_wallcycle_t
#include <gromacs/utility/arrayref.h>  // ArrayRef
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/math/vec.h>  // rvec_inc

#include <waxs/gmxlib/waxsmd.h>  // do_waxs_md_low
#include <waxs/types/enums.h>  // escatter*
#include <waxs/types/waxsrec.h>  // t_waxsrec

void do_waxs_md(const t_commrec* cr, const gmx_multisim_t* ms, const t_mdatoms* mdatoms, gmx::ArrayRef<gmx::RVec> xlocal,
        double simtime, int64_t step, t_forcerec* fr, const gmx_mtop_t* mtop, const matrix box, rvec f_novirsum[],
        real* enerXray, real* enerNeutron) {
    t_waxsrec* wr = fr->waxsrec;

    /** Updating WAXS potential and forces and store in vLast / fLast */
    if (do_per_step(step, wr->inputParams->nstcalc)) {
        /* Get all coordinates into Master node - not elegant for now */
        if (cr->nnodes == 1) {
            memcpy(as_rvec_array(wr->x.data()), as_rvec_array(xlocal.data()), mtop->natoms * sizeof(rvec));
        } else if (DOMAINDECOMP(cr)) {
            dd_collect_vec(cr->dd, wr->local_state->ddp_count, wr->local_state->ddp_count_cg_gl, wr->local_state->cg_gl,
                    xlocal, wr->x);
        } else {
            gmx_fatal(FARGS, "WAXS MD is only supported with domain decomposition (at the moment)\n");
        }

        do_waxs_md_low(cr, ms, wr->x, simtime, step, wr, mtop, box, fr->pbcType, do_per_step(step, wr->inputParams->nstlog));
    }

    /** Collect Xray and Neutron-derived potential */
    *enerXray = 0;
    *enerNeutron = 0;

    if (wr->inputParams->bCalcPot) {
        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            if (wr->wt[t].type == escatterXRAY) {
                *enerXray += wr->wt[t].vLast;
            } else if (wr->wt[t].type == escatterNEUTRON) {
                *enerNeutron += wr->wt[t].vLast;
            } else {
                gmx_fatal(FARGS, "Unknown scatter type %d\n", wr->wt[t].type);
            }
        }
    }

    if (wr->inputParams->bCalcForces) {
        for (int p = 0; p < wr->nindA_prot; p++) {
            int ii = wr->indA_prot[p];

            if (cr->dd) {
                const int* local_atom_index = cr->dd->ga2la->findHome(ii);
                ii = local_atom_index ? *local_atom_index : -1;
            }

            /* With DomDec, start=0 and end=nr of atoms on this node */
            // Previously `if (ii >= mdatoms->start and ii < (mdatoms->start + mdatoms->homenr))`
            // `->start` was removed in Gromacs 5.0, analysing the code, 0 was used anyway in the other places.
            // TODO: Make sure it is fine.
            if (ii >= 0 and ii < mdatoms->homenr) {
                /* ii is now the local index for f[] array */
                /* fprintf(stderr,"Force on %d = %10g / %10g / %10g  (before %10g / %10g / %10g)\n",p,
                        wr->fLast[p][0], wr->fLast[p][1], wr->fLast[p][2],
                        f_novirsum[ii][0], f_novirsum[ii][1], f_novirsum[ii][2]); */
                rvec_inc(f_novirsum[ii], wr->fLast[p]);
            }
        }
    }
}
