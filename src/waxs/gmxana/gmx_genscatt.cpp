#include <cctype>  // toupper, tolower, isalpha
#include <cmath>  // fabs, round
#include <cstdlib>  // exit
#include <cstring>  // strcpy, strlen, strcpy
#include <ctime>  // time_t

#include <gromacs/gmxana/gmx_ana.h>  // Required to register this file as a CLI tool

#include <gromacs/commandline/filenm.h>  // ftp2fn, fn2ftp, opt2bSet, opt2fn
#include <gromacs/commandline/pargs.h>  // etBOOL, etINT, etREAL, parse_common_args, t_pargs, PCA_CAN_VIEW
#include <gromacs/fileio/confio.h>  // read_tps_conf, t_inputrec
#include <gromacs/fileio/oenv.h>  // gmx_output_env_t
#include <gromacs/fileio/tpxio.h>  // read_tps_conf, read_tpx_state
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/mdtypes/state.h>  // t_state
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/topology/ifunc.h>  // interaction_function, IS_CHEMBOND
#include <gromacs/topology/index.h>  // get_index
#include <gromacs/topology/topology.h>  // t_atoms, gmx_mtop_global_atoms
#include <gromacs/utility/arraysize.h>  // asize
#include <gromacs/utility/cstringutil.h>  // gmx_strcasecmp, gmx_strcasecmp, STRLEN
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/futil.h>  // gmx_ffopen, gmx_ffclose
#include <gromacs/utility/smalloc.h>  // snew, srenew, sfree
#include <gromacs/utility/sysinfo.h>  // gmx_ctime_r

typedef char ElementName[20];

const struct {
    char element_name[3];
    real element_mass;
} elements_masses[] = { { "H", 1.0079 }, { "He", 4.0026 }, { "Li", 6.941 }, { "Be", 9.0122 }, { "B", 10.811 },
    { "C", 12.0107 }, { "N", 14.0067 }, { "O", 15.9994 }, { "F", 18.9984 }, { "Ne", 20.1797 }, { "Na", 22.9897 },
    { "Mg", 24.305 }, { "Al", 26.9815 }, { "Si", 28.0855 }, { "P", 30.9738 }, { "S", 32.065 }, { "Cl", 35.453 },
    { "Ar", 39.948 }, { "K", 39.0983 }, { "Ca", 40.078 }, { "Sc", 44.9559 }, { "Ti", 47.867 }, { "V", 50.9415 },
    { "Cr", 51.9961 }, { "Mn", 54.938 }, { "Fe", 55.845 }, { "Co", 58.9332 }, { "Ni", 58.6934 }, { "Cu", 63.546 },
    { "Zn", 65.39 }, { "Ga", 69.723 }, { "Ge", 72.64 }, { "As", 74.9216 }, { "Se", 78.96 }, { "Br", 79.904 },
    { "Kr", 83.8 }, { "Rb", 85.4678 }, { "Sr", 87.62 }, { "Y", 88.9059 }, { "Zr", 91.224 }, { "Nb", 92.9064 },
    { "Mo", 95.94 }, { "Tc", 98 }, { "Ru", 101.07 }, { "Rh", 102.906 }, { "Pd", 106.42 }, { "Ag", 107.868 },
    { "Cd", 112.411 }, { "In", 114.818 }, { "Sn", 118.71 }, { "Sb", 121.76 }, { "Te", 127.6 }, { "I", 126.904 },
    { "Xe", 131.293 }, { "Cs", 132.905 }, { "Ba", 137.327 }, { "La", 138.905 }, { "Ce", 140.116 }, { "Pr", 140.908 },
    { "Nd", 144.24 }, { "Pm", 145 }, { "Sm", 150.36 }, { "Eu", 151.964 }, { "Gd", 157.25 }, { "Tb", 158.925 },
    { "Dy", 162.5 }, { "Ho", 164.93 }, { "Er", 167.259 }, { "Tm", 168.934 }, { "Yb", 173.04 }, { "Lu", 174.967 },
    { "Hf", 178.49 }, { "Ta", 180.948 }, { "W", 183.84 }, { "Re", 186.207 }, { "Os", 190.23 }, { "Ir", 192.217 },
    { "Pt", 195.078 }, { "Au", 196.966 }, { "Hg", 200.59 }, { "Tl", 204.383 }, { "Pb", 207.2 }, { "Bi", 208.98 },
    { "Po", 209 }, { "At", 210 }, { "Rn", 222 }, { "Fr", 223 }, { "Ra", 226 }, { "Ac", 227 }, { "Th", 232.038 },
    { "Pa", 231.036 }, { "U", 238.029 }, { "Np", 237 }, { "Pu", 244 }, { "Am", 243 }, { "Cm", 247 }, { "Bk", 247 },
    { "Cf", 251 }, { "Es", 252 }, { "Fm", 257 }, { "Md", 258 }, { "No", 259 }, { "Lr", 262 }, { "Rf", 261 },
    { "Db", 262 }, { "Sg", 266 }, { "Bh", 264 }, { "Hs", 277 }, { "Mt", 268 } };

const char* protein_residues_names[] = { "ABU", "ACE", "AIB", "ALA", "ARG", "ARGN", "ASN", "ASN1", "ASP", "ASP1", "ASPH", "ASH",
    "CT3", "CYS", "CYS1", "CYS2", "CYSH", "DALA", "GLN", "GLU", "GLUH", "GLH", "GLY", "HIS", "HIS1", "HISA", "HISB",
    "HISH", "HISD", "HISE", "HISP", "HSD", "HSE", "HSP", "HYP", "ILE", "LEU", "LSN", "LYS", "LYSH", "MELEU", "MET",
    "MEVAL", "NAC", "NME", "NHE", "NH2", "PHE", "PHEH", "PHEU", "PHL", "PRO", "SER", "THR", "TRP", "TRPH", "TRPU",
    "TYR", "TYRH", "TYRU", "VAL", "PGLU", "HID", "HIE", "HIP", "LYP", "LYN", "CYN", "CYM", "CYX", "DAB", "ORN", "HYP",
    "NALA", "NGLY", "NSER", "NTHR", "NLEU", "NILE", "NVAL", "NASN", "NGLN", "NARG", "NHID", "NHIE", "NHIP", "NHISD",
    "NHISE", "NHISH", "NTRP", "NPHE", "NTYR", "NGLU", "NASP", "NLYS", "NORN", "NDAB", "NLYSN", "NPRO", "NHYP", "NCYS",
    "NCYS2", "NMET", "NASPH", "NGLUH", "CALA", "CGLY", "CSER", "CTHR", "CLEU", "CILE", "CVAL", "CASN", "CGLN", "CARG",
    "CHID", "CHIE", "CHIP", "CHISD", "CHISE", "CHISH", "CTRP", "CPHE", "CTYR", "CGLU", "CASP", "CLYS", "CORN", "CDAB",
    "CLYSN", "CPRO", "CHYP", "CCYS", "CCYS2", "CMET", "CASPH", "CGLUH" };


/* Check whether a string is in a list of strings */
static bool is_text_in_list(const char* string, const char* list[], int size) {
    for (int i = 0; i < size; i++) {
        if (!gmx_strcasecmp(list[i], string)) {
            return true;
        }
    }

    return false;
}

/** Check if the heavy atom to which this is bound is a backbone nitrogen */
static bool is_backbone_hydrogen(int hydrogen_index, int heavy_atom_index, t_atoms* atoms) {
    bool is_protein = false;

    for (int i = 0; i < asize(protein_residues_names); i++) {
        if (!gmx_strcasecmp(protein_residues_names[i], *(atoms->resinfo[atoms->atom[hydrogen_index].resind].name))) {
            is_protein = true;
            break;
        }
    }

    bool is_nitrogen = gmx_strcasecmp(*(atoms->atomname[heavy_atom_index]), "N") == 0;
    bool is_hydrogen = gmx_strcasecmp(*(atoms->atomname[hydrogen_index]), "H") == 0 or gmx_strcasecmp(*(atoms->atomname[hydrogen_index]), "HN") == 0;

    return is_protein and is_nitrogen and is_hydrogen;
}


/** Check if this is a polar hydrogen since it is bound to O, N, S, or P */
static bool is_polarizing_hydrogen(const char* boundToElem) {
    const char* polarizing_hydrogen_names[] = { "O", "N", "S", "P" };

    for (int i = 0; i < asize(polarizing_hydrogen_names); i++) {
        if (!gmx_strcasecmp(polarizing_hydrogen_names[i], boundToElem)) {
            return true;
        }
    }

    return false;
}

/* Check if non-protonated Glu or Asp */
static bool isCarboxylateOminus(const char* atom_name, const char* residue_name) {
    const char* glutamic_acid_residue_names[] = { "GLU", "NGLU", "CGLU" };
    const char* aspartic_acid_residue_names[] = { "ASP", "NASP", "CASP" };

    if (is_text_in_list(residue_name, glutamic_acid_residue_names, asize(glutamic_acid_residue_names))) {
        if (!strcmp(atom_name, "OE1") or !strcmp(atom_name, "OE2")) {
            return true;
        }
    }

    if (is_text_in_list(residue_name, aspartic_acid_residue_names, asize(aspartic_acid_residue_names))) {
        if (!strcmp(atom_name, "OD1") or !strcmp(atom_name, "OD2")) {
            return true;
        }
    }

    return false;
}

static bool is_protonated_lysine_N_plus(const char* atom_name, const char* residue_name) {
    const char* lysine_residue_names[] = { "LYS", "NLYS", "CLYS" };

    if (is_text_in_list(residue_name, lysine_residue_names, asize(lysine_residue_names))) {
        if (!strcmp(atom_name, "NZ")) {
            return true;
        }
    }

    return false;
}

static bool is_protonated_arginine_C_plus(const char* atom_name, const char* residue_name) {
    const char* arginine_residue_names[] = { "ARG", "NARG", "CARG" };

    if (is_text_in_list(residue_name, arginine_residue_names, asize(arginine_residue_names))) {
        if (strcmp(atom_name, "CZ") == 0) {
            return true;
        }
    }

    return false;
}

/* Check phophorous of DNA or RNA backbone */
static bool isNucleicAcidPhosphate(const char* atom_name, const char* residue_name) {
    const char* dna_residue_names[] = { "DA5", "DA", "DA3", "DAN", "DT5", "DT", "DT3", "DTN", "DG5", "DG", "DG3", "DGN", "DC5", "DC", "DC3", "DCN" };
    const char* rna_residue_names[] = { "RA5", "RA", "RA3", "RAN", "RU5", "RU", "RU3", "RUN", "RG5", "RG", "RG3", "RGN", "RC5", "RC", "RC3", "RCN", "A", "G", "U", "C" };

    if ((strcmp(atom_name, "P") == 0)
            and (is_text_in_list(residue_name, dna_residue_names, asize(dna_residue_names))
                    or is_text_in_list(residue_name, rna_residue_names, asize(rna_residue_names)))) {
        return true;
    }

    return false;
}

static bool isCTerminusCarboxylateOxygen(const char* atom_name) {
    const char* CTermiinusOxygenNames[] = { "OC1", "OC2", "OT1", "OT2", "O1", "O2" };

    return is_text_in_list(atom_name, CTermiinusOxygenNames, asize(CTermiinusOxygenNames));
}

/**
 * Given a one- or two-character string, such as O, HE, or so,
 * return the element such as O, He, or so, and the mass.
 */
static int search_elements(const char* atom, real* mReturn, char* elem) {
    char el[3];
    strcpy(el, atom);

    el[0] = toupper(el[0]);

    if (strlen(el) == 2) {
        el[1] = tolower(el[1]);
    }

    int size = asize(elements_masses);

    for (int i = 0; i < size; i++) {
        if (!strcmp(el, elements_masses[i].element_name)) {
            strcpy(elem, elements_masses[i].element_name);
            *mReturn = elements_masses[i].element_mass;

            return 0;
        }
    }

    return 1;
}


#define ELEMENT_ALLOW_REL_MASS_DIFF 0.05


struct GenscattOptions {
    real max_mass_diff = 0.3;
    bool bGromos = false;
    bool expect_virtual_sites = false;
    bool bChargeModifiedCM = false;
    bool generate_neutron_scattering_lengths = false;
};


static int get_element(const char* atom_name, const char* residue_name, const real m, char* elemStr,
        const GenscattOptions& options, char* comment) {
    if (comment) {
        /* empty comment */
        comment[0] = '\0';
    }

    if (m < 0.0) {
        gmx_fatal(FARGS, "Atom %s has mass < zero.\nThis is not implemented yet\n", atom_name);
    }

    if (strlen(atom_name) > 5) {
        gmx_fatal(FARGS, "Atom name %s too long\n", atom_name);
    }

    /** Remove numbers and make fist and second char upper and lower-case, respectively */

    char nm[6];
    char* ptr;
    strcpy(nm, atom_name);
    ptr = &nm[0];

    while (!isalpha(ptr[0])) {
        ptr++;
    }

    if (strlen(ptr) < 1) {
        gmx_fatal(FARGS, "Atom name %s is invalid. Maybe only numbers\n", atom_name);
    }

    ptr[0] = toupper(ptr[0]);

    if (isalpha(ptr[1])) {
        ptr[1] = tolower(ptr[1]);
        ptr[2] = '\0';
    } else {
        ptr[1] = '\0';
    }

    if (m == 0.0) {
        /* If no mass is given:
           if (atom name == residue name) -> assume two letter element, otherwise use the first letter of the atom name */
        if (residue_name == nullptr) {
            gmx_fatal(FARGS, "Inconsistency in get_element()\n");
        }

        /* Check for water */
        if (!strcmp(residue_name, "HOH") or !strcmp(residue_name, "SOL")) {
            if (ptr[0] == 'O') {
                strcpy(elemStr, "Owat");

                return 0;
            } else if (ptr[0] == 'H') {
                strcpy(elemStr, "Hwat");

                return 0;
            }
        }

        char capitalized_residue_name[6];
        strcpy(capitalized_residue_name, residue_name);

        capitalized_residue_name[0] = toupper(capitalized_residue_name[0]);

        for (int i = 1; isalpha(residue_name[i]); i++) {
            capitalized_residue_name[i] = tolower(capitalized_residue_name[i]);
        }

        if (!strcmp(ptr, capitalized_residue_name)) {
            strcpy(elemStr, ptr);
        } else {
            elemStr[0] = ptr[0];
            // strncpy(elemStr, ptr, 1); Causes a warning.
            elemStr[1] = '\0';
        }

        return 0;
    }


    /** Check one-character elements */

    char tmp[2];
    strncpy(tmp, ptr, 1);
    tmp[1] = '\0';

    bool bOne = false;
    bool bTwo = false;

    real mass = 0.0;
    char elem[3];

    if (search_elements(tmp, &mass, &elem[0]) == 0) {
        real relative_mass_difference = fabs((mass - m) / m);
        real mass_diff = fabs(mass - m);

        /** Strict test first */

        if (relative_mass_difference < ELEMENT_ALLOW_REL_MASS_DIFF) {
            bOne = true;
            strcpy(elemStr, elem);
        } else {
            /**
             * Catch for virtual sites C and N. Takes advantage of the fact that all 2-char elements have much larger masses.
             * Split into integral part and percentage deviation.
             */
            int whole = round((m - mass) / 1.008);  /* Needs to be 1, 2, or 3 for vsite */

            bool is_carbon = (toupper(elem[0]) == 'C');
            bool is_nitrogen = (toupper(elem[0]) == 'N');

            if (options.bGromos and is_carbon and (mass_diff < 3.5)) {
                sprintf(elemStr, "CH%d", whole);
                bOne = true;

                if (comment) {
                    sprintf(comment, "Gromos united atom");
                }
            } else if (options.expect_virtual_sites and (mass_diff < 3.5) and (is_carbon or is_nitrogen)) {
                bOne = true;
                strcpy(elemStr, elem);

                if (comment) {
                    sprintf(comment, "mass difference of %g au due to v-sites", mass_diff);
                }
            } else if (mass_diff < options.max_mass_diff) {
                bOne = true;
                strcpy(elemStr, elem);

                if (comment) {
                    sprintf(comment, "large mass difference of %g au - correct?", mass_diff);
                }

                fprintf(stderr, "nAtom %-3s (mass %7g) could be element %s - "
                        "but better check the assignment (option -el)\n",
                        atom_name, m, elem);
            }
        }
    }

    /** Check two-character elements */

    if (strlen(ptr) == 2) {
        if (search_elements(ptr, &mass, &elem[0]) == 0) {
            real relMassDiff = fabs((mass - m) / m);

            if (relMassDiff < ELEMENT_ALLOW_REL_MASS_DIFF) {
                if (bOne) {
                    gmx_fatal(FARGS,"For atom %s (mass %g), both a one-letter and two-letter "
                              "chemical element seems to match\n",
                            atom_name, m);
                }

                bTwo = true;
                strcpy(elemStr, elem);
            }
        }
    }

    if (bOne or bTwo) {
        return 0;
    } else {
        strcpy(elemStr, "UNKNOWN");

        return 1;
    }
}

/**
 * Check for oxygen of COO(-) group in GLU or ASP or C-Terminus, NH3 of Lys or N-Terminus,
 * C of guanidinium moiety of Arg, or phosphorus of nucleic acids.
 * These are defined in $GMXDATA/top/cromer-mann-defs.itp
 */
static void ionicResiduesOverwriteElemstr(t_atoms* atoms, int iatom, char* elemStr, char* comment) {

    int natoms_mol = atoms->nr;
    const char* atom_name = *(atoms->atomname[iatom]);

    int resid       = atoms->resinfo[atoms->atom[iatom       ].resind].nr;
    int resid_first = atoms->resinfo[atoms->atom[0           ].resind].nr;
    int resid_last  = atoms->resinfo[atoms->atom[natoms_mol-1].resind].nr;

    char* residue_name = *(atoms->resinfo[atoms->atom[iatom].resind].name);

    if (isCarboxylateOminus(atom_name, residue_name)) {
        strcpy(elemStr, "COO_O");
        if (comment) {
            sprintf(comment, "Anionic carboxylate oxygen, # of electrons reduced by 0.5");
        }
    } else if (is_protonated_lysine_N_plus(atom_name, residue_name)) {
        strcpy(elemStr, "NHHH_N");
        if (comment) {
            sprintf(comment, "Amine nitrogen of Lysine, # of electrons increased by +1");
        }
    } else if (is_protonated_arginine_C_plus(atom_name, residue_name)) {
        strcpy(elemStr, "Arg_C");
        if (comment) {
           sprintf(comment, "Carbon of Arginine guanidinium moiety, # of electrons increased by +1");
        }
    } else if (isNucleicAcidPhosphate(atom_name, residue_name)) {
        strcpy(elemStr, "Nucleic_P");
        if (comment) {
            sprintf(comment, "Phosphorus of DNA/RNA backbone, # of electrons reduced by 1");
        }
    } else if (resid == resid_first and !strcmp(atom_name, "N")) {
        strcpy(elemStr, "NHHH_N");
        if (comment) {
            sprintf(comment, "Amine nitrogen of N-terminus, # of electrons increased by +1");
        }
    } else if (resid == resid_last and isCTerminusCarboxylateOxygen(atom_name)) {
        strcpy(elemStr, "COO_O");
        if (comment) {
            sprintf(comment, "Carboxylate O of C-terminus, # of electrons reduced by 0.5");
        }
    }
}

static void write_dummy_top(const char* output_file_name, const char* input_file_name, t_topology* top,
        const GenscattOptions& options) {
    char define_name[STRLEN];

    /** Choose wheather we write neutron scattering lengths or Cromer-mann parameters */
    sprintf(define_name, "%s", options.generate_neutron_scattering_lengths ? "NEUTRON_SCATT_LEN_" : "CROMER_MANN_");

    FILE* file = gmx_ffopen(output_file_name, "w");

    fprintf(file, "; This is a trivial topology generated by gmx genscatt from file %s\n;\n"
            "; This topology is intended to be used with mdrun -rerun to compute the scattering\n"
            "; intensity I(q) of a group of atoms (such as a CH3 group or similar)\n\n", input_file_name);
    fprintf(file, "[ defaults ]\n1       2        yes      0.5     0.8333\n\n");
    fprintf(file, "#include \"cromer-mann-defs.itp\"\n\n");
    fprintf(file, "[ atomtypes ]\n");

    ElementName elemStr;
    ElementName* elemHave = nullptr;

    int ntypes = 0;

    for (int i = 0; i < top->atoms.nr; i++) {
        char* residue_name = *(top->atoms.resinfo[top->atoms.atom[i].resind].name);
        get_element(*(top->atoms.atomname[i]), residue_name, 0.0, &elemStr[0], options, nullptr);

        bool bNew = true;

        for (int j = 0; j < ntypes; j++) {
            if (!strcmp(elemStr, elemHave[j])) {
                bNew = false;
            }
        }

        if (bNew) {
            fprintf(file, "dummy_%-4s  %d   %5.2f  0.000  A  0.00000e+00  0.00000e+00\n", elemStr, 1, top->atoms.atom[i].m);
            ntypes++;
            srenew(elemHave, ntypes);
            strcpy(elemHave[ntypes - 1], elemStr);
        }
    }

    fprintf(file,"\n[ moleculetype ]\nMolecule 2\n\n[ atoms ]\n");
    fprintf(file, ";   nr     type         resnr residue  atom   cgnr     charge       mass\n");

    for (int i = 0; i < top->atoms.nr; i++) {
        char* residue_name = *(top->atoms.resinfo[top->atoms.atom[i].resind].name);
        get_element(*(top->atoms.atomname[i]), residue_name, 0.0, &elemStr[0], options, nullptr);

        fprintf(file,"   %3d     dummy_%-4s       1     %3s       %4s       %d  0.0  1.0\n",
                i + 1, elemStr, residue_name, *(top->atoms.atomname[i]), i);
    }

    fprintf(file, "\n[ scattering_params ]\n");

    if (!options.generate_neutron_scattering_lengths) {
        fprintf(file, "; atom ft a1     a2      a3      a4      b1      b2      b3      b4      c\n");
    } else {
        fprintf(file, "; atom ft NSL(Coh b)\n");
    }

    for (int i = 0; i < top->atoms.nr; i++) {
        char* res_name = *(top->atoms.resinfo[top->atoms.atom[i].resind].name);
        get_element(*(top->atoms.atomname[i]), res_name, 0.0, &elemStr[0], options, nullptr);

        fprintf(file, "%5d  1  %s%s\n", i + 1, define_name, elemStr);
    }

    fprintf(file, "\n[ system ]\nA trivial topology to compute scattering intensities with mdrun -rerun\n");
    fprintf(file, "\n[ molecules ]\nMolecule       1\n");

    gmx_ffclose(file);
    fprintf(stderr, "\nWrote %s\n\n", output_file_name);

    sfree(elemHave);
}

int gmx_genscatt(int argc, char* argv[]) {
    const char* desc[] = { "[TT]gmx genscatt[tt] produces include files (itp) for a topology containing ",
        "a list of atom numbers and definitions for X-ray or neutron scattering, ",
        "that is, definitions for Cromer-Mann parameters and Neutron Scattering Lengths ([TT]-nsl[tt]). ",
        "The tool will write one itp file per molecule type found in the selected index group. ",
        "For instance, if your protein contains two chains A and B, and these are defined in ",
        "separate [TT][ moleculetype ][tt] blocks, then gmx genscatt would write two itp files.[PAR]",
        "Like position restraint definitions, scattering-types are defined within molecules, ",
        "and therefore should be #included within the correct [TT][ moleculetype ][tt] ",
        "block in the topology. For instance, you can place it near the #include \"posre.itp\" statement ",
        "of the moleculetype definition.[PAR]",
        "Make sure to select a group that contains all physcial atoms of your solute, such as the protein, ",
        "DNA/RNA, ligands, coordinated ions, heme groups etc. Maybe you will have to generate an index ",
        "file first. Alternatively, you could also run gmx genscatt once for each scattering molecule.[PAR]",
        "In case you use virtual sites, make sure to select a group ",
        "that contains only physical atoms, such as \"Prot-Masses\".[PAR]",
        "Because the chemical element is not stored in a tpr file, the tool guesses ",
        "the element using a combination of the atom name and the atom mass. Consequently, ",
        "if the atomic masses deviate from the physical masses, either due to a united-atom force field, ",
        "or because you model hydrogen atoms as virtual sites, you must help gmx genscatt with the options ",
        "[TT]-gromos[tt] or [TT]-vsites[tt], respectively.[PAR]",
        "Option [TT]-el[tt] writes files with more details on guessed chemical elements.[PAR]",
        "By default, charges on ionic residues are ignored, that is, the tabulated Cromer-Mann parameteres of the",
        "neutral elements are used. With [TT]-ionic[tt], you can correct the Cromer-Mann parameter by the charge for oxygen atoms of",
        "carboxyl group (Glu, Asp), carbon of the Arg moiety, nitrogen of Lys, and phosphate of DNA/RNA backbone.",
        "See comments in $GMXDATA/top/cromer-mann-defs.itp for more explanations.[PAR]",
        "To merge the Cromer-Mann parameters of hydrogen atoms into the heavy atoms, use [TT]-ua[tt]. This can ",
        "be used to test the effect of atomic details in the far WAXS regime.[PAR]" };

    static bool bDefault = false, bUA = false;
    static int warnings_limit = 20;

    GenscattOptions options;

    t_pargs shell_arguments[] = {
        { "-def", false, etBOOL, { &bDefault },
                "Write no scattering factors and allow grompp to read from the forcefield definitions later" },
        { "-maxwarn", false, etINT, { &warnings_limit },
                "Limit of v-site related warnings gmx genscatt will show before they are suppressed." },
        { "-ionic", false, etBOOL, { &options.bChargeModifiedCM },
                "Correct number of electrons for ionic residues and nucleic acid backbone based on residue and atom names" },
        { "-ua", false, etBOOL, { &bUA },
                "Merge atomic scattering factors of hydrogens into bonded heavy atom" },
        { "-gromos", false, etBOOL, { &options.bGromos },
                "Expect united-atom carbon atoms" },
        { "-vsites", false, etBOOL, { &options.expect_virtual_sites },
                "Expect vsites (H with mass zero & heavier C and N)" },
        { "-mdiff", false, etREAL, { &options.max_mass_diff },
                "Largest allowed mass difference for element assignments" },
        { "-nsl", false, etBOOL, { &options.generate_neutron_scattering_lengths },
                "Write Neutron Scattering Lengths in addition to Cromer-Mann definitions. Required for SANS calculations" },
    };

    t_filenm files_names[] = {
        { efSTX, "-s", nullptr, ffREAD },
        { efNDX, "-n", nullptr, ffOPTRD },
        { efITP, "-o", "scatter", ffWRITE },
        { efDAT, "-el", "elemassign", ffOPTWR },
        { efTOP, "-p", "dummy", ffOPTWR },
    };

    gmx_output_env_t* output_env = nullptr;

    if (!parse_common_args(&argc, argv, PCA_CAN_VIEW, asize(files_names), files_names, asize(shell_arguments),
            shell_arguments, asize(desc), desc, 0, nullptr, &output_env)) {
        return 0;
    }

    if (options.bChargeModifiedCM and (options.bGromos or bUA)) {
        gmx_fatal(FARGS, "Option -ionic is not supported together with -ua or -gromos.");
    }

    bool bDummyTop = opt2bSet("-p", asize(files_names), files_names);

    const char* in_file = ftp2fn(efSTX, asize(files_names), files_names);
    bool bHaveTpr = (fn2ftp(in_file) == efTPR);

    t_topology* top = nullptr;
    snew(top, 1);

    PbcType pbcType = PbcType::Unset; rvec* xtop = nullptr; matrix box;  // unused
    read_tps_conf(in_file, top, &pbcType, &xtop, nullptr, box, false);

    int maxwarnStart = warnings_limit;

    if (bDummyTop) {
        write_dummy_top(opt2fn("-p", asize(files_names), files_names), in_file, top, options);

        exit(0);
    }

    fprintf(stderr, "\nSelect atoms that scatter (e.g., Prot-Masses, Protein):\n");

    int isize = 0;
    int* index = nullptr;
    char* groups_names = nullptr;
    get_index(&top->atoms, ftp2fn_null(efNDX, asize(files_names), files_names), 1, &isize, &index, &groups_names);

    if (!bHaveTpr) {
        gmx_fatal(FARGS,"Need a tpr file to write itp file with scattering information\n");
    }

    if (options.generate_neutron_scattering_lengths and bUA) {
        gmx_fatal(FARGS, "United atom form factors are not supported for neutron scattering.\n");
    }

    /**
     * Read the molecule types from tpr, then check which atoms in the molecule types appear.
     * This is done because we write one scatter.itp file for each molecule type.
     */
    t_inputrec ir; t_state state;  // unused
    gmx_mtop_t mtop;
    read_tpx_state(in_file, &ir, &state, &mtop);

    bool** bInIndex_perMoltype = nullptr;
    int* nInIndex_perMoltype = nullptr;
    int** moltype_globalIndex = nullptr;

    snew(bInIndex_perMoltype, mtop.molblock.size());
    snew(nInIndex_perMoltype, mtop.molblock.size());
    snew(moltype_globalIndex, mtop.molblock.size());

    int moltype_firstAtomIndex = 0;
    int nMoltypeInSelection = 0;

    printf("\nThe following molecule types are found in the tpr file:\n"
            "------------------------------------------------------\n");

    /* Loop over molecule types */
    for (size_t mb = 0; mb < mtop.molblock.size(); mb++) {
        int nmols = mtop.molblock[mb].nmol;

        const gmx_molblock_t& molblock = mtop.molblock[mb];
        const gmx_moltype_t& moltype = mtop.moltype[molblock.type];
        int natoms_mol = moltype.atoms.nr;

        char* molname = *(mtop.moltype[mtop.molblock[mb].type].name);
        int moltype_lastAtomIndex = moltype_firstAtomIndex + nmols * natoms_mol - 1;

        snew(bInIndex_perMoltype[mb], natoms_mol);
        snew(moltype_globalIndex[mb], natoms_mol);

        for (int i = 0; i < natoms_mol; i++) {
            /* Make sure we get a Segfault in case we use below an atom that is not found in the index file */
            moltype_globalIndex[mb][i] = -999999999;
        }

        /* For this molecule type, make a boolean list that is true if this atom appears in the index group */
        for (int i = 0; i < isize; i++) {
            if (moltype_firstAtomIndex <= index[i] and index[i] <= moltype_lastAtomIndex) {
                int molAtomIndex = (index[i] - moltype_firstAtomIndex) % natoms_mol;
                int moltype_instance = (index[i] - moltype_firstAtomIndex) / natoms_mol;

                if (moltype_instance > 0 and not bInIndex_perMoltype[mb][molAtomIndex]) {
                    gmx_fatal(FARGS, "Your selected index group \"%s\" contains multiple molecules of molecule type %s,\n"
                            "which is fine. However, your index groups seems to contain different atoms of the same\n"
                            "molecule type, which is not allowed. For instance, if you have two identical protein\n"
                            "chains, then your index group must contain exactly the same atoms of the two chains.\n"
                            "The problematic atom is atom with global index %d, or molecule no %d of type %s, \n"
                            "molecule-internal index %d\n",
                            groups_names, molname, index[i] + 1, moltype_instance + 1, molname, molAtomIndex + 1);
                }

                bInIndex_perMoltype[mb][molAtomIndex] = true;

                /* Make an array that translates the atom number within a molecule type
                   into the global index */
                if (moltype_instance == 0) {
                    moltype_globalIndex[mb][molAtomIndex] = index[i];
                }
            }
        }

        /* Count the number of atoms of this molecule type that appear somewhere in the index group. */
        for (int i = 0; i < natoms_mol; i++) {
            if (bInIndex_perMoltype[mb][i]) {
                nInIndex_perMoltype[mb]++;
            }
        }

        if (nInIndex_perMoltype[mb] > 0) {
            /* Count how many molecule types are represented in the index group */
            nMoltypeInSelection++;
        }

        printf("  %2d) %-25s  #molecules = %5d   #atoms = %4d   #atoms in user selection (\"%s\") = %d\n",
                static_cast<int>(mb + 1), molname, nmols, natoms_mol, groups_names, nInIndex_perMoltype[mb]);

        /* Set first index of the next molecule type */
        moltype_firstAtomIndex += nmols * natoms_mol;
    }

    printf("\nFound %d molecule types in the selection \"%s\".\n\n", nMoltypeInSelection, groups_names);

    FILE* fpAssign = nullptr;
    char comment[STRLEN];
    char buf[15];

    /* Loop over molecule types for writing itp files */
    for (size_t mb = 0; mb < mtop.molblock.size(); mb++) {
        /* Check if this moltype is in the index group */
        if (nInIndex_perMoltype[mb] == 0) {
            continue;
        }

        char* molname = *(mtop.moltype[mtop.molblock[mb].type].name);
        t_atoms* atoms = &mtop.moltype[mtop.molblock[mb].type].atoms;
        int natoms_mol = atoms->nr;

        const char* fnUser = opt2fn("-o", asize(files_names), files_names);
        char fnout[STRLEN];

        sprintf(fnout, "%.*s_%s.itp", (int)(strlen(fnUser) - 4), fnUser, molname);
        printf("Writing scatter parameters of molecule \"%s\" into %s\n", molname, fnout);

        FILE* file = gmx_ffopen(fnout, "w");
        fprintf(file, "; Written by gmx genscatt");

        time_t timeNow = 0;
        time(&timeNow);
        fprintf(file, "; This file was created %s\n", gmx_ctime_r(&timeNow).c_str());

        if (opt2bSet("-el", asize(files_names), files_names)) {
            fnUser = opt2fn("-el", asize(files_names), files_names);
            sprintf(fnout, "%.*s_%s.dat", (int)(strlen(fnUser) - 4), fnUser, molname);
            printf("\nWriting atom assignments of molecule %s into %s\n", molname, fnout);
            fpAssign = gmx_ffopen(fnout, "w");
        }

        /* array to store the element, whether the atom is a hydrogen atom, and to which heavy atom this
           H-atom is bound to (if it is an H-atom) */
        std::vector<ElementName> elements(natoms_mol);
        std::vector<bool> is_hydrogen(natoms_mol, false);
        std::vector<int> bonded_to(natoms_mol, -1);

        /* Next block: writing Cromer-Mann */
        fprintf(file, "[ scattering_params ]\n");
        fprintf(file, "; atom ft a1     a2      a3      a4      b1      b2      b3      b4      c\n");

        char define_name[STRLEN];
        sprintf(define_name, "%s", "CROMER_MANN_");

        int ft = 1;

        int nHeavy = 0;
        bool alerted = false;

        for (int i = 0; i < natoms_mol; i++) {
            if (!bInIndex_perMoltype[mb][i]) {
                continue;
            }

            int global_index = moltype_globalIndex[mb][i];

            if (bDefault) {
                fprintf(file, "%5d  1  \n", i + 1);

                continue;
            }

            ElementName element_name;

            char* residue_name = *(atoms->resinfo[atoms->atom[i].resind].name);
            int residue_id = atoms->resinfo[atoms->atom[i].resind].nr;

            int ret = get_element(*(atoms->atomname[i]), residue_name, atoms->atom[i].m, &element_name[0], options, comment);

            /* Keep element of this atom for NSL */
            strcpy(elements[i], element_name);

            if (ret != 0 and warnings_limit > 0) {
                fprintf(stderr,"WARNING -Atom nr %d, name %s, mass %g could not be assigned"
                        " to an element (maybe need option -vsites?)\n", global_index, *(atoms->atomname[i]),
                        atoms->atom[i].m);

                warnings_limit--;
            }

            /* Overwrite element string for some charged groups, such as Lys-NZ, Arg-CZ, termini, DNA/RNA-P, Glu/Asp */
            if (options.bChargeModifiedCM) {
                ionicResiduesOverwriteElemstr(atoms, i, &element_name[0], comment);
            }

            /* If we don't write united-atom form factors, we can write the CM parameters now. Otherwise,
               the CM parameters are written below */
            if (!bUA) {
                /* Write the Cromer-Mann or NSL definition */
                fprintf(file, "%5d  %d  %s%-10s   ; %4s - %4s-%d\n", i + 1, ft, define_name, element_name,
                        *(atoms->atomname[i]), residue_name, residue_id);

                if (fpAssign) {
                    fprintf(fpAssign, "%4s-%-4d  %4s, mass %7.3f -> %-10s   %s\n", residue_name, residue_id,
                            *(atoms->atomname[i]), atoms->atom[i].m, element_name, comment);
                }
            }


            if (!strcmp(element_name, "H")) {
                is_hydrogen[i] = true;
            } else {
                nHeavy++;
            }

            if (!alerted and !warnings_limit) {
                fprintf(stderr, "\nMaximum number of mass and vsite-related warnings reached. Further notes suppressed.\n\n");
                fprintf(stderr, "NOTE: Warnings may occur because your system contains virtual sites. If so, make sure to select only the physical\n"
                        "atoms (e.g. group \"Prot-Masses\") but not the non-physical dummy atoms.\n\n");

                alerted = true;
            }
        }

        int* nHydOnHeavy = nullptr;

        /**
         * For each H-atom, find the heavy atom to which it is bound. This is needed for:
         *  1) United atom form factors.
         *  2) For Neutron Scattering Lengths to tell whether an H-atom is polar or not, and whether it is bound
         *     to a backbone nitrogen.
         */
        if (bUA or options.generate_neutron_scattering_lengths) {
            /**
             * To do: The following routine should better pick the bonds from the mtop, not from the top. This way,
             * we would not have to translate the molecule-internal atom numbers into the global atom numbers. Well,
             * but it works for now...
             */

            /* Find out to which heavy atom the hydrogen is bound */
            printf("%s: Found %d hydrogen atoms and %d heavy atoms\n", molname, nInIndex_perMoltype[mb] - nHeavy, nHeavy);
            snew(nHydOnHeavy, nInIndex_perMoltype[mb]);

            for (int interaction_type = 0; interaction_type < F_NRE; interaction_type++) {
                if (IS_CHEMBOND(interaction_type)) {
                    /* Now loop over all chemical bonds */

                    for (int j = 0; (j < top->idef.il[interaction_type].nr); j += interaction_function[interaction_type].nratoms + 1) {
                        /**
                         * Caution: atom1 and atom2 are global atom indices, whereas the counter i, atom1ind, and atom2ind
                         * are indices within the molecule type (0 <= atom1ind < natoms_mol)
                         */
                        int atom1 = top->idef.il[interaction_type].iatoms[j + 1];
                        int atom2 = top->idef.il[interaction_type].iatoms[j + 2];

                        /* find nr for index[] */
                        int atom1ind = -1;
                        int atom2ind = -1;

                        for (int i = 0; i < natoms_mol; i++) {
                            if (bInIndex_perMoltype[mb][i]) {
                                int global_index = moltype_globalIndex[mb][i];

                                if (global_index == atom1) {
                                    atom1ind = i;
                                }

                                if (global_index == atom2) {
                                    atom2ind = i;
                                }
                            }

                            if (atom1ind >= 0 and atom2ind >= 0) {
                                break;
                            }
                        }

                        if (atom1ind == -1 or atom2ind == -1) {
                            /* Not both atoms are in index[] -> continue */
                            continue;
                        }

                        if ((not is_hydrogen[atom1ind] and is_hydrogen[atom2ind])
                                or (not is_hydrogen[atom2ind] and is_hydrogen[atom1ind])) {
                            /* increase nHydOnHeavy[] of the heavy atom */
                            if (is_hydrogen[atom1ind]) {
                                nHydOnHeavy[atom2ind]++;
                                bonded_to[atom1ind] = atom2ind;
                            } else {
                                nHydOnHeavy[atom1ind]++;
                                bonded_to[atom2ind] = atom1ind;
                            }
                        }
                    }
                }
            }
        }

        if (options.generate_neutron_scattering_lengths) {
            /* Next block: writing NSL */
            fprintf(file, "\n; atom ft NSL(Coh b)\n");
            sprintf(define_name, "%s", "NEUTRON_SCATT_LEN_");
            ft = 2;

            for (int i = 0; i < natoms_mol; i++) {
                if (!bInIndex_perMoltype[mb][i]) {
                    continue;
                }

                char* resname = *(atoms->resinfo[atoms->atom[i].resind].name);
                int resid = atoms->resinfo[atoms->atom[i].resind].nr;

                if (!is_hydrogen[i]) {
                    /* empty comment */
                    comment[0] = '\0';

                    /* Write the NSL definition */
                    fprintf(file, "%5d  %d  %s%-10s   ; %4s - %s-%d\n", i + 1, ft, define_name, elements[i], *(atoms->atomname[i]), resname, resid);
                } else {
                    if (bonded_to[i] != -1 and is_backbone_hydrogen(i, bonded_to[i], atoms)) {
                        sprintf(comment, " -- Protein backbone hydrogen");
                        fprintf(file, "%5d  %d  %-28s   ; %4s - %s-%d\n", i + 1, ft,
                                "NSL_H_DEUTERATABLE_BACKBONE", *(atoms->atomname[i]), resname, resid);
                    } else if (bonded_to[i] != -1 and is_polarizing_hydrogen(elements[bonded_to[i]])) {
                        sprintf(comment, " -- deuteratable non-backbone hydrogen");
                        fprintf(file, "%5d  %d  %-28s   ; %4s - %s-%d\n", i + 1, ft,
                                "NSL_H_DEUTERATABLE", *(atoms->atomname[i]), resname, resid);
                    } else {
                        sprintf(comment, " -- non-deuteratable non-polar hydrogen");
                        fprintf(file, "%5d  %d  %-28s   ; %4s - %s-%d\n", i + 1, ft,
                                "NEUTRON_SCATT_LEN_1H", *(atoms->atomname[i]), resname, resid);
                    }
                }

                if (fpAssign) {
                    fprintf(fpAssign, "%4s-%-4d  %4s, mass %7.3f -> %-10s   %s\n", resname, resid,
                            *(atoms->atomname[i]), atoms->atom[i].m, elements[i], comment);
                }
            }
        }

        if (bUA) {
            /** Next block: writing unite-atom Cromer-Mann */
            sprintf(define_name, "%s", "CROMER_MANN_");
            ft = 1;

            /** For united atom scattering factors, write CH, CH2, NH3, etc. */
            for (int i = 0; i < natoms_mol; i++) {
                if (!bInIndex_perMoltype[mb][i]) {
                    continue;
                }

                if (!is_hydrogen[i]) {
                    if (nHydOnHeavy[i] > 1) {
                        sprintf(buf, "%sH%d", elements[i], nHydOnHeavy[i]);
                    } else if (nHydOnHeavy[i] == 1) {
                        sprintf(buf, "%sH", elements[i]);
                    } else {
                        sprintf(buf, "%s", elements[i]);
                    }

                    /* Write the Cromer-Mann or NSL definition */
                    fprintf(file, "%5d  %d  %s%s\n", i + 1, ft, define_name, buf);

                    if (fpAssign) {
                        fprintf(fpAssign, "%4s, mass %7.3f -> %s, %s bound to %d H\n", *(atoms->atomname[i]),
                                atoms->atom[i].m, buf, elements[i], nHydOnHeavy[i]);
                    }
                } else {
                    if (bonded_to[i] == -1) {
                        /* Catch the case that this hydrogen is not bonded to any heavy atom */
                        fprintf(stderr,"WARNING -\n Atom %d (%s) of molecule type %s is not bonded to any heavy atom in the group.\n",
                                i + 1, *(atoms->atomname[i]), molname);

                        fprintf(file, "%5d  %d  %s%s\n", i + 1, ft, define_name, elements[i]);

                        if (fpAssign) {
                            fprintf(fpAssign, "%4s, mass %7.3f -> %s\n", *(atoms->atomname[i]),
                                    atoms->atom[i].m, elements[i]);
                        }
                    } else {
                        if (fpAssign) {
                            fprintf(fpAssign, "%4s, mass %7.3f -> merged into atom %d (%s)\n", *(atoms->atomname[i]),
                                    atoms->atom[i].m, bonded_to[i], *(atoms->atomname[bonded_to[i]]));
                        }
                    }
                }
            }
        }

        gmx_ffclose(file);

        if (fpAssign) {
            gmx_ffclose(fpAssign);
        }

        if (nHydOnHeavy) {
            sfree(nHydOnHeavy);
        }
    }

    sfree(top);

    if (maxwarnStart == warnings_limit) {
        return 0;
    } else {
        return 1;
    }
}
