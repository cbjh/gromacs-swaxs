#pragma once

#include <waxs/envelope/envelope.h>  // gmx_envelope

namespace swaxs {

/** Write a VMD TCL script into `envelope.tcl` */
void envelope_write_vmd_cgo(gmx_envelope* envelope, const char* fn, const rvec rgb, real alpha);

/** Write a PyMOL CGO file into `envelope.py` */
void envelope_write_pymol_cgo(gmx_envelope* envelope, const char* fn, const char* name, rvec rgb, rvec rgb_inside, real alpha);

/**
 * Writes envelope to PDB file. It is useful for debugging, not used anywhere.
 */
void envelope_dump_to_pdb(gmx_envelope* envelope, int nHighlight, int* high, gmx_bool bExtra, rvec x_extra, double R, const char* fn);

/** Reads raw envelope file */
gmx_envelope* envelope_read_from_file(const char* fn);

/** Writes raw envelope file */
void envelope_write_to_file(gmx_envelope* envelope, const char* fn);

// TODO: This should be removed.
void envelope_triangle_to_cgo(gmx_envelope* envelope, FILE* fp, int f, double rcyl, double r, double g, double b);

// TODO: This should be removed.
void envelope_dvec_to_cgo(FILE* fp, const dvec x, double radius);

void envelope_dump_triangle_to_cgo_file(gmx_envelope* envelope, int f, const dvec x, const char* fn);

}
