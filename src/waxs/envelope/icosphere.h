#pragma once

#include <cstdio>  // FILE

#include <gromacs/math/vec.h>  // ivec, dvec
#include <gromacs/utility/basedefinitions.h>  // gmx_bool

struct t_icosphere_face {
    ivec v; /* the 3 vertices IDs */
    dvec normal; /* normal vector */
    double sprodLim; /* smallest scalar product between the normal with each of the vertices.
                        Useful to quickly check if a vector crosses this face. */
};

struct t_icosphere {
    int nrec;
    int nvertex;
    int nface; /* # of faces in lowest recusion level == rec_nface[nrec-1] */
    dvec* vertex;
    t_icosphere_face* face; /* shortcut to the faces of lowest recusion level */

    /* Also store all levels of faces, to facilitate a search */
    int* rec_nface; /* # of faces in recusion levels */
    t_icosphere_face** rec_face; /* faces in previsous recursion levels */
};

t_icosphere* icosphere_build(int nrec, gmx_bool bVerbose);

int icosphere_isInFace(t_icosphere* ico, t_icosphere_face* face, const dvec x, double tol, double* deviation);

void icosphere_thisFace_to_CGO(FILE* fp, t_icosphere* ico, t_icosphere_face* face, double rcyl, double r, double g,
        double b, double fact);

int icosphere_x2faceID(t_icosphere* ico, const dvec xgiven);
