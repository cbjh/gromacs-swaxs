#include "./density.h"

#include <cmath>  // floor, isinf, isnan
#include <cstdio>  // fprintf, fflush, printf

#include <gromacs/gmxlib/network.h>  // gmx_bcast
#include <gromacs/math/vec.h>  // diprod, rvec
#include <gromacs/mdtypes/commrec.h>  // PAR
#include <gromacs/simd/simd.h>  // gmx::SimdReal
#include <gromacs/simd/simd_math.h>  // gmx::sincos
#include <gromacs/utility/basedefinitions.h>  // gmx_bool
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/real.h>  // real
#include <gromacs/utility/smalloc.h>  // snew, snew_aligned, sfree, sfree_aligned

#include <waxs/envelope/envelope.h>  // gmx_envelope
#include <waxs/envelope/icosphere.h>  // icosphere_x2faceID
#include <waxs/envelope/rvec.h>  // copy_rdvec

namespace swaxs {

void envelope_solvent_density_next_frame(gmx_envelope* envelope, double scale) {
    if (!envelope->bHaveEnv) {
        gmx_fatal(FARGS, "Error, trying update solvent density, but envelope is not defined\n");
    }

    /** Norm and factors for cumulative averaging: D[n] = fac1 * D[n-1] + fac2 * d[n] */
    envelope->solventNelecNorm = 1. + scale * envelope->solventNelecNorm;
    envelope->solventNelec_fac1 = 1. * (envelope->solventNelecNorm - 1.) / envelope->solventNelecNorm;
    envelope->solventNelec_fac2 = 1. / envelope->solventNelecNorm;

    for (int f = 0; f < envelope->nSurfElems; f++) {
        for (int bin = 0; bin < envelope->ngrid; bin++) {
            envelope->solventNelec[f][bin] *= envelope->solventNelec_fac1;
        }
    }

    envelope->nSolventStep++;
}

void envelope_solvent_density_add_atom(gmx_envelope* envelope, const rvec xin, double nElec) {
    dvec x;
    copy_rdvec(xin, x);

    int f = icosphere_x2faceID(envelope->ico, x);

    /**
     * Important: r is here not simply the distance from the origin, since the volume bins
     * have a flat surface. Instead, our r is the distance AFTER projecting on normal vector
     * of the face.
     */
    double r = diprod(envelope->ico->face[f].normal, x);

    if (!envelope->surfElemOuter[f].bDefined) {
        gmx_fatal(FARGS, "Error while updatig solvent density inside envelope\n"
                "Atom (x = %g / %g / %g) is in a non-defined face of the envelope\n", x[XX], x[YY], x[ZZ]);
    }

    double rmin = envelope->bOriginInside ? 0.0 : envelope->surfElemInner[f].rmin;
    double rmax = envelope->surfElemOuter[f].rmax;

    if (r > rmax or r < rmin) {
        gmx_fatal(FARGS, "Error while updating solvent density inside envelope\n"
                "Atom (r = %g ) is outside of the envelope (rmin = %g, rmax = %g)\n", r, rmin, rmax);
    }

    double dr = (rmax - rmin) / envelope->ngrid;
    int ibin = floor((r - rmin) / dr);

    if (ibin == envelope->ngrid and r < (rmax + 1e-5)) {
        /* catch numerical inaccuracy */
        ibin = envelope->ngrid - 1;
    }

    if (ibin < 0 or ibin >= envelope->ngrid) {
        gmx_fatal(FARGS, "Incorrect ibin = %d found (ngrid = %d)\n", ibin, envelope->ngrid);
    }

    /** Add density for the present frame with appriate weight fac2 */
    envelope->solventNelec[f][ibin] += envelope->solventNelec_fac2 * nElec;

    if (std::isinf(envelope->solventNelec[f][ibin]) or std::isnan(envelope->solventNelec[f][ibin])) {
        fprintf(stderr, "Inf/ Nan for f/ ibin = %d / %d. fac2 = %g, nelec = %g\n", f, ibin, envelope->solventNelec_fac2, nElec);
    }
}

void envelope_solvent_density_bcast(gmx_envelope* envelope, const t_commrec* cr) {
    double sumNelec = 0.;

    if (cr and PAR(cr)) {
        for (int f = 0; f < envelope->nSurfElems; f++) {
            gmx_bcast(envelope->ngrid * sizeof(double), envelope->solventNelec[f], cr->mpi_comm_mygroup);

            for (int ibin = 0; ibin < envelope->ngrid; ibin++) {
                sumNelec += envelope->solventNelec[f][ibin];

                if (std::isnan(envelope->solventNelec[f][ibin]) or std::isinf(envelope->solventNelec[f][ibin])) {
                    gmx_fatal(FARGS, "Found NaN or Inf for solvent density of envelope: solventNelec[%d][%d] = %g\n",
                            f, ibin, envelope->solventNelec[f][ibin]);
                }
            }
        }
    }

    envelope->solventNelecTotal = 0.;

    for (int f = 0; f < envelope->nSurfElems; f++) {
        for (int ibin = 0; ibin < envelope->ngrid; ibin++) {
            envelope->solventNelecTotal += envelope->solventNelec[f][ibin];
        }
    }

    if (envelope->bVerbose and FALSE) {
        printf("\tNode %2d: Total # of electrons in solvation shell = %g\n", cr->nodeid, sumNelec);
    }
}

static void gmx_envelope_FourierTransform_low(gmx_envelope* envelope, rvec* q, int qhomenr, real* ft_re, real* ft_im,
        gmx_bool bSolventDens) {
    #pragma omp parallel for
    for (int jj = 0; jj < qhomenr; jj++) {
        ft_re[jj] = 0.;
        ft_im[jj] = 0.;
    }

    int nReal = sizeof(gmx::SimdReal) / sizeof(real);
    int nq2 = ((qhomenr - 1) / nReal + 1) * nReal;

    #pragma omp parallel shared(ft_re, ft_im)
    {
        gmx::SimdReal *m_qdotx, *mRe, *mIm;
        real *p_qdotx, *re, *im, fact, *ft_re_loc, *ft_im_loc;

        snew_aligned(p_qdotx, nq2, 32);
        snew_aligned(re, nq2, 32);
        snew_aligned(im, nq2, 32);
        snew(ft_re_loc, qhomenr);
        snew(ft_im_loc, qhomenr);

        #pragma omp for
        for (int f = 0; f < envelope->nSurfElems; f++) {
            if (!envelope->surfElemOuter[f].bDefined) {
                continue;
            }

            if (envelope->bVerbose and ((f % 20) == 0)) {
                printf("\r%7.2f %% done", (f + 1.) * 100. / envelope->nSurfElems);
                fflush(stdout);
            }

            {
                for (int i = 0; i < envelope->ngrid; i++) {
                    if (bSolventDens) {
                        /** When doing FT of the solvent, multiply here by the # of electrons in this bin */
                        fact = envelope->solventNelec[f][i];
                    } else {
                        /** For FT of a unit density, multiply by the volume of the bin */
                        fact = envelope->binVol[f][i];
                    }

                    if (fact == 0.0) {
                        continue;
                    }

                    /** Write q * x into array */
                    for (int j = 0; j < qhomenr; j++) {
                        p_qdotx[j] = iprod(q[j], envelope->xBinVol[f][i]);
                    }

                    m_qdotx = (gmx::SimdReal*)p_qdotx;
                    mRe = (gmx::SimdReal*)re;
                    mIm = (gmx::SimdReal*)im;

                    /** Compute all sines and cosines AVX/FMA instructions */
                    for (int k = 0; k < qhomenr; k += nReal) {
                        #ifndef GMX_WAXS_NO_SIMD
                            gmx::sincos(*m_qdotx, mIm, mRe);
                        #else
                            /** Without SIMD accelleration, m_qdotx ect. are simply double. */
                            *mIm = gmx::cos(*m_qdotx);
                            *mRe = gmx::sin(*m_qdotx);
                        #endif

                        m_qdotx++;
                        mRe++;
                        mIm++;
                    }

                    /** And write result into ft_re/ft_im arrays */
                    for (int j = 0; j < qhomenr; j++) {
                        ft_re_loc[j] += fact * re[j];
                        ft_im_loc[j] += fact * im[j];
                    }
                }
            }
        }

        #pragma omp critical
        {
            for (int j = 0; j < qhomenr; j++) {
                ft_im[j] += ft_im_loc[j];
                ft_re[j] += ft_re_loc[j];
            }
        }

        sfree_aligned(p_qdotx);
        sfree_aligned(re);
        sfree_aligned(im);
        sfree(ft_re_loc);
        sfree(ft_im_loc);
    }

    for (int jj = 0; jj < qhomenr; jj++) {
        if (std::isnan(ft_re[jj]) or std::isinf(ft_re[jj]) or std::isnan(ft_im[jj]) or std::isinf(ft_im[jj])) {
            gmx_fatal(FARGS, "Nan/Inf error after fourier transform: j = %d, ft_re / ft_im = %g / %g\n",
                    jj, ft_re[jj], ft_im[jj]);
        }
    }

    if (envelope->bVerbose) {
        printf("...done\n\n");
    }
}

void envelope_unit_ft(gmx_envelope* envelope, rvec* q, int nq, real** ft_re, real** ft_im) {
    if (!envelope->bHaveEnv) {
        gmx_fatal(FARGS, "Cannot compute FT of envelope since the envelope is not yet constructed.\n");
    }

    if (envelope->bHaveFourierTrans == FALSE) {
        if (!envelope->ftunit_re) {
            snew(envelope->ftunit_re, nq);
        }

        if (!envelope->ftunit_im) {
            snew(envelope->ftunit_im, nq);
        }

        if (envelope->bVerbose) {
            printf("\nDoing Fourier transform of envelope... (%d surface elements, %d grid, nq %d -> %.2e sin/cos)\n",
                    envelope->nSurfElems, envelope->ngrid, nq, 1.0 * envelope->nSurfElems * envelope->ngrid * nq);

            fflush(stdout);
        }

        gmx_envelope_FourierTransform_low(envelope, q, nq, envelope->ftunit_re, envelope->ftunit_im, FALSE);
        envelope->bHaveFourierTrans = TRUE;
    }

    /* return pointers to the FT (if != nullptr) */

    if (ft_re) {
        *ft_re = envelope->ftunit_re;
    }

    if (ft_im) {
        *ft_im = envelope->ftunit_im;
    }
}

void envelope_solvent_ft(gmx_envelope* envelope, rvec* q, int nq, gmx_bool bRecalcFT, real** ft_re, real** ft_im) {
    gmx_bool bCalcFT = bRecalcFT;

    if (envelope->nSolventStep == 0) {
        gmx_fatal(FARGS, "Envelope: cannot compute FT of density, since the density has not been computed yet\n");
    }

    if (envelope->ftdens_re == nullptr) {
        snew(envelope->ftdens_re, nq);
        snew(envelope->ftdens_im, nq);
        bCalcFT = TRUE;
    }

    if (bCalcFT) {
        if (envelope->bVerbose) {
            printf("\nDoing Fourier transform of solvent density... (%d surface elements, %d grid, nq %d)\n",
                    envelope->nSurfElems, envelope->ngrid, nq);

            fflush(stdout);
        }

        gmx_envelope_FourierTransform_low(envelope, q, nq, envelope->ftdens_re, envelope->ftdens_im, TRUE);

        envelope->bHaveSolventFT = TRUE;
    }

    /* return pointers to the FT (if != nullptr) */
    if (ft_re) {
        *ft_re = envelope->ftdens_re;
    }

    if (ft_im) {
        *ft_im = envelope->ftdens_im;
    }
}

gmx_bool envelope_have_solvent_ft(gmx_envelope* envelope) {
    return envelope->bHaveSolventFT;
}

double envelope_get_solvent_nelec(gmx_envelope* envelope) {
    if (envelope->nSolventStep == 0) {
        gmx_fatal(FARGS, "Cannot report number of electrons of solvent in envelope - not computed yet\n");
    }

    return envelope->solventNelecTotal;
}

}
