#pragma once

#include <gromacs/math/vec.h>  // rvec
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/utility/basedefinitions.h>  // gmx_bool
#include <gromacs/utility/real.h>  // real

#include <waxs/envelope/envelope.h>  // gmx_envelope

namespace swaxs {

/* Averaging density of solvation shell inside envelope and compute Fourier Transform */
/* cumulative average of solvent density
   tau specifies the exponentially decaying weight (tau = 0 means non-weighted average)

   scale = exp(-dt/tau) gives the decay of weights for cumulative average. scale == 1 means non-weighted average
   */
void envelope_solvent_density_next_frame(gmx_envelope* envelope, double scale);

void envelope_solvent_density_add_atom(gmx_envelope* envelope, const rvec x, double nElec);

void envelope_solvent_density_bcast(gmx_envelope* envelope, const t_commrec* cr);

/* Compute Fourier Transform of the envelope */
void envelope_unit_ft(gmx_envelope* envelope, rvec* q, int nq, real** ft_re, real** ft_im);

void envelope_solvent_ft(gmx_envelope* envelope, rvec* q, int nq, gmx_bool bRecalcFT, real** ft_re, real** ft_im);

gmx_bool envelope_have_solvent_ft(gmx_envelope* envelope);

double envelope_get_solvent_nelec(gmx_envelope* envelope);

}
