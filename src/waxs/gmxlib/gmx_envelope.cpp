/*
 *  This source file was written by Jochen Hub.
 */

#include "./gmx_envelope.h"

#include <gromacs/gmxlib/network.h>  // gmx_bcast
#include <gromacs/math/functions.h>  // gmx::square
#include <gromacs/math/vec.h>  // dcprod, diprod, dnorm2, dsvmul, dvec_*, dvec_add, dvec_sub, copy_ivec
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/gmxomp.h>  // gmx_omp_get_thread_num, gmx_omp_get_max_threads
#include <gromacs/utility/smalloc.h>  // snew, srenew, sfree
#include <gromacs/simd/simd_math.h>  // gmx::cos
#include <gromacs/pbcutil/pbc.h>  // ecenterTRIC, t_pbc

#include <waxs/envelope/envelope.h>  // gmx_envelope_init
#include <waxs/envelope/icosphere.h>  // icosphere_isInFace, icosphere_thisFace_to_CGO
#include <waxs/envelope/rvec.h>  // copy_rdvec, copy_drvec, ddet
#include <waxs/envelope/serialization.h>  // envelope_read_from_file, envelope_dump_triangle_to_cgo_file, envelope_dvec_to_cgo, swaxs::envelope_triangle_to_cgo
#include <waxs/gmxlib/gmx_miniball.h>  // gmx_miniball_*, NT

/* # of recusions done to build icosphere.
   ->  # of faces = 20 * 4^N
*/
#define WAXS_ENVELOPE_ICOSPHERE_NREC 4

// #define ENVELOPE_DEBUG

#if defined( ENVELOPE_DEBUG )
#define DEBUG_PRINTF(x) printf x
#else
#define DEBUG_PRINTF(x)
#endif

/* Given an arbitrary direction vec{x}, return where a*vec{x} crosses inner and outer surface */
static void gmx_envelope_x2OuterInner(gmx_envelope* envelope, const rvec xin, real* innerReturn, real* outerReturn) {
    dvec x;
    copy_rdvec(xin, x);

    int f = icosphere_x2faceID(envelope->ico, x);

    if (!envelope->surfElemOuter[f].bDefined) {
        // TODO: This should be extracted as serialization procedure.

        FILE* fp = fopen("env_error.py", "w");
        fprintf(fp, "from pymol.cgo import *\nfrom pymol import cmd\n\n");
        fprintf(fp, "obj = [\n\nCOLOR, %g, %g, %g,\n\n", 0.8, 0.8, 0.8);

        icosphere_thisFace_to_CGO(fp, envelope->ico, envelope->ico->face + f, 0.1, 1., 0.3, 0.3, 10.);
        swaxs::envelope_dvec_to_cgo(fp, x, 0.5);

        fprintf(fp, "]\n\ncmd.load_cgo(obj, 'error')\n\n");
        fclose(fp);

        fprintf(stderr, "\n\nDumped icopshere face and atom to cgo file env_error.py\n\n");

        gmx_fatal(FARGS, "Trying to get distance of x = %g / %g / %g from the surface, but the surface element %d"
                " is not defined\n", x[XX], x[YY], x[ZZ], f);
    }

    /* get xcross where the line along x crosses the surfElemOuter */
    double x_dot_n = diprod(x, envelope->surfElemOuter[f].normal);

    dvec xcross;
    dsvmul(envelope->surfElemOuter[f].c / x_dot_n, x, xcross);

    *outerReturn = sqrt(dnorm2(xcross));

    const double tol = 1e-2;

    if ((*outerReturn > (envelope->outer[envelope->surfElemOuter[f].ind[0]] + tol))
            and (*outerReturn > (envelope->outer[envelope->surfElemOuter[f].ind[1]] + tol))
            and (*outerReturn > (envelope->outer[envelope->surfElemOuter[f].ind[2]] + tol))) {
        gmx_fatal(FARGS, "Inconsistency error in gmx_envelope_x2OuterInner().\n");
    }

    if (envelope->bOriginInside) {
        *innerReturn = 0.;
    } else {
        x_dot_n = diprod(x, envelope->surfElemInner[f].normal);
        dsvmul(envelope->surfElemInner[f].c / x_dot_n, x, xcross);
        *innerReturn = sqrt(dnorm2(xcross));
    }
}

static gmx_bool gmx_envelope_withinInnerOuter_thisSurfElem(gmx_envelope* envelope, int f, const dvec x, double tol) {
    if (!envelope->surfElemOuter[f].bDefined) {
        return FALSE;
    }

    dvec xmc;
    dvec_sub(x, envelope->surfElemOuter[f].center, xmc);

    gmx_bool bBelowOuter = (diprod(envelope->surfElemOuter[f].normal, xmc) <= tol);

    gmx_bool bAboveInner = TRUE;

    if (!envelope->bOriginInside) {
        dvec_sub(x, envelope->surfElemInner[f].center, xmc);
        bAboveInner = (diprod(envelope->surfElemInner[f].normal, xmc) >= -tol);
    }

    return (bBelowOuter and bAboveInner);
}

gmx_bool gmx_envelope_isInside(gmx_envelope* envelope, const rvec xin) {
    real normx2 = norm2(xin);

    if (normx2 > gmx::square(envelope->maxOuter)) {
        return FALSE;
    }

    /** We cannot do the same with the envelope->minInner, since the surface triangle may be close to the origin than envelope->minInner */

    dvec x;
    copy_rdvec(xin, x);

    int f = icosphere_x2faceID(envelope->ico, x);

    return gmx_envelope_withinInnerOuter_thisSurfElem(envelope, f, x, 0.);
}

static void gmx_envelope_distToOuterInner(gmx_envelope* envelope, const rvec x, real* distInner, real* distOuter) {
    real normx = sqrt(norm2(x));

    real inner, outer;
    gmx_envelope_x2OuterInner(envelope, x, &inner, &outer);

    if (inner > 1e-5) {
        *distInner = normx - inner;
    } else {
        *distInner = 1e20;
    }

    *distOuter = outer - normx;
}

/**
 * Return distance and index of the atom, which is closest to the envelope surface bMinDistToOuter indicates
 * if the closest distance was found to an outer surface or to an inner surface.
 */
void gmx_envelope_minimumDistance(gmx_envelope* envelope, const gmx::ArrayRef<gmx::RVec> x, int* index, int isize,
        real* mindist, int* imin, gmx_bool* bMinDistToOuter) {
    real distInner, distOuter;
    *imin = -1;
    *mindist = 1e20;

    /* loop over atoms */
    for (int i = 0; i < isize; i++) {
        gmx_envelope_distToOuterInner(envelope, x[index[i]], &distInner, &distOuter);

        if (distOuter < -1e-5) {
            if (gmx_envelope_isInside(envelope, x[index[i]])) {
                gmx_fatal(FARGS, "Got distOuter = %g, but x is inside\n", distOuter);
            }
        }

        if (distInner < *mindist) {
            *mindist = distInner;
            *imin = i;
            *bMinDistToOuter = FALSE;
        }

        if (distOuter < *mindist) {
            *mindist = distOuter;
            *imin = i;
            *bMinDistToOuter = TRUE;
        }
    }
}

gmx_bool gmx_envelope_bInsideCompactBox(gmx_envelope* envelope, matrix Rinv, const matrix box, rvec boxToXRef, t_pbc* pbc,
        gmx_bool bVerbose, real tolerance) {
    int nOutside = 0;
    FILE* fp = nullptr;

    dvec dRay;
    rvec boxcenter, dxpbc, dxdirect, rRay, rRayRot, boxToRay;
    real small = box[XX][XX] / 10;

    calc_box_center(ecenterTRIC, box, boxcenter);

    for (int j = 0; j < envelope->nrays; j++) {
        if (envelope->isDefined[j]) {
            /* Get vertex relative to boxcenter */
            dsvmul(envelope->outer[j] + tolerance, envelope->r[j], dRay);
            copy_drvec(dRay, rRay);
            mvmul(Rinv, rRay, rRayRot);
            rvec_add(rRayRot, boxToXRef, boxToRay);

            /* Now check if the ray is inside the compact box */
            pbc_dx(pbc, boxToRay, boxcenter, dxpbc);
            rvec_sub(boxToRay, boxcenter, dxdirect);

            if (fabs(dxpbc[XX] - dxdirect[XX]) > small or fabs(dxpbc[YY] - dxdirect[YY]) > small
                    or fabs(dxpbc[ZZ] - dxdirect[ZZ]) > small) {
                if (bVerbose) {
                    if (fp == nullptr) {
                        // TODO: It should be extracted as serialization procedure.
                        fp = fopen("env_vertex_outside.py", "w");
                        fprintf(fp, "from pymol.cgo import *\nfrom pymol import cmd\n\n");
                        fprintf(fp, "obj = [\n\nCOLOR, %g, %g, %g,\n\n", 1., 0.0, 0.0);
                    }

                    swaxs::envelope_dvec_to_cgo(fp, dRay, 0.4);
                    nOutside++;

                    printf("Wrote ray %d, r = %g R = %g %g %g\n", j, envelope->outer[j], envelope->r[j][0],
                            envelope->r[j][1], envelope->r[j][2]);
                    printf("\t rRayRot   = %g %g %g\n", rRayRot[0], rRayRot[1], rRayRot[2]);
                    printf("\t boxToXRef = %g %g %g\n", boxToXRef[0], boxToXRef[1], boxToXRef[2]);
                    printf("\t boxToRay  = %g %g %g\n", boxToRay[0], boxToRay[1], boxToRay[2]);
                } else {
                    return FALSE;
                }
            }
        }
    }

    if (nOutside) {
        fprintf(fp, "]\n\ncmd.load_cgo(obj, 'vertex_outside')\n\n");
        fclose(fp);

        printf("Found %d vertices outside of the compact unitcell. Wrote Pymol cgo file env_vertex_outside.py\n\n", nOutside);

        return FALSE;
    }

    return TRUE;
}

/**
 * Gives the center and the radius of sphere bounding the envelope.
 * It is just a getter with a safety check.
 *
 * TODO: Move to envelope class.
 *
 * @param[in] envelope
 * @param[out] center Center of bounding sphere
 * @param[out] R2 Radius of bounding sphere
 */
void gmx_envelope_bounding_sphere(gmx_envelope* envelope, rvec center, real* R2) {
    if (!envelope->bHaveEnv) {
        gmx_fatal(FARGS, "Requested bounding sphere of the envelope, but the envelope is not yet constructed\n");
    }

    copy_drvec(envelope->bsphereCent, center);
    *R2 = envelope->bsphereR2;
}

double gmx_envelope_diameter(gmx_envelope* envelope) {
    if (!envelope->bHaveEnv) {
        gmx_fatal(FARGS, "Requested diameter of the envelope, but the envelope is not yet constructed\n");
    }

    return 2 * sqrt(envelope->bsphereR2);
}

void gmx_envelope_center_xyz(gmx_envelope* envelope, matrix Rinv, rvec cent) {
    rvec rRay, rRayRot;
    dvec dRay;
    rvec min = { 1e20, 1e20, 1e20 }, max = { -1e20, -1e20, -1e20 };

    for (int j = 0; j < envelope->nrays; j++) {
        if (envelope->isDefined[j]) {
            /* Get vertex relative to boxcenter */
            dsvmul(envelope->outer[j], envelope->r[j], dRay);
            copy_drvec(dRay, rRay);
            mvmul(Rinv, rRay, rRayRot);

            for (int d = 0; d < DIM; d++) {
                min[d] = (rRayRot[d] < min[d]) ? rRayRot[d] : min[d];
                max[d] = (rRayRot[d] > max[d]) ? rRayRot[d] : max[d];
            }
        }
    }

    for (int d = 0; d < DIM; d++) {
        cent[d] = (min[d] + max[d]) / 2;
    }
}

static void gmx_envelope_build_bounding_sphere(gmx_envelope* envelope) {
    /* NT is the floating point variable in gmx_miniball.c. It may be either real or double */
    NT *centNT, **xenvNT, subopt, relerr;

    snew(xenvNT, envelope->nrays);

    for (int j = 0; j < envelope->nrays; j++) {
        snew(xenvNT[j], DIM);
    }

    /* Need a linear NT* array with pointers to the envelope vertices */
    int ndef = 0;
    dvec xenv;

    for (int j = 0; j < envelope->nrays; j++) {
        if (envelope->isDefined[j]) {
            /* Get vertex vector from envelope reference point */
            dsvmul(envelope->outer[j], envelope->r[j], xenv);

            for (int d = 0; d < DIM; d++) {
                xenvNT[ndef][d] = xenv[d];
            }

            ndef++;
        }
    }

    gmx_miniball_t mb = gmx_miniball_init(DIM, xenvNT, ndef);

    if (!gmx_miniball_is_valid(mb, -1)) {
        /* In very rare cases, Miniball does even in double precision not fully converge. Check for approximate convergence. */
        relerr = gmx_miniball_relative_error(mb, &subopt);

        fprintf(stderr, "\nMiniball:  Relative error = %g"
                "\n           Suboptimality  = %g"
                "\n           Radius         = %g\n",
                relerr, subopt, sqrt(gmx_miniball_squared_radius(mb)));

        if (relerr < 1e-3 and subopt < 0.1) {
            fprintf(stderr, "\nWARNING, Miniball did not converged to the default of 10 x machine precision\n"
                    "\tbut to %f. This is not a problem (but worth mentioning).\n\n", relerr);
        } else if (relerr < 1e-3) {
            fprintf(stderr, "\nWARNING, Miniball generated a slighly sub-optimal bounding sphere (%f instead of 0).\n"
                    "\tThis is not a problem (but worth mentioning).\n\n", subopt);
        } else {
            fflush(stdout);
            fprintf(stderr, "\n\nThe generated Miniball is invalid.\n");

            FILE* fp = fopen("miniball_coords_error.dat", "w");

            for (int i = 0; i < envelope->nrays; i++) {
                fprintf(fp, "%.15f %.15f %.15f\n", xenvNT[i][XX], xenvNT[i][YY], xenvNT[i][ZZ]);
            }

            fclose(fp);

            fprintf(stderr, "Dumped %d coordinates that made an invalid Miniball to miniball_coords_error.dat\n",
                    envelope->nrays);
            fprintf(stderr, "Relative error = %g, suboptimality = %g\n", gmx_miniball_relative_error(mb, &subopt), subopt);

            gmx_fatal(FARGS, "Generating the bounding sphere with Miniball failed.\n");
        }
    }

    centNT = gmx_miniball_center(mb);

    for (int d = 0; d < DIM; d++) {
        envelope->bsphereCent[d] = centNT[d];
    }

    envelope->bsphereR2 = gmx_miniball_squared_radius(mb);

    for (int j = 0; j < envelope->nrays; j++) {
        sfree(xenvNT[j]);
    }

    sfree(xenvNT);
    gmx_miniball_destroy(&mb);
}

static void gmx_envelope_smoothEnvelope(gmx_envelope* envelope, double sigma) {
    if (envelope->bVerbose) {
        printf("Smoothing envelope with sigma = %g degree.\n", sigma * 180. / M_PI);
    }

    double cos_2sigma = gmx::cos(2 * sigma);
    double sigma2 = gmx::square(sigma);

    double* outer;
    double* inner;

    snew(outer, envelope->nrays);
    snew(inner, envelope->nrays);

    double w = 0.;

    for (int j = 0; j < envelope->nrays; j++) {
        double wsumo = 0.;
        double wsumi = 0.;
        double osum = 0.;
        double isum = 0.;

        if (envelope->outer[j] == 0.0) {
            outer[j] = envelope->outer[j];
            inner[j] = envelope->inner[j];
        } else {
            for (int k = 0; k < envelope->nrays; k++) {
                double cosphi = diprod(envelope->r[j], envelope->r[k]);

                /* cut-off at ~2sigma */
                if (cosphi > cos_2sigma and envelope->outer[k] != 0.0) {
                    /* We smooth such that the outer surface is only moved outwards, while
                       the inner surface is only moved inwards */
                    if (envelope->outer[k] >= envelope->outer[j] or envelope->inner[k] <= envelope->inner[j]) {
                        /* approximate phi^2 by Taylor series, OK because sigma is small. */
                        double phi2 = 2 * (1 - cosphi);
                        w = exp(-phi2 / (2 * sigma2));
                    }

                    if (envelope->outer[k] >= envelope->outer[j]) {
                        wsumo += w;
                        osum += w * envelope->outer[k];
                    }

                    if (envelope->inner[k] <= envelope->inner[j]) {
                        wsumi += w;
                        isum += w * envelope->inner[k];
                    }
                }
            }

            outer[j] = osum / wsumo;
            inner[j] = isum / wsumi;
        }
    }

    /* Store the smoothed outer/inner */
    for (int j = 0; j < envelope->nrays; j++) {
        envelope->outer[j] = outer[j];
        envelope->inner[j] = inner[j];
    }

    sfree(outer);
    sfree(inner);
}

static void gmx_envelope_set_surfaceElements(gmx_envelope* envelope, t_triangle* surfElem, double* R) {
    int ind[3];
    dvec x[3], vec1, vec2, tmp;
    dvec m[DIM];

    for (int f = 0; f < envelope->ico->nface; f++) {
        /* store corners and whether defined or not */
        copy_ivec(envelope->ico->face[f].v, surfElem[f].ind);
        copy_ivec(envelope->ico->face[f].v, ind);
        surfElem[f].iface = f;
        surfElem[f].bDefined = (envelope->isDefined[ind[0]] and envelope->isDefined[ind[1]] and envelope->isDefined[ind[2]]);

        if (!surfElem[f].bDefined) {
            continue;
        }

        /* corners and get center */
        clear_dvec(tmp);

        for (int i = 0; i < 3; i++) {
            dsvmul(R[ind[i]], envelope->r[ind[i]], x[i]);
            dvec_inc(tmp, x[i]);
        }

        dsvmul(1. / 3, tmp, surfElem[f].center);

        /* Get rmax and rmin */
        double rmax = 0.;
        double rmin = 1e20;

        for (int i = 0; i < 3; i++) {
            /* important: rmin / rmax are not the from the distance of the corners, but instead from the
               distance AFTER projecting onto the normal */
            double thisr = diprod(envelope->ico->face[f].normal, x[i]);

            rmax = (thisr > rmax) ? thisr : rmax;
            rmin = (thisr < rmin) ? thisr : rmin;
        }

        surfElem[f].rmax = rmax;
        surfElem[f].rmin = rmin;

        /* Get (normalized) normal vector and volume of pyramid */
        dvec_sub(x[1], x[0], vec1);
        dvec_sub(x[2], x[0], vec2);
        dcprod(vec1, vec2, tmp);

        double length = sqrt(dnorm2(tmp));

        double mult;

        if (diprod(surfElem[f].center, tmp) < 0) {
            mult = -1. / length;
        } else {
            mult = 1. / length;
        }

        dsvmul(mult, tmp, tmp);
        copy_dvec(tmp, surfElem[f].normal);
        surfElem[f].volPyramid = (length / 2) / 3 * diprod(surfElem[f].normal, x[0]);

        /* plane constant */
        surfElem[f].c = diprod(surfElem[f].normal, x[0]);

        /* Get the area of the triangle projected to a radius of r = 1 */
        dvec_sub(envelope->r[ind[1]], envelope->r[ind[0]], vec1);
        dvec_sub(envelope->r[ind[2]], envelope->r[ind[0]], vec2);
        dcprod(vec1, vec2, tmp);

        /* area is 0.5 times the area of the parallelogram spaned by two triangle sides */
        surfElem[f].area_r1 = 0.5 * sqrt(dnorm2(tmp));

        /* Get the space angle using Oosterom-and-Strackee algorithm using |r1| = |r2| = |r3| = 1 */
        double sum_iprods = 0.;

        for (int i = 0; i < 3; i++) {
            copy_dvec(envelope->r[ind[i]], m[i]);
            sum_iprods += diprod(envelope->r[ind[i]], envelope->r[ind[(i + 1) % 3]]);
        }

        double det1 = ddet(m);
        double tanOmegaHalf = (det1 < 0 ? -1 : 1) * det1 / (1 + sum_iprods);
        surfElem[f].spaceangle = 2 * atan(tanOmegaHalf);
    }
}

static double gmx_envelope_triangle_2_area(dvec a, dvec b, dvec c) {
    dvec vec1, vec2, normal;

    dvec_sub(a, b, vec1);
    dvec_sub(a, c, vec2);
    dcprod(vec1, vec2, normal);

    double A = 0.5 * sqrt(dnorm2(normal));

    if (A < 0) {
        A = -A;
    }

    return A;
}

/**
 * We know that the corners were added in clockwise (or counterclockwise) order, but a[0] and a[1] are for sure
 * not opposite -> can use Area = 0.5 * |q x p|, where q and p are the two diagonal vectors
 */
static double gmx_envelope_quadrangle_2_area(dvec a[]) {
    dvec p, q, tmp;

    dvec_sub(a[0], a[2], p);
    dvec_sub(a[1], a[3], q);

    dcprod(p, q, tmp);

    return 0.5 * sqrt(dnorm2(tmp));
}

static void linear_vec_comb2(dvec x0, double a, const dvec avec, double b, const dvec bvec, dvec res) {
    dvec tmp;

    copy_dvec(x0, res);
    dsvmul(a, avec, tmp);
    dvec_inc(res, tmp);
    dsvmul(b, bvec, tmp);
    dvec_inc(res, tmp);
}

static gmx_bool bNewCorner(dvec x, dvec corner[], int ncorn, double tol) {
    dvec v;
    double tol2 = gmx::square(tol);

    for (int i = 0; i < ncorn; i++) {
        dvec_sub(x, corner[i], v);

        if (dnorm2(v) <= tol2) {
            return FALSE;
        }
    }

    return TRUE;
}

static void gmx_envelope_binVolumes(gmx_envelope* envelope) {
    double tol;
    double tols[2] = { 1e-6, 1e-5 };

    if (envelope->bVerbose and FALSE) {
        printf("Generating volume bins of envelope...");
    }

    fflush(stdout);

    snew(envelope->binVol, envelope->nSurfElems);
    snew(envelope->xBinVol, envelope->nSurfElems);

    double* area;
    dvec* xcomArea;
    snew(area, envelope->nSurfElems + 1);
    snew(xcomArea, envelope->nSurfElems + 1);

    int nVolWarn = 0;
    double vtotalsum = 0.;

    for (int f = 0; f < envelope->nSurfElems; f++) {
        if (!envelope->surfElemOuter[f].bDefined) {
            continue;
        }

        if (!envelope->bOriginInside) {
            if (envelope->surfElemOuter[f].rmin < envelope->surfElemInner[f].rmax) {
                gmx_fatal(FARGS, "Volume of face %d of envelope is too skewed. (rminOuter = %g, rmaxInner = %g)\n",
                        f,
                        envelope->surfElemOuter[f].rmin, envelope->surfElemInner[f].rmax);
            }
        }

        snew(envelope->binVol[f], envelope->ngrid);
        snew(envelope->xBinVol[f], envelope->ngrid);

        double rmin = envelope->bOriginInside ? 0. : envelope->surfElemInner[f].rmin;
        double rmax = envelope->surfElemOuter[f].rmax;

        double dr = (rmax - rmin) / envelope->ngrid;

        ivec iray;
        double router[3], rinner[3];
        dvec xouter[3], xinner[3];

        for (int j = 0; j < 3; j++) {
            /* Corners of the volume (3 for the outer, and 3 for the inner end) */
            iray[j] = envelope->surfElemOuter[f].ind[j];
            dsvmul(envelope->outer[iray[j]], envelope->r[iray[j]], xouter[j]);
            router[j] = sqrt(dnorm2(xouter[j]));

            if (!envelope->bOriginInside) {
                dsvmul(envelope->inner[iray[j]], envelope->r[iray[j]], xinner[j]);
                rinner[j] = sqrt(dnorm2(xinner[j]));
            } else {
                clear_dvec(xinner[j]);
                rinner[j] = 0.;
            }
        }

        /* We have ngrid volumes and ngrid + 1 areas (parallel to the faces of the Ikosphere) that cover
         * the volumes above and below. This loop computes the areas */
        for (int i = 0; i < envelope->ngrid + 1; i++) {
            /* Take radii at the bottom (or top) of the volumina (not at the center) */
            double r = rmin + i * dr;
            dvec x[3];

            /* Intersections of the 3 rays with the plane (vec{n}*vec{x} = r) */
            for (int j = 0; j < 3; j++) {
                double a = r / diprod(envelope->ico->face[f].normal, envelope->r[iray[j]]);
                dsvmul(a, envelope->r[iray[j]], x[j]);
            }

            /* First get area of the triangle of 3 intersections of rays with plane (vec{n}*vec{x} = r) */
            double area_r = gmx_envelope_triangle_2_area(x[0], x[1], x[2]);

            /* normalized vector towards the midpoint of the triangle */
            double length = sqrt(dnorm2(envelope->surfElemOuter[f].center));

            dvec xcentNorm;
            dsvmul(1. / length, envelope->surfElemOuter[f].center, xcentNorm);

            const int nAreaGrid = 50;

            if (r <= (envelope->surfElemOuter[f].rmin + 1e-6) and (envelope->bOriginInside or r >= (envelope->surfElemInner[f].rmax - 1e-6))) {
                /* The normal case. The normal plane simply crosses the 3 rays. */
                area[i] = area_r;
                dsvmul(r, xcentNorm, xcomArea[i]);
            } else {
                /* Do a simple grid search over the triangle to estimate the area. Anything else is too complicated.
                 * Use barycentric coordinates and split each side of the triangle into nAreaGrid. This way,
                 * you get nAreaGrid^2 little triangles. */
                dvec u, v;
                dvec_sub(x[1], x[0], u);
                dvec_sub(x[2], x[0], v);

                dvec du, dv;
                dsvmul(1. / nAreaGrid, u, du);
                dsvmul(1. / nAreaGrid, v, dv);

                int ntriangles = 0;
                int nInside = 0;

                clear_dvec(xcomArea[i]);
                tol = (i == 0 or i == envelope->ngrid) ? 1e-5 : 0.;

                for (int j = 0; j < nAreaGrid; j++) {
                    for (int k = 0; k < (nAreaGrid - j + nAreaGrid - j - 1); k++) {
                        double xu, xv;

                        if (k < (nAreaGrid - j)) {
                            /* first row - upright little triangles */
                            xv = (1. / 3 + j);
                            xu = (1. / 3 + k);
                        } else {
                            /* second row - upside down little trigles */
                            xv = (2. / 3 + j);
                            xu = (2. / 3 + k - (nAreaGrid - j));
                        }

                        /* y = x0 + xv*dv + xu*du */
                        dvec y;
                        linear_vec_comb2(x[0], xv, dv, xu, du, y);

                        if (gmx_envelope_withinInnerOuter_thisSurfElem(envelope, f, y, tol)) {
                            dvec_inc(xcomArea[i], y);
                            nInside++;
                        }

                        ntriangles++;
                    }
                }

                if (ntriangles != nAreaGrid * nAreaGrid) {
                    gmx_fatal(FARGS, "Inconsistency while doing grid over triangle\n");
                }

                if (nInside == 0) {
                    /* If none of the triangle grids was inside, we have to go over the corners.
                       This may happen at the very first or very last point */
                    for (int j = 0; j < 3; j++) {
                        if (gmx_envelope_withinInnerOuter_thisSurfElem(envelope, f, x[j], 1e-6)) {
                            dvec_inc(xcomArea[i], x[j]);
                            nInside++;
                        }
                    }
                }

                if (nInside == 0) {
                    FILE* fp = fopen("env_error.py", "w");

                    fprintf(fp, "from pymol.cgo import *\nfrom pymol import cmd\n\n");
                    fprintf(fp, "obj = [\n\nCOLOR, %g, %g, %g,\n\n", 0.8, 0.8, 0.8);

                    swaxs::envelope_triangle_to_cgo(envelope, fp, f, 0.08, 1, 0, 0);

                    swaxs::envelope_dvec_to_cgo(fp, x[0], 0.2);
                    swaxs::envelope_dvec_to_cgo(fp, x[1], 0.2);
                    swaxs::envelope_dvec_to_cgo(fp, x[2], 0.2);

                    fprintf(fp, "]\n\ncmd.load_cgo(obj, 'error')\n\n");
                    fclose(fp);

                    gmx_fatal(FARGS, "Still nInside == 0. ibin = %d of %d, r = %g\n", i, envelope->ngrid, r);
                }

                dsvmul(1. / nInside, xcomArea[i], xcomArea[i]);
                double areaGrid = 1.0 * nInside / ntriangles * area_r;

                /* The second way - should be better than the grid search */

                dvec corner[9];
                int ncorners;

                for (std::size_t itol = 0; itol < sizeof(tols) / sizeof(tols[0]); itol++) {
                    tol = tols[itol];
                    ncorners = 0;

                    for (int j = 0; j < 3; j++) {
                        double rray = sqrt(dnorm2(x[j]));

                        if ((rinner[j] - tol) <= rray and rray <= (router[j] + tol)
                                and bNewCorner(x[j], corner, ncorners, 2 * tol)) {
                            copy_dvec(x[j], corner[ncorners++]);
                        }

                        int jj = (j + 1) % 3;
                        /* Search intersection of the vec{n}*vec{x} = r plane with the edges of the triangle */

                        if (r >= envelope->surfElemOuter[f].rmin - tol) {
                            dvec v;
                            dvec_sub(xouter[jj], xouter[j], v);

                            double n_dot_v = diprod(envelope->ico->face[f].normal, v);

                            double a;

                            if (fabs(n_dot_v) < 1e-15) {
                                a = 1e15;
                            } else {
                                a = (r - diprod(envelope->ico->face[f].normal, xouter[j])) / n_dot_v;
                            }

                            if (-tol <= a and a <= 1. + tol) {
                                dvec vtmp;
                                dsvmul(a, v, vtmp);

                                dvec xIntersect;
                                dvec_add(xouter[j], vtmp, xIntersect);

                                if (bNewCorner(xIntersect, corner, ncorners, 2 * tol)) {
                                    copy_dvec(xIntersect, corner[ncorners++]);
                                }
                            }
                        } else if (!envelope->bOriginInside and r <= envelope->surfElemInner[f].rmax + tol) {
                            dvec v;
                            dvec_sub(xinner[jj], xinner[j], v);

                            double n_dot_v = diprod(envelope->ico->face[f].normal, v);

                            double a;

                            if (fabs(n_dot_v) < 1e-15) {
                                a = 1e15;
                            } else {
                                a = (r - diprod(envelope->ico->face[f].normal, xinner[j])) / n_dot_v;
                            }

                            if (-tol < a and a < 1. + tol) {
                                dvec vtmp;
                                dsvmul(a, v, vtmp);

                                dvec xIntersect;
                                dvec_add(xinner[j], vtmp, xIntersect);

                                if (bNewCorner(xIntersect, corner, ncorners, tol)) {
                                    copy_dvec(xIntersect, corner[ncorners++]);
                                }
                            }
                        }
                    }

                    if (ncorners == 1 or ncorners == 2) {
                        area[i] = 0.;
                        break;
                    } else if (ncorners == 3) {
                        area[i] = gmx_envelope_triangle_2_area(corner[0], corner[1], corner[2]);
                        break;
                    } else if (ncorners == 4) {
                        area[i] = gmx_envelope_quadrangle_2_area(corner);
                        break;
                    } else {
                        if (itol == sizeof(tols) / sizeof(tols[0]) - 1) {
                            gmx_fatal(FARGS, "Found %d corners for face %d, r = %g, bin %d of %d\n"
                                      "In this face: envelope->surfElemOuter[f].rmax %g \n"
                                      "R of ray intersections with normal: %g %g %g\n"
                                      "R of outer corners: %g %g %g\n"
                                      "diff = %g %g %g"
                                      "diff(with tol) = %g %g %g",
                                      ncorners, f, r, i, envelope->ngrid, envelope->surfElemOuter[f].rmax,
                                      sqrt(dnorm2(x[0])), sqrt(dnorm2(x[1])), sqrt(dnorm2(x[2])),
                                      router[0], router[1], router[2],
                                      router[0] - sqrt(dnorm2(x[0])), router[1] - sqrt(dnorm2(x[1])), router[2] - sqrt(dnorm2(x[2])),
                                      router[0] + tol - sqrt(dnorm2(x[0])), router[1] + tol - sqrt(dnorm2(x[1])),
                                      router[2] + tol - sqrt(dnorm2(x[2])));
                        } else {
                            fprintf(stderr, "\nNOTE: While getting volume bins of envelope face %d, r = %g, bin %d of %d:\n"
                                    "\t%d corners found with tolerance = %g. Now try with %g\n\n",
                                    f, r, i, envelope->ngrid, ncorners, tol, tols[itol + 1]);
                        }
                    }
                } /* loop itol over tolerances */

                if (fabs(area[i] - areaGrid) > 1e-2 * (area[i] + areaGrid) and fabs(area[i] - areaGrid) > 1e-1) {
                    FILE* fp = fopen("env_error.py", "w");

                    fprintf(fp, "from pymol.cgo import *\nfrom pymol import cmd\n\n");
                    fprintf(fp, "obj = [\n\nCOLOR, %g, %g, %g,\n\n", 0.8, 0.8, 0.8);

                    swaxs::envelope_triangle_to_cgo(envelope, fp, f, 0.08, 1, 0, 0);

                    swaxs::envelope_dvec_to_cgo(fp, x[0], 0.15);
                    swaxs::envelope_dvec_to_cgo(fp, x[1], 0.15);
                    swaxs::envelope_dvec_to_cgo(fp, x[2], 0.15);

                    fprintf(fp, "COLOR, 0., 1., 0.,\n");

                    for (int j = 0; j < ncorners; j++) {
                        swaxs::envelope_dvec_to_cgo(fp, corner[j], 0.2);

                        printf("Added corner %g %g %g to env_error.py\n", corner[j][0], corner[j][1], corner[j][2]);
                    }

                    fprintf(fp, "]\n\ncmd.load_cgo(obj, 'error')\n\n");
                    fclose(fp);

                    fprintf(stderr, "Cross section area of volume bin %d of %d: area from grid and from intersections do not"
                            " agree : %g -- %g\n"
                            "r = %g, %d corners found\n"
                            "nInside = %d of %d", i, envelope->ngrid, areaGrid, area[i], r, ncorners, nInside, ntriangles);
                }
            }

            if (!gmx_envelope_withinInnerOuter_thisSurfElem(envelope, f, xcomArea[i], 1e-5)) {
                swaxs::envelope_dump_triangle_to_cgo_file(envelope, f, xcomArea[i], "env_error.py");

                gmx_fatal(FARGS, "Center of area bin (%d, r = %g) x = (%g / %g / %g, r = %g) is not between inner and "
                        "outer surface of face %d (rmin = %g, rmax = %g)\n",
                        i, r, xcomArea[i][XX], xcomArea[i][YY], xcomArea[i][ZZ], sqrt(dnorm2(xcomArea[i])), f, rmin, rmax);
            }

            if (icosphere_isInFace(envelope->ico, &(envelope->ico->face[f]), xcomArea[i], 1e-5, nullptr) <= 0) {
                gmx_fatal(FARGS, "Center of area bin (%d, r = %g) x = (%g / %g / %g, r = %g) is not inside face %d\n",
                        i, r, xcomArea[i][XX], xcomArea[i][YY], xcomArea[i][ZZ], sqrt(dnorm2(xcomArea[i])), f);
            }
        } /* end i loop over volume grid */

        /* Store the volumina */
        double vsum = 0.;

        for (int i = 0; i < envelope->ngrid; i++) {
            envelope->binVol[f][i] = 0.5 * dr * (area[i] + area[i + 1]);
            vsum += envelope->binVol[f][i];

            if (envelope->binVol[f][i] <= 0) {
                gmx_fatal(FARGS, "Strange, got a <= volueme (%g) for f = %d, i = %d \n", envelope->binVol[f][i], f, i);
            }

            dvec v;
            dvec_add(xcomArea[i], xcomArea[i + 1], v);

            /* double to real conversion */
            envelope->xBinVol[f][i][XX] = 0.5 * v[XX];
            envelope->xBinVol[f][i][YY] = 0.5 * v[YY];
            envelope->xBinVol[f][i][ZZ] = 0.5 * v[ZZ];

            if (icosphere_isInFace(envelope->ico, &(envelope->ico->face[f]), v, 1e-3, nullptr) <= 0) {
                gmx_fatal(FARGS, "Center of volume bin %d (face %d) is not within this face: x = %g / %g / %g\n",
                        i, f,
                        envelope->xBinVol[f][i][XX], envelope->xBinVol[f][i][YY], envelope->xBinVol[f][i][ZZ]);
            }
        }

        vtotalsum += vsum;

        double vexpect = envelope->surfElemOuter[f].volPyramid;

        if (!envelope->bOriginInside) {
            vexpect -= envelope->surfElemInner[f].volPyramid;
        }

        /* Check releative difference betwwen the two ways to compute the volumen, as compared to the
           average volume of the volume bins */
        double relVolDiff = fabs(vsum - vexpect) / (envelope->vol / (envelope->nSurfElems * envelope->ngrid));

        if (relVolDiff > 1e-2) {
            nVolWarn++;
        }

        if (relVolDiff > 1e-2 and nVolWarn == 1 and envelope->bVerbose and FALSE) {
            fprintf(stderr, "\n\nWARNING --- In face %d:"
                    "\n\tSum of volumina of volume bins (%g) does not equal the "
                    "volume of the pyramides (%g) (rel. diff = %g %%)\n\tConsider using a deeper recusion level deeper than %d "
                    "for the envelope's ikosphere.\n\n", f, vsum, vexpect, 100 * fabs(vsum - vexpect) / vexpect, envelope->nrec);
        }

        if (fabs(vsum - vexpect) / vexpect > 1e-1) {
            gmx_fatal(FARGS, "Sum of volumina of volume bins (%g) does not equal the volume from pyramides (%g) (rel diff = %g %%)\n",
                    vsum, vexpect, 100 * fabs(vsum - vexpect) / vexpect);
        }
    } /* End of loop over faces */

    if (nVolWarn > 0 and envelope->bVerbose and FALSE) {
        printf("\nWARNING - There were %d warnings in total because the sum of volume bins did not\n equal the result "
               "from the pyramides.\n\n", nVolWarn);
    }

    double relVolDiff = envelope->vol / vtotalsum;

    if (envelope->bVerbose) {
        printf("Total volume of envelope: exact = %g, from bins = %g (%g %% difference)\n"
               "\t -> scaling volume bins by %g\n",
                envelope->vol, vtotalsum, 100 * (envelope->vol - vtotalsum) / envelope->vol, relVolDiff);
    }

    for (int f = 0; f < envelope->nSurfElems; f++) {
        if (envelope->surfElemOuter[f].bDefined) {
            for (int i = 0; i < envelope->ngrid; i++) {
                envelope->binVol[f][i] *= relVolDiff;
            }
        }
    }

    sfree(area);
    sfree(xcomArea);
}

/**
 * assign isDefined of 1 to a ray if inner and outer surface are defined, and set maxOuter and minInner
 */
void gmx_envelope_setStats(gmx_envelope* envelope) {
    envelope->minInner = 1e20;
    envelope->minOuter = 1e20;
    envelope->maxInner = 0.;
    envelope->maxOuter = 0.;
    envelope->nDefined = 0;

    for (int j = 0; j < envelope->nrays; j++) {
        envelope->minInner = (envelope->inner[j] < envelope->minInner) ? envelope->inner[j] : envelope->minInner;
        envelope->maxInner = (envelope->inner[j] > envelope->maxInner) ? envelope->inner[j] : envelope->maxInner;

        envelope->minOuter = (envelope->outer[j] < envelope->minOuter) ? envelope->outer[j] : envelope->minOuter;
        envelope->maxOuter = (envelope->outer[j] > envelope->maxOuter) ? envelope->outer[j] : envelope->maxOuter;

        if (envelope->outer[j] > 0.) {
            envelope->isDefined[j] = TRUE;
            envelope->nDefined++;

            if (envelope->inner[j] > envelope->outer[j]) {
                gmx_fatal(FARGS, "Envelope error, ray %d: outer is %g, but inner is %g\n", j, envelope->outer[j],
                        envelope->inner[j]);
            }
        } else {
            envelope->isDefined[j] = FALSE;
        }
    }

    if (envelope->nDefined < 0.2 * envelope->nrays and envelope->bVerbose) {
        fprintf(stderr, "\n\nWARNING -- Only %.2f %% of envelope rays (%d rays) are defined.\n"
                "\tMaybe your solute is far away from the origin?\n\n",
                100. * envelope->nDefined / envelope->nrays, envelope->nDefined);
    }

    if (envelope->nDefined == 0) {
        gmx_fatal(FARGS, "No envelope rays are defined - something went wrong\n");
    }

    if (envelope->maxInner == 0.0) {
        /* set in case that that the envelope was not build on this node */
        envelope->bOriginInside = TRUE;
    } else {
        envelope->bOriginInside = FALSE;
    }

    if (envelope->surfElemOuter == nullptr) {
        envelope->surfElemOuter = new t_triangle[envelope->nSurfElems];
    }

    gmx_envelope_set_surfaceElements(envelope, envelope->surfElemOuter, envelope->outer);

    if (!envelope->bOriginInside) {
        if (envelope->surfElemInner == nullptr) {
            envelope->surfElemInner = new t_triangle[envelope->nSurfElems];
        }

        gmx_envelope_set_surfaceElements(envelope, envelope->surfElemInner, envelope->inner);
    }

    /* Compute total volume */
    double vsum = 0.;

    for (int f = 0; f < envelope->nSurfElems; f++) {
        if (envelope->surfElemOuter[f].bDefined) {
            vsum += envelope->surfElemOuter[f].volPyramid;
        }

        if (!envelope->bOriginInside and envelope->surfElemInner[f].bDefined) {
            vsum -= envelope->surfElemInner[f].volPyramid;
        }
    }

    envelope->vol = vsum;

    if (envelope->bVerbose) {
        printf("Volume of envelope [nm3] = %12.8g\n", envelope->vol);
    }

    gmx_envelope_binVolumes(envelope);

    gmx_envelope_build_bounding_sphere(envelope);
}

static void gmx_envelope_bcast(gmx_envelope* envelope, const t_commrec* cr) {
    if (MASTER(cr) and envelope->bHaveEnv == FALSE) {
        gmx_fatal(FARGS, "Trying to bcast envelope to slaves, but the envelope is not defined on the Master\n");
    }

    for (int j = 0; j < envelope->nrays; j++) {
        gmx_bcast(envelope->nrays * sizeof(double), envelope->inner, cr->mpi_comm_mygroup);
        gmx_bcast(envelope->nrays * sizeof(double), envelope->outer, cr->mpi_comm_mygroup);
    }

    if (!envelope->bHaveEnv) {
        gmx_envelope_setStats(envelope);
    }

    envelope->bHaveEnv = TRUE;
}

gmx_envelope* gmx_envelope_init_md(int nrecReq, const t_commrec* cr, gmx_bool bVerbose) {
    char* envfile = nullptr;
    gmx_bool bEnvFile;

    if (MASTER(cr)) {
        envfile = getenv("GMX_ENVELOPE_FILE");
        bEnvFile = (envfile != nullptr);
    }

    if (PAR(cr)) {
        gmx_bcast(sizeof(gmx_bool), &bEnvFile, cr->mpi_comm_mygroup);
    }

    gmx_envelope* envelope = nullptr;
    int nrec;

    if (!bEnvFile) {
        /* Only init the envelope, don't have the envelope yet. */
        if (nrecReq < 0) {
            nrecReq = WAXS_ENVELOPE_ICOSPHERE_NREC;
        }

        envelope = gmx_envelope_init(nrecReq, bVerbose);
    } else {
        /* If we read the envelope from a file, we bcast it to all the nodes immediately */
        if (MASTER(cr)) {
            printf("\nFound environment variable GMX_ENVELOPE_FILE -- reading envelope from file %s\n", envfile);

            envelope = swaxs::envelope_read_from_file(envfile);
            nrec = envelope->nrec;

            if (nrecReq >= 0 and nrec != nrecReq) {
                fprintf(stderr, "\n\nWARNING -- requested to build an envelope with recusion level %d\n"
                        "\tFile %s contains %d recursion levels, however\n\n", nrecReq, envfile, nrec);
            }
        }

        if (PAR(cr)) {
            gmx_bcast(sizeof(int), &nrec, cr->mpi_comm_mygroup);
        }

        if (!MASTER(cr)) {
            envelope = gmx_envelope_init(nrec, bVerbose);
        }

        if (PAR(cr)) {
            gmx_envelope_bcast(envelope, cr);
        }

        if (MASTER(cr) and PAR(cr)) {
            printf("Broadcast envelope to all the nodes\n");
        }
    }

    return envelope;
}

void gmx_envelope_superimposeEnvelope(gmx_envelope* envelope_add, gmx_envelope* envelope_base) {
    /* Safety checks */
    if (!envelope_base->bHaveEnv or !envelope_add->bHaveEnv) {
        gmx_fatal(FARGS, "The envelopes are not defined for at least one of the inputs!\n");
    }

    if (envelope_base->nrec != envelope_add->nrec) {
        fprintf(stderr, "The recursion levels of the two envelopes are different!\n");
        gmx_fatal(FARGS, "Bailing out.\n");
    }

    /* Now should have the same number of rays in both. */
    int tot = envelope_base->nrays;

    for (int i = 0; i < tot; i++) {
        envelope_base->outer[i] = (envelope_base->outer[i] < envelope_add->outer[i]) ? envelope_add->outer[i] : envelope_base->outer[i];
    }

    /* Also update inner if required. Otherwise always 0. */
    if (!envelope_base->bOriginInside) {
        for (int i = 0; i < tot; i++) {
            envelope_base->inner[i] = (envelope_base->inner[i] > envelope_add->inner[i]) ? envelope_add->inner[i] : envelope_base->inner[i];
        }
    }

    gmx_envelope_setStats(envelope_base);
}

void gmx_envelope_buildSphere(gmx_envelope* envelope, real rad) {
    for (int i = 0; i < envelope->nrays; i++) {
        envelope->inner[i] = 0.;
        envelope->outer[i] = rad;
    }

    envelope->bHaveEnv = TRUE;

    gmx_envelope_setStats(envelope);
}

void gmx_envelope_buildEllipsoid(gmx_envelope* envelope, rvec r) {
    gmx_fatal(FARGS, "Building an ellipsoindal envelope is currently not implemented\n");

    for (int i = 0; i < envelope->nrays; i++) {
        /* This seems wrong: */
        envelope->inner[i] = 0.;
        envelope->outer[i] = 0;
    }

    envelope->bHaveEnv = TRUE;

    /* Remove warning for now */
    r[0] = 0;

    gmx_envelope_setStats(envelope);
}

/* We want to use an envelope with an inner and outer radius only if there is a major hole
 * in the center of the solute as, for instance, for a virus particle. Hence, request that
 * there is no atom within ORIGIN_OUTSIDE_ENVELOPE_THREASHOLD + d_envelope for setting
 * bOriginInside to false.
*/
#define ORIGIN_OUTSIDE_ENVELOPE_THREASHOLD 0.5

void gmx_envelope_buildEnvelope_omp(gmx_envelope* envelope, gmx::ArrayRef<gmx::RVec> x, int* index, int isize, real dGiven,
        real phiSmooth, gmx_bool bSphere, real* vdwRadii, int nRadii) {
    dvec* r = envelope->r;

    envelope->d = dGiven;

    /* Consistenty checks for vdw radii */
    if ((vdwRadii and nRadii < 1) or (!vdwRadii and nRadii > 0)) {
        gmx_incons("vdwRadii != nullptr but nRadii < 0, or vice versa");
    }

    if (vdwRadii and (isize % nRadii) != 0) {
        gmx_fatal(FARGS, "In gmx_envelope_buildEnvelope_omp(): isize = %d, but nRadii = %d,\n"
                "but isize must be an integer multiple of nRadii", isize, nRadii);
    }

    /* first check if the origin is within the envelope */
    if (envelope->bOriginInside == FALSE) {
        for (int i = 0; i < isize; i++) {
            double vdwRadius = vdwRadii ? vdwRadii[i % nRadii] : 0.0;
            double d2 = gmx::square(envelope->d + vdwRadius + ORIGIN_OUTSIDE_ENVELOPE_THREASHOLD);

            if (norm2(x[index[i]]) < d2) {
                envelope->bOriginInside = TRUE;
                break;
            }
        }
    }

    if (!envelope->bOriginInside) {
        printf("\nNote: The origin is not within the envelope\n\n");
    }

    if (envelope->bVerbose) {
        printf("\nBuilding envelope around %d atoms\n", isize);
    }

    /* Get largest VdW radius, used to skip atoms below */
    double maxVdwRadius = 0;

    if (vdwRadii) {
        for (int i = 0; i < isize; i++) {
            if (vdwRadii[i % nRadii] > maxVdwRadius) {
                maxVdwRadius = vdwRadii[i % nRadii];
            }
        }
    }

    /* loop over atoms */

    #pragma omp parallel shared(x, index, envelope, r, vdwRadii, nRadii)
    {
        /* Variables private to each thread: */
        double discr, sqrtDiscr, xnorm2, xnorm, inv_xnorm, l1, l2, cosGamma, r_dot_x, *outer, *inner, minOuter = 0, tmp;
        double d2;
        dvec xd, xdNorm;
        int threadID = gmx_omp_get_thread_num();
        int nThreads = gmx_omp_get_max_threads();

        snew(outer, envelope->nrays);
        snew(inner, envelope->nrays);

        for (int j = 0; j < envelope->nrays; j++) {
            outer[j] = 0.;
            inner[j] = 1e20;
        }

        #pragma omp for
        for (int i = 0; i < isize; i++) {
            copy_rdvec(x[index[i]], xd);
            xnorm2 = dnorm2(xd);
            xnorm = sqrt(xnorm2);
            dsvmul(1. / xnorm, xd, xdNorm);

            double vdwradius = vdwRadii ? vdwRadii[i % nRadii] : 0.0;
            d2 = gmx::square(envelope->d + vdwradius);

            /* Check if we can skip this atom */
            if (envelope->bOriginInside and (xnorm + vdwradius + envelope->d) < minOuter) {
                continue;
            }

            inv_xnorm = 1.0 / xnorm;
            // printf("Envelope: x = %g %g %g\n", x[index[i]][XX], x[index[i]][YY], x[index[i]][ZZ]);

            if (envelope->bVerbose and (((i + 1) % 10000) == 0 or (i == isize - 1)) and threadID == 0) {
                printf("\r        %8.1f %% done", 100. * (i + 1) / (isize / nThreads));
                fflush(stdout);
            }

            if (((i + 1) % 5000) == 0) {
                /* Every 5000th atom, get the minimum outer surface - used to skip atoms
                   with r < minOuter-d */
                minOuter = 1e20;

                for (int j = 0; j < envelope->nrays; j++) {
                    minOuter = (outer[j] < minOuter) ? outer[j] : minOuter;
                }
            }

            /* loop over envelope rays */
            for (int j = 0; j < envelope->nrays; j++) {
                /* Angle between x and r */
                r_dot_x  = diprod(r[j], xd);
                cosGamma = r_dot_x*inv_xnorm;
                /* Intersection of envelope ray with the sphere of radius d round x.
                   Use law of cosine:

                   l[1,2] = x.cos(gamma) +- sqrt[ (x.cos(gamma))^2 - (x^2-d^2) ]
                */
                discr = gmx::square(r_dot_x) - (xnorm2-d2);

                /* The origin is NOT within the sphere of radius d round x
                   -> may bet 2 solutions */
                if (xnorm2 >= d2) {
                    /* The origin is NOT within the sphere of radius d round x
                       -> ray crosses the sphere only if gamma<90degree, that is cosGamma > 0
                       -> have 2 solutions if discr > 0 (discr == 0 case is ignored) */

                    if (discr > 0. and cosGamma > 0.) {
                        sqrtDiscr = sqrt(discr);
                        l1 = r_dot_x - sqrtDiscr;
                        l2 = r_dot_x + sqrtDiscr;

                        #ifdef ENVELOPE_DEBUG
                            if (l1 < 0. or l2 < 0.) {
                                gmx_fatal(FARGS, "Error while building envelope, found negative distance from"
                                        "origin:\n l1 = %g, l2 = %g  (|x|*cos(gamma) = %g, sqrt(discr) = %g\n"
                                        "cosGamma = %g", l1, l2, xnorm * cosGamma, sqrtDiscr, cosGamma);
                            }

                            if (l1 > l2) {
                                gmx_fatal(FARGS, "Error while constructing envelope: l1 > l2: %g > %g\n", l1, l2);
                            }
                        #endif

                        inner[j] = (l1 < inner[j]) ? l1 : inner[j];
                        outer[j] = (l2 > outer[j]) ? l2 : outer[j];
                    }
                } else {
                    /* The origin is WITHIN the sphere of radius d around x
                       -> want only the positive solution of the law of cosine */
                    l2 = r_dot_x + sqrt(discr);

                    #ifdef ENVELOPE_DEBUG
                        if (l2 < 0.) {
                            gmx_fatal(FARGS, "Error while building envelope, found negative distance from"
                                    "origin:\n l2 = %g (|x|*cos(gamma) = %g, sqrt(discr) = %g\n"
                                    "cosGamma = %g", l2, xnorm * cosGamma, sqrt(discr), cosGamma);
                        }
                    #endif

                    /* everything between 0 and l2 is withing protein */
                    inner[j] = 0.;
                    outer[j] = (l2 > outer[j]) ? l2 : outer[j];
                }
            }
        }

        /* Now collect maximum outer[] / minimal inner[] from the threads */
        for (int j = 0; j < envelope->nrays; j++) {
            tmp = outer[j];

            #pragma omp flush(tmp)
            if (tmp > envelope->outer[j]) {
                #pragma omp critical
                {
                    if (tmp > envelope->outer[j]) {
                        envelope->outer[j] = tmp;
                    }
                }
            }

            tmp = inner[j];

            #pragma omp flush(tmp)
            if (tmp < envelope->inner[j]) {
                #pragma omp critical
                {
                    if (tmp < envelope->inner[j]) {
                        envelope->inner[j] = tmp;
                    }
                }
            }
        }

        sfree(inner);
        sfree(outer);
    } /* end omp pagma */

    if (envelope->bOriginInside) {
        for (int j = 0; j < envelope->nrays; j++) {
            /* In the main loop above, we may have skipped a few atoms that are far inside of the envelope,
               because we knew already that bOriginInside = TRUE. Consequently, inner[j] may not be zero here,
               and we must fix this (example is PDB code 1RJW with g_genenv -d 0.7): */
            envelope->inner[j] = 0.;
        }
    }

    /* For all faces, the radii for ALL three inner corners must be smaller than the
     * radii of all three outer corners. Otherwise, our method to construct the volume bins
     * fails. Check for this:
     */
    for (int f = 0; f < envelope->ico->nface; f++) {
        double innerMax = 0;
        double outerMin = 1e20;

        for (int i = 0; i < 3; i++) {
            int j = envelope->ico->face[f].v[i];
            innerMax = (envelope->inner[j] > innerMax) ? envelope->inner[j] : innerMax;
            outerMin = (envelope->outer[j] < outerMin) ? envelope->outer[j] : outerMin;
        }

        if (outerMin < innerMax and innerMax < 0.9e20 and outerMin > 0) {
            printf("\nThe pyramide of face %d is highly skewed (innerMax = %g - outerMin = %g), "
                   "\treducing the inner radii to %g\n", f, innerMax, outerMin, outerMin);

            printf("Radii of inner = ");

            for (int i = 0; i < 3; i++) {
                printf(" %10g", envelope->inner[envelope->ico->face[f].v[i]]);
            }

            printf("\n");
            printf("Radii of outer = ");

            for (int i = 0; i < 3; i++) {
                printf(" %10g", envelope->outer[envelope->ico->face[f].v[i]]);
            }

            printf("\n");

            for (int i = 0; i < 3; i++) {
                int j = envelope->ico->face[f].v[i];

                if (envelope->inner[j] > outerMin) {
                    envelope->inner[j] = outerMin - GMX_FLOAT_EPS;
                }
            }
        }
    }

    if (bSphere) {
        double maxR = 0.;

        for (int j = 0; j < envelope->nrays; j++) {
            envelope->inner[j] = 0.;
            maxR = (envelope->outer[j] > maxR) ? envelope->outer[j] : maxR;
        }

        for (int j = 0; j < envelope->nrays; j++) {
            envelope->outer[j] = maxR;
        }
    }

    if (isize > 50000 and envelope->bVerbose) {
        printf("\n");
    }

    if (!envelope->bOriginInside) {
        for (int j = 0; j < envelope->nrays; j++) {
            if (envelope->outer[j] > 0. and envelope->inner[j] == 0.) {
                gmx_fatal(FARGS, "ray %d: outer = %g, inner = %g, this is not possible when the origin is not within "
                        "the envelope\n", j, envelope->outer[j], envelope->inner[j]);
            }
        }
    }

    if (phiSmooth > 0.) {
        gmx_envelope_smoothEnvelope(envelope, phiSmooth);
    }

    if (envelope->bVerbose) {
        printf("\n");
    }

    gmx_envelope_setStats(envelope);

    printf("\nConstructed envelope around %d atoms.\n\tInner surface spanning radii from %g to %g.\n"
           "\tOuter surface spanning radii from %g to %g\n"
           "\t%d of %d directions defined.\n\n",
            isize, envelope->minInner, envelope->maxInner, envelope->minOuter, envelope->maxOuter, envelope->nDefined,
            envelope->nrays);

    envelope->bHaveEnv = TRUE;
}

gmx_bool gmx_envelope_bHaveSurf(gmx_envelope* envelope) {
    return envelope->bHaveEnv;
}

double gmx_envelope_maxR(gmx_envelope* envelope) {
    if (!envelope->bHaveEnv) {
        gmx_fatal(FARGS, "gmx_envelope_maxR: envelope not defined.\n");
    }

    return envelope->maxOuter;
}
