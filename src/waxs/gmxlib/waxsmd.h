#pragma once

#include <gromacs/fileio/oenv.h>  // gmx_output_env_t
#include <gromacs/mdrunutility/multisim.h>  // gmx_multisim_t
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/mdtypes/forcerec.h>  // t_forcerec
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/utility/basedefinitions.h>  // gmx_bool

#include <waxs/types/waxsrec.h>  // t_waxsrec


/** The entry function into all the SAS calculations in every MD step */
void do_waxs_md_low(const t_commrec* cr, const gmx_multisim_t* ms, gmx::ArrayRef<gmx::RVec> x, double t, int64_t step,
        t_waxsrec* wr, const gmx_mtop_t* mtop, const matrix box, PbcType pbcType, gmx_bool bDoLog);

/** Error of the intensity, using correct scale (log vs. linear) and different contributions. */
double waxs_intensity_sigma(t_waxsrec* wr, t_waxsrecType* wt, double* varIcalc, int iq);
