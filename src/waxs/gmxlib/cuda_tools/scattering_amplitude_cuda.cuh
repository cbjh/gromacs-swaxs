#ifndef GMX_WAXS_GMXLIB_CUDA_TOOLS_SCATTERING_AMPLITUDE_CUDA_H
#define GMX_WAXS_GMXLIB_CUDA_TOOLS_SCATTERING_AMPLITUDE_CUDA_H

#include <gromacs/gmxlib/network.h>  // t_commrec

#include <waxs/gmxcomplex.h>  // t_complex_d
#include <waxs/types/waxsrec.h>  // t_waxsrec

#ifdef __cplusplus
extern "C" {
#endif

void compute_scattering_amplitude_cuda(t_waxsrec* wr, const t_commrec* cr, int t, t_complex_d* sf,
        rvec* atomEnvelope_coord, int* atomEnvelope_type, int isize, int nprot, int qhomenr,
        real** aff_table, real* nsl_table, int num_qabs, double normA, double normB, double scale,
        gmx_bool bCalcForces, gmx_bool bFixSolventDensity, double* elapsed_time_d);

void calculate_dkI_GPU(int this_type, dvec* dkI, rvec* atomEnvelope_coord, int* atomEnvelope_type,
        int qstart, int qhomenr, int nabs, int nprot, double* GPU_time, const t_commrec* cr);

void init_gpudata_type(t_waxsrec* wr);

void push_unitFT_to_GPU(t_waxsrec *wr);

void update_envelope_solvent_density_GPU(t_waxsrec* wr);

#ifdef __cplusplus
}
#endif

#endif
