#include <gromacs/utility/fatalerror.h>  // gmx_fatal
#include <gromacs/fileio/gmxfio.h>  // t_fileio, gmx_ffopen, gmx_ffclose, gmx_fexist
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/mdtypes/forcerec.h>  // t_forcerec
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/utility/basedefinitions.h>  // gmx_bool
#include <gromacs/math/units.h>  // BOLTZ
#include <gromacs/gmxpreprocess/notset.h>  // NOTSET

#include <waxs/types/waxsrec.h>  // t_waxsrec
#include <waxs/gmxlib/waxsmd.h>  // t_waxsrec
#include <waxs/gmxlib/gmx_envelope.h> // gmx_envelope

/** Read a SAS curve and interpolate at given q-points */
void read_intensity_and_interpolate(const char* fn, real minq, real maxq, int nq, gmx_bool bRequireErrors,
    gmx_bool bReturnVariance, double** I_ret, double** Ierr_ret);

/** Read a SAS curve */
void read_waxs_curves(const char* fnScatt, t_waxsrec* wr, t_commrec* cr);

/** Generate the list of q-vectors where we compute A(q), B(q), and D(q) */
t_spherical_map* gen_qvecs_map(real minq, real maxq, int nqabs, int J, gmx_bool bDebug, t_commrec* cr,
        gmx_envelope* envelope, int Jmin, real Jalpha, gmx_bool bVerbose);


/** Set up all the data structures for SAS calculations or for running SAS-driven MD */
void init_waxs_md(t_forcerec* fr, t_commrec* cr, const gmx_multisim_t* ms, t_inputrec* ir, const gmx_mtop_t* top_global,
        const gmx_output_env_t* oenv, double t0, const char* runFilePath, const char* trajectoryFilePath,
        const char* fnOut, const char* fnScatt, t_state* state_local, gmx_bool bRerunMD, gmx_bool bWaterOptSet,
        gmx_bool bReadI, gmx_bool started_from_checkpoint, gmx_bool use_gpu);

void done_waxs_md(t_waxsrec* wr);

/** Gives the number of groups, minus the 'rest' group. */
int countGroups(const SimulationGroups& groups, SimulationAtomGroupType groupType);

void done_waxs_datablock(t_waxsrec* wr);

/** Initiate and empty waxs-record structure */
t_waxsrec* init_t_waxsrec();

/** Clear the waxs-record structure */
void done_t_waxsrec(t_waxsrec* t);

/** Init the record for "waxs types" (saxs, neutron1, neutron2, etc) */
t_waxsrecType init_t_waxsrecType();
