#include "./integrator.h"

#include <gromacs/fileio/oenv.h>  // gmx_output_env_t
#include <gromacs/mdlib/forcerec.h>  // t_forcerec
#include <gromacs/mdtypes/commrec.h>  // t_commrec

#include <waxs/gmxlib/init_waxs.h>  // done_waxs_md
#include <waxs/gmxlib/sftypeio.h>  // done_t_waxsrec
#include <waxs/gmxlib/swaxs_output.h>  // done_waxs_md
#include <waxs/gmxlib/waxsmd.h>  // swaxsAllocateGlobalAvs, swaxsFreeGlobalAvs, swaxsProcessGlobalAvs
#include <waxs/gmxlib/waxsmd_utils.h>  // waxsEstimateNumberIndepPoints

void done_swaxs(t_forcerec* fr, t_commrec* cr, const gmx_output_env_t* oenv) {
    if (!fr->waxsrec->bHaveNindep) {
        /** If we don't have Nindep yet: Get em now and scale var(I) accordingly */
        if (MASTER(cr)) {
            printf("Reached end of simulation, number of independent points were not not yet computed. Doing now.\n");
        }

        waxsEstimateNumberIndepPoints(fr->waxsrec, cr, FALSE, TRUE);
    }

    if (MASTER(cr)) {
        swaxsAllocateGlobalAvs(fr->waxsrec);
    }

    swaxsProcessGlobalAvs(fr->waxsrec, cr, true);

    if (MASTER(cr)) {
        fr->waxsrec->swaxsOutput->writeOut(fr->waxsrec, oenv);

        swaxsFreeGlobalAvs(fr->waxsrec);
    }

    done_waxs_md(fr->waxsrec);
    done_t_waxsrec(fr->waxsrec);
}
