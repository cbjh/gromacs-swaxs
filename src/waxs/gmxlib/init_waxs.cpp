#include "./init_waxs.h"
#include <gromacs/fileio/gmxfio.h>  // t_fileio, gmx_ffopen, gmx_ffclose, gmx_fexist
#include <gromacs/fileio/xvgr.h>
#include <gromacs/gmxlib/network.h>  // gmx_bcast, gmx_sumd
#include <gromacs/math/do_fit.h>  // reset_x
#include <gromacs/mdlib/gmx_omp_nthreads.h>  // gmx_omp_nthreads_get, emntDefault
#include <gromacs/random/uniformrealdistribution.h>  // UniformRealDistribution
#include <gromacs/utility/fatalerror.h>  // gmx_fatal

#include <waxs/gmxlib/waxsmd.h> // waxs_intensity_sigma
#include <waxs/gmxlib/waxsmd_utils.h> // CMSF_q
#include <waxs/gmxlib/env.h> // readEnvironmentVariables
#include <waxs/gmxlib/solvent_scattering.h>  // swaxs::solventScatteringFromRdfs
#include <waxs/gmxlib/cuda_tools/scattering_amplitude_cuda.cuh> // init_gpudata_type, push_unitFT_to_GPU
#include <swaxs/maxent_ensemble.h>  // init_maxent_ensemble_refinement
#include <swaxs/bayesian_ensemble.h>  // init_bayesian_ensemble_refinement

static const char* waxs_simd_string() {
#if defined(GMX_WAXS_NO_SIMD)
    return "None";
#elif defined(GMX_X86_AVX_256)
    return "AVX-256";
#elif defined(GMX_X86_AVX_128_FMA)
    return "AVX-128-FMA";
#elif defined(GMX_X86_SSE4_1)
    return "SSE-4.1";
#elif defined(GMX_X86_SSE2)
    return "SSE-2";
#else
    return "Unknown";
#endif
}

void read_intensity_and_interpolate(const char* fn, real minq, real maxq, int nq, bool bRequireErrors,
        bool bReturnVariance, double** I_ret, double** Ierr_ret) {
    double** y;
    int ncol;
    int nlines = read_xvg(fn, &y, &ncol);

    /** Now: q = y[0], I = y[1], sigma = y[2] */
    if (ncol != 3 and ncol != 2) {
        gmx_fatal(FARGS, "Expected 2 or 3 rows in file %s (q, Intensity, and (optionally) sigma(Intensity). Found %d rows.\n",
                fn, ncol);
    }

    if (bRequireErrors and ncol < 3) {
        gmx_fatal(FARGS, "Require uncertainties in intensity file %s, but found only %d columns\n", fn, ncol);
    }

    if (ncol == 2) {
        printf("WAXS-MD: No experimental errors found in %s\n", fn);
    }

    if (y[0][0] > minq) {
        gmx_fatal(FARGS, "Smallest q-value in %s is %g, but the smallest q requested is %g. Provide a different\n "
                "scattering intensity file or increase waxs-startq in the mdp file\n", fn, y[0][0], minq);
    }

    if (y[0][nlines - 1] < maxq) {
        gmx_fatal(FARGS, "Largest q-value in %s is %g, but the largest q requested is %g. Provide a different\n "
                "scattering intensity file or decrease waxs-endq in the mdp file\n", fn, y[0][nlines - 1], maxq);
    }

    double* I;
    double* Ierr;

    snew(I, nq);

    if (ncol >= 3) {
        snew(Ierr, nq);
    } else {
        Ierr = nullptr;
    }

    real dq = nq > 1 ? (maxq - minq) / (nq - 1) : 0.0;

    real left = 0.;
    real right = 0.;

    for (int i = 0; i < nq; i++) {
        real q = minq + i * dq;
        gmx_bool bFound = FALSE;

        int j;

        /** Simple linear interpolation. Maybe we can improve this later. */
        for (j = 0; j < nlines - 1; j++) {
            if ((left = y[0][j]) <= q and q <= (right = y[0][j + 1])) {
                bFound = TRUE;
                break;
            }
        }

        if (!bFound) {
            gmx_fatal(FARGS, "Error in read_intensity_and_interpolate(). This should not happen.\n");
        }

        real Ileft = y[1][j];
        real Iright = y[1][j + 1];
        real slopeI = (Iright - Ileft) / (right - left);
        I[i] = Ileft + (q - left) * slopeI;

        if (ncol >= 3) {
            real Isigleft = y[2][j];
            real Isigright = y[2][j + 1];
            real slopeIsig = (Isigright - Isigleft) / (right - left);
            Ierr[i] = Isigleft + (q - left) * slopeIsig;

            if (bReturnVariance) {
                Ierr[i] = gmx::square(Ierr[i]);
            }

            if (Ierr[i] < 1e-20) {
                gmx_fatal(FARGS, "sigma of exprimental I(q) is zero at q = %g. Provide a non-zero sigma.\n", q);
            }
        }
    }

    *I_ret = I;
    *Ierr_ret = Ierr;
}

void read_waxs_curves(const char* fnScatt, t_waxsrec* wr, t_commrec* cr) {
    char* fnUsed = nullptr;
    char* base = nullptr;
    char* fnInterp = nullptr;

    if (MASTER(cr)) {
        int len = strlen(fnScatt);

        snew(base, len);

        strncpy(base, fnScatt, len - 4);
        base[len - 4] = '\0';
        fprintf(stderr, "Using base file name for WAXS curve reading: %s\n", base);

        snew(fnUsed, len + 10);
        snew(fnInterp, len + 10);
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];

        if (wt->nq <= 0 or wt->nq > 999999) {
            gmx_incons("Invalid waxs.nq in read_waxs_curves()\n");
        }

        if (MASTER(cr)) {
            /* With one type:       Reading file fnScatt, such as saxs-curve.xvg
             * With multiple types: Reading files such as saxs-curve_1.xvg, saxs-curve_2.xvg,
             */
            if (wr->inputParams->nTypes > 1) {
                sprintf(fnUsed, "%s_%d.xvg", base, t + 1);
                sprintf(fnInterp, "Iinterp_%d.dat", t + 1);
            } else {
                sprintf(fnUsed, "%s", fnScatt);
                sprintf(fnInterp, "Iinterp.dat");
            }

            printf("WAXS-MD: Reading intensities from %s\n", fnUsed);
            printf("WAXS-MD: Writing interpolated intensities to %s\n", fnInterp);
            read_intensity_and_interpolate(fnUsed, wt->minq, wt->maxq, wt->nq, FALSE, FALSE, &wt->Iexp, &wt->Iexp_sigma);

            if (wt->Iexp_sigma == nullptr and (
                    wr->inputParams->weightsType == ewaxsWeightsEXPERIMENT or
                    wr->inputParams->weightsType == ewaxsWeightsEXP_plus_CALC or
                    wr->inputParams->weightsType == ewaxsWeightsEXP_plus_SOLVDENS or
                    wr->inputParams->weightsType == ewaxsWeightsEXP_plus_CALC_plus_SOLVDENS
            )) {
                gmx_fatal(FARGS, "Requested to use exerimental SAXS errors, but no errors were read from "
                        "experimental intensity file %s\n", fnScatt);
            }

            FILE* fp = fopen(fnInterp, "w");
            fprintf(fp, "@type xydy\n");

            for (int i = 0; i < wt->nq; i++) {
                fprintf(fp, "%g %g %g\n", wt->minq + i * (wt->maxq - wt->minq) / (wt->nq - 1), wt->Iexp[i], wt->Iexp_sigma[i]);
            }

            fclose(fp);
        }

        /** Broadcast it */
        if (wt->Iexp_sigma == nullptr) {
            snew(wt->Iexp, wt->nq);
            snew(wt->Iexp_sigma, wt->nq);
        }

        if (PAR(cr)) {
            gmx_bcast(wt->nq * sizeof(double), wt->Iexp, cr->mpi_comm_mygroup);
            gmx_bcast(wt->nq * sizeof(double), wt->Iexp_sigma, cr->mpi_comm_mygroup);
        }

        /** Scale I(q=0) to input I(q)? Then store Iexp(q=0) to targetI0 */
        if (wr->inputParams->bScaleI0) {
            if (wt->minq != 0.) {
                gmx_fatal(FARGS, "When scaling I(q = 0) to target, you must compute I(q = 0). Use waxs-startq = 0 (found %g)\n",
                        wt->minq);
            }

            /*NEW, differend mode to get I0. Added by MH*/
            /* wt->targetI0 = wt->Iexp[0]; */
            char* buf;

            if ((buf = getenv("GMX_WAXS_I0")) != nullptr) {
                wt->targetI0 = atof(buf);

                fprintf(stderr, "\nWAXS-MD read saxs curve: Found environment variable GMX_WAXS_I0 = %.1f\n", wt->targetI0);
            } else {
                wt->targetI0 = wt->Iexp[0];

                if (MASTER(cr)) {
                    fprintf(stderr, "\nWAXS-MD read saxs curve: Did not  found GMX_WAXS_I0, taking the experimental value: %.1f\n",
                            wt->targetI0);
                }
            }

            if (MASTER(cr)) {
                printf("WAXS-MD read saxs curve: Will scale I(q=0) to predefined target from input I(q) curve: %g\n",
                        wt->targetI0);
            }

            if (PAR(cr)) {
                gmx_bcast(sizeof(real), &wt->targetI0, cr->mpi_comm_mygroup);
            }
        }
    }

    if (MASTER(cr)) {
        sfree(base);
        sfree(fnUsed);
        sfree(fnInterp);
    }
}


t_waxsrecType init_t_waxsrecType() {
    t_waxsrecType t;

    t.type = -1;
    t.fc = 0;
    t.nq = 0;
    t.minq = t.maxq = 0;
    t.nShannon = 0;
    t.qvecs = nullptr;
    t.Ipuresolv = nullptr;
    t.Iexp = nullptr;
    t.Iexp_sigma = nullptr;
    t.f_ml = t.c_ml = 0;
    t.targetI0 = 0;

    t.bayesianEnsemble.I = nullptr;
    t.bayesianEnsemble.Ivar = nullptr;
    t.bayesianEnsemble.sum_I = nullptr;
    t.bayesianEnsemble.sum_Ivar = nullptr;

    t.wd = nullptr;
    strcpy(t.saxssansStr, "");

    t.atomEnvelope_scatType_A = nullptr ;
    t.atomEnvelope_scatType_B = nullptr ;

    t.naff_table = 0;
    t.aff_table = nullptr;

    t.vLast = 0;
    t.fLast = nullptr;

    t.nnsl_table = 0;
    t.nsl_table = nullptr;
    t.deuter_conc = -1;

    t.envelope = nullptr;
    t.givenSolventDensity = 0;
    t.soluteSumOfScattLengths = 0;
    t.soluteContrast = 0;
    t.contrastFactor = 0;

    t.n2HAv_A  = 0;
    t.n1HAv_A  = 0;
    t.nHydAv_A = 0;
    t.n2HAv_B  = 0;
    t.n1HAv_B  = 0;
    t.nHydAv_B = 0;

    return t;
}

t_waxsrec* init_t_waxsrec(void)
{
    t_waxsrec* t = new t_waxsrec;

    t->x_ref = nullptr;

    /* Make origin 0,0,0 - this is where the protein is eventually shifted
       to fit into the envelope */
    clear_rvec(t->origin);
    t->w_fit = nullptr;

    t->wt = nullptr;
    t->bHaveNindep = FALSE;

    t->nSolvWarn = 0;
    t->nWarnCloseBox = 0;
    t->soluteVolAv = 0;

    t->solElecDensAv = 0;
    t->nElectrons = nullptr;
    t->nElecAddedA = t->nElecAddedB = 0.;
    t->nElecTotA = 0.;
    t->nElecProtA = 0.;

    t->epsilon_buff = 1;
    t->rng = nullptr;

    t->waxsStep = 0;
    t->nAtomsExwaterAver = t->nAtomsLayerAver = 0;
    t->nElecAvA = t->nElecAv2A = t->nElecAv4A = 0.;
    t->nElecAvB = t->nElecAv2B = t->nElecAv4B = 0.;
    t->solElecDensAv = t->solElecDensAv2 = t->solElecDensAv4 = 0.;
    t->RgAv = 0;

    t->isizeA = t->isizeB = 0;
    t->pbcatoms = nullptr;

    t->atomEnvelope_coord_A = t->atomEnvelope_coord_B = nullptr;

    t->indexA = t->indexB = nullptr;
    t->indexA_nalloc = t->indexB_nalloc = 0;

    t->ensemble_weights = nullptr;

    t->indA_prot = nullptr;
    t->indA_sol = nullptr;
    t->indB_sol = nullptr;
    t->nindA_prot = 0;
    t->nindA_sol = 0;
    t->nindB_sol = 0;
    t->nSoluteMols = 0;

    t->shiftsInsideEnvelope_A_prot = nullptr;
    t->shiftsInsideEnvelope_A_sol = nullptr;
    t->shiftsInsideEnvelope_B = nullptr;

    t->ind_RotFit = nullptr;
    t->nind_RotFit = 0;

    t->vLast = 0;
    t->fLast = nullptr;

    t->solvent = nullptr;
    t->local_state = nullptr;

    return t;
}

void done_t_waxsrec(t_waxsrec* t) {
    /* This function is totally incomplete, but we don't really need it anyway */

    if (t->x_ref) sfree(t->x_ref);
    if (t->w_fit) sfree(t->w_fit);
    if (t->pbcatoms) sfree(t->pbcatoms);

    sfree(t->indexA);
    if (t->indexB) sfree(t->indexB);
    if (t->indA_prot) sfree(t->indA_prot);
    if (t->indA_sol) sfree(t->indA_sol);
    if (t->indB_sol) sfree(t->indB_sol);
    if (t->ind_RotFit) sfree(t->ind_RotFit);

    delete t;
    t = nullptr;
}


static void copy_ir2waxsrec(t_waxsrec* wr, t_inputrec* ir, t_commrec* cr) {
    wr->inputParams->nstlog = ir->waxs_nstlog;
    wr->inputParams->nfrsolvent = ir->waxs_nfrsolvent;
    wr->inputParams->bVacuum = FALSE;
    wr->inputParams->stepCalcNindep = 0;

    wr->inputParams->npbcatom = ir->waxs_npbcatom;
    snew(wr->pbcatoms, wr->inputParams->npbcatom);

    for (int i = 0; i < wr->inputParams->npbcatom; i++) {
        wr->pbcatoms[i] = ir->waxs_pbcatoms[i];
    }

    wr->inputParams->bScaleI0 = ir->ewaxs_bScaleI0;
    wr->inputParams->givenElecSolventDensity = ir->waxs_denssolvent;
    wr->inputParams->solventDensRelErr = ir->waxs_denssolventRelErr;

    if (wr->inputParams->givenElecSolventDensity == 0 and wr->inputParams->solventDensRelErr > 0) {
        gmx_fatal(FARGS, "To account for the uncertainty of the solvent density, you must specify the expected solvent density.\n"
                "Specify the solvent density in the mdp file, e.g. to 334 e/nm^3\n");
    }

    wr->inputParams->bCorrectBuffer = (ir->ewaxs_correctbuff == waxscorrectbuffYES);
    wr->inputParams->bFixSolventDensity = (ir->waxs_denssolvent > GMX_FLOAT_EPS);
    wr->inputParams->bBayesianSolvDensUncert = (ir->ewaxs_solvdensUnsertBayesian == ewaxsSolvdensUncertBayesian_YES);
    wr->inputParams->solv_warn_lay = ir->waxs_warn_lay;

    real tau = wr->inputParams->tau = ir->waxs_tau;
    wr->inputParams->nstcalc = ir->waxs_nstcalc;

    real dt = 1.0 * wr->inputParams->nstcalc * ir->delta_t;
    wr->inputParams->tausteps = wr->inputParams->tau / dt;

    if (dt <= 0.) {
        gmx_fatal(FARGS, "dt for WAXS intensity calculation %g is <= 0\n", dt);
    }

    if (tau > 0.0) {
        wr->inputParams->scale = exp(-1.0 * dt / tau);
        wr->inputParams->bSwitchOnForce = TRUE;
        wr->inputParams->stepCalcNindep = (int)((tau / dt)) / 2;
    } else if (tau == -1.0) {
        wr->inputParams->scale = 1.0;  /** non-weighted averaging */
        wr->inputParams->bSwitchOnForce = FALSE;
    } else if (tau == 0.0) {
        wr->inputParams->scale = 0.0;  /** no averaging, i.e., instantaneous coupling. */
        wr->inputParams->bSwitchOnForce = FALSE;
    } else {
        gmx_fatal(FARGS, "Illegal value for waxs_tau found (%g). Allowed: -1, 0, >0)\n", tau);
    }

    wr->inputParams->ewaxs_Iexp_fit      = ir->ewaxs_Iexp_fit;
    wr->inputParams->potentialType       = ir->ewaxs_potential;

    wr->inputParams->weightsType         = ir->ewaxs_weights;
    wr->inputParams->ensembleType        = static_cast<swaxs::EnsembleType>(ir->ewaxs_ensemble_type);
    wr->inputParams->t_target            = ir->waxs_t_target;

    wr->inputParams->kT = ir->opts.ref_t[0] * BOLTZ;

    if (MASTER(cr)) {
        fprintf(stderr, "WAXS-MD: kT = %g\n", wr->inputParams->kT);
        fprintf(stderr, "WAXS-MD: Computing # independent points every %d steps.\n", wr->inputParams->stepCalcNindep);
    }

    /** Init stuff that is specific to the type of scattering (xray or neutron) */
    wr->inputParams->nTypes = ir->waxs_nTypes;
    wr->inputParams->bDoingNeutron = FALSE;

    snew(wr->wt, wr->inputParams->nTypes);

    for (int i = 0; i < wr->inputParams->nTypes; i++) {
        wr->wt[i]             = init_t_waxsrecType();
        wr->wt[i].fc          = ir->waxs_fc         [i];
        wr->wt[i].type        = ir->escatter        [i];
        wr->inputParams->bDoingNeutron = (wr->inputParams->bDoingNeutron or ir->escatter[i] == escatterNEUTRON);
        wr->wt[i].nq          = ir->waxs_nq         [i];
        wr->wt[i].minq        = ir->waxs_start_q    [i];
        wr->wt[i].maxq        = ir->waxs_end_q      [i];
        wr->wt[i].deuter_conc = ir->waxs_deuter_conc[i];

        sprintf(wr->wt[i].saxssansStr, "%s", ir->escatter[i] == escatterXRAY ? "SAXS" : "SANS");
        sprintf(wr->wt[i].scattLenUnit, "%s", ir->escatter[i] == escatterXRAY ? "electrons" : "NSLs");
    }

    wr->inputParams->J = ir->waxs_J;

    if (tau > 0.0) {
        wr->inputParams->nAverI = 2 * tau / dt;
    } else {
        wr->inputParams->nAverI = 100;
    }

    /* Hardcoded - uh, ugly. Or should we depend this on tau? */
    if (MASTER(cr)) {
        fprintf(stderr, "WAXS-MD: Running sigma of I(q) computed from the last %d intensities (tau/dt = %g/%g) - NOT used at the moment.\n",
                wr->inputParams->nAverI, tau, dt);
    }

    /** In case of SAXS ensemble refinement: set initial weights of states */
    wr->inputParams->ensemble_nstates = ir->waxs_ensemble_nstates;
    snew(wr->ensemble_weights, wr->inputParams->ensemble_nstates);
    snew(wr->inputParams->ensemble_weights_init, wr->inputParams->ensemble_nstates);

    wr->inputParams->ensemble_weights_fc = ir->waxs_ensemble_fc;

    for (int i = 0; i < wr->inputParams->ensemble_nstates; i++) {
        wr->ensemble_weights[i] = ir->waxs_ensemble_init_w[i];
        wr->inputParams->ensemble_weights_init[i] = ir->waxs_ensemble_init_w[i];
    }

    if (MASTER(cr) and wr->inputParams->ensemble_nstates) {
        fprintf(stderr, "WAXS-MD: Found initial weights for %d states for ensemble refinement =", wr->inputParams->ensemble_nstates);

        for (int i = 0; i < wr->inputParams->ensemble_nstates; i++) {
            fprintf(stderr, " %g", wr->inputParams->ensemble_weights_init[i]);
        }

        fprintf(stderr, "\n");
        fprintf(stderr, "WAXS-MD: Force constant for ensemble weights = %g", wr->inputParams->ensemble_weights_fc);
    }

    /** The other structures to be initialised later. */
}

/** Returns a continuous array (size mtop->natoms) of Cromer-Mann or NSL (bNeutron = TRUE) types from mtop */
static int* mtop2scattTypeList(const gmx_mtop_t* mtop, gmx_bool bNeutron) {
    /**
     * Bookkeeping: natoms = Sum _i ^ nmolblock ( molblock[i]->nmol * molblock[i]->natoms_mol )
     * moltype.atoms->nr = molblock[i]->natoms_mol
     * Loop over mblocks instead and use its information to write
     */

    int* scattTypeId = nullptr;
    snew(scattTypeId, mtop->natoms);

    for (int i = 0; i < mtop->natoms; i++) {
        scattTypeId[i] = -1000;
    }

    int isum = 0;

    /** Loop over molecule types */
    for (std::size_t mb = 0; mb < mtop->molblock.size(); mb++) {
        const t_atoms* atoms = &mtop->moltype[mtop->molblock[mb].type].atoms;

        int nmols = mtop->molblock[mb].nmol;
        int natoms_mol = atoms->nr;

        /** Loop over atoms in this molecule type */
        for (int iatom = 0; iatom < natoms_mol; iatom++) {
            /** Pick NSL or Cromer-Mann type of this atom */
            int stype = bNeutron ? atoms->atom[iatom].nsltype : atoms->atom[iatom].cmtype;

            /** Loop over moleculs of this molecule type */
            for (int imol = 0; imol < nmols; imol++) {
                int itot = isum + imol * natoms_mol + iatom;
                scattTypeId[itot] = stype;
            }
        }

        isum += nmols * natoms_mol;
    }

    /** For debugging, make sure that Cromer-Mann or NSL type of *every* atom was set. */
    for (int i = 0; i < mtop->natoms; i++) {
        if (scattTypeId[i] == -1000) {
            gmx_fatal(FARGS, "scattTypeId[%d] was not set in mtop2sftype_list\n", i);
        }
    }

    return scattTypeId;
}

/**
 * Assume both solvent and solute .tpr files are set up identically, allowing matching of the atomic form factors.
 * Do not allow types that are only found in solvent, as this would indicate some buffer mismatch between solute
 * and solvent systems.
 */
static void redirect_md_solventtypes(gmx_mtop_t* solvent_top, const t_scatt_types* scattTypesSolute) {
    int nSolventGroups = countGroups(solvent_top->groups, SimulationAtomGroupType::egcWAXSSolvent);

    fprintf(stderr, "In water-background: nSolventGroups = %d. \n", nSolventGroups);

    /**
     * This section is somewhat shared with grompp's remap to unique atom-types. Do we combine later?
     *
     * Set up redirect map. Although there normally should be only one moltype and molblock,
     * we should make allowances for complex solvents in the future.
     */

    int j, k;
    int isum = 0;

    for (std::size_t mb = 0; mb < solvent_top->moltype.size(); mb++) {
        t_atoms* atoms = &solvent_top->moltype[solvent_top->molblock[mb].type].atoms;

        for (int iatom = 0; iatom < atoms->nr; iatom++) {
            int itot = iatom + isum;
            gmx_bool bSolvent = (getGroupType(solvent_top->groups, SimulationAtomGroupType::egcWAXSSolvent, itot) < nSolventGroups);

            if (bSolvent) {
                t_scatt_types* solvent_scattTypes = &solvent_top->scattTypes;

                /** Look for idenitcal Cromer-Mann parameters in solute topology */
                j = atoms->atom[iatom].cmtype;
                k = search_scattTypes_by_cm(scattTypesSolute, &(solvent_scattTypes->cromerMannParameters[j]));

                if (k == NOTSET) {
                    gmx_fatal(FARGS,"Cromer-Mann type %d of solvent not found in main system\n\n"
                            "We do not allow using waxs_solvents that has scattering types"
                            " not found in the solute system! Please use waxs_solvents molecules that are exactly like"
                            " or are subsets of the solvent in the main simulation.\n", j);
                } else if (k >= scattTypesSolute->nCromerMannParameters or k < 0) {
                    gmx_fatal(FARGS, "search_scattTypes_by_cm returned illegal sftype index (%d). (Allowed 0 to %d)",
                            k, scattTypesSolute->nCromerMannParameters);
                } else {
                    atoms->atom[iatom].cmtype = k;
                    fprintf(stderr, "Redirecting solvent Cromer-Mann type %d (atom %d) to main system Cromer-Mann type %d\n",
                            j, iatom, k);
                }

                /**
                 * Optionally: Look or neutron scattering length in solute topology. Only needed if we have
                 * NSL types in the solute system, otherwise we don't do neutron scattering anyway.
                 */
                j = atoms->atom[iatom].nsltype;

                if (j >= 0 and scattTypesSolute->nNeutronScatteringLengths > 0) {
                    k = search_scattTypes_by_nsl(scattTypesSolute, &(solvent_scattTypes->neutronScatteringLengths[j]));

                    if (k == NOTSET) {
                        gmx_fatal(FARGS, "Neutron scattering length type %d of solvent not found in main system (cohb = %g)\n\n"
                                "We do not allow using waxs_solvents that has NEUTRON scattering types\n"
                                "not found in the main system! Please use waxs_solvents molecules that are exactly like\n"
                                "or are subsets of the solvent in the main simulation.\n", j, solvent_scattTypes->neutronScatteringLengths[j].cohb);
                    } else if (k >= scattTypesSolute->nNeutronScatteringLengths or k < 0) {
                        gmx_fatal(FARGS, "search_scattTypes_by_nsl returned illegal NSL type index (%d). (Allowed 0 to %d)",
                                k, scattTypesSolute->nNeutronScatteringLengths);
                    } else {
                        atoms->atom[iatom].nsltype = k;

                        fprintf(stderr, "Redirecting solvent NSL type %d (atom %d) to main system NSL type %d\n", j, iatom, k);
                    }
                }
            }
        }

        isum += atoms->nr * solvent_top->molblock[mb].nmol;
    }
}

/** Return the number of unconnected molecules in the solute */
static int nSoluteMolecules(const gmx_mtop_t* mtop) {
    int mb_last = -1;
    int imol_last = -1;
    int nSoluteMols = 0;

    int isum = 0;
    int nSoluteGroups = countGroups(mtop->groups, SimulationAtomGroupType::egcWAXSSolute);

    for (std::size_t mb = 0; mb < mtop->molblock.size(); mb++) {
        const t_atoms* atoms = &mtop->moltype[mtop->molblock[mb].type].atoms;
        int nmols = mtop->molblock[mb].nmol;
        int natoms_mol = atoms->nr;

        /** molecules */
        for (int imol = 0; imol < nmols; imol++) {
            /** atoms in molecule */
            for (int iatom = 0; iatom < natoms_mol; iatom++) {
                int itot = isum + imol * natoms_mol + iatom;
                gmx_bool bSolute = (getGroupType(mtop->groups, SimulationAtomGroupType::egcWAXSSolute, itot) < nSoluteGroups);

                // TODO: The way this procedures detects last molecule block needs to be revisited. (remove cast, analyse the issue)

                if (bSolute and (static_cast<int>(mb) != mb_last or imol != imol_last)) {
                    nSoluteMols++;
                    mb_last = mb;
                    imol_last = imol;
                }
            }
        }

        isum += nmols*natoms_mol;
    }

    printf("Found %d unconnected molecule%s in the solute group.\n", nSoluteMols, nSoluteMols > 1 ? "s" : "");

    return nSoluteMols;
}


/** Setup indices of protein and solvent */
static void prep_md_indices(int** indA_prot, int* nindA_prot, int** indA_solv, int* nindA_solv, const gmx_mtop_t* mtop,
        gmx_bool bDoProt, gmx_bool bDoSolv) {
    int nSoluteGroups = countGroups(mtop->groups, SimulationAtomGroupType::egcWAXSSolute);
    int nSolventGroups = countGroups(mtop->groups, SimulationAtomGroupType::egcWAXSSolvent);

    int* prot = nullptr;
    int* solv = nullptr;
    int nprot = 0;
    int nsolv = 0;

    int natoms = mtop->natoms;

    for (int i = 0; i < natoms; i++) {
        if (bDoSolv and getGroupType(mtop->groups, SimulationAtomGroupType::egcWAXSSolvent, i) < nSolventGroups) {
            nsolv++;
            srenew(solv, nsolv);
            solv[nsolv - 1] = i;
        } else if (bDoProt and getGroupType(mtop->groups, SimulationAtomGroupType::egcWAXSSolute, i) < nSoluteGroups) {
            nprot++;
            srenew(prot, nprot);
            prot[nprot - 1] = i;
        }
    }

    if (bDoProt) {
        *nindA_prot = nprot;
        *indA_prot = prot;
        fprintf(stderr, "Created array of %d solute atoms.\n", *nindA_prot);
    }

    if (bDoSolv) {
        *nindA_solv = nsolv;
        *indA_solv = solv;
        fprintf(stderr, "Created array of %d solvent atoms.\n", *nindA_solv);
    }
}

static void prep_fitting_weights(t_waxsrec* wr, int** ind_RotFit, int* nind_RotFit, real* w_fit, const gmx_mtop_t* mtop) {
    printf("WAXS-MD: Preparing weights for %s fitting...", wr->inputParams->bMassWeightedFit ? "mass-weighted" : "non-weighted");
    fflush(stdout);

    int natoms = mtop->natoms;

    for (int itot = 0; itot < natoms; itot++) {
        w_fit[itot] = 0.;
    }

    int ngrps_fit = countGroups(mtop->groups, SimulationAtomGroupType::egcROTFIT);

    int isum = 0;
    int* fitgrp = nullptr;
    int nfit = 0;

    /* This is a loop over all atoms. Global atom id is itot */
    /* Molecule types */
    for (std::size_t mb = 0; mb < mtop->molblock.size(); mb++) {
        const t_atoms* atoms = &mtop->moltype[mtop->molblock[mb].type].atoms;
        int nMols = mtop->molblock[mb].nmol;
        int natoms_mol = atoms->nr;

        /* molecules */
        for (int iMol = 0; iMol < nMols; iMol++) {
            /* atoms in molecule */
            for (int iatom = 0; iatom < natoms_mol; iatom++) {
                int itot = isum + iMol * natoms_mol + iatom;

                gmx_bool bFit = (getGroupType(mtop->groups, SimulationAtomGroupType::egcROTFIT, itot) < ngrps_fit);

                if (bFit) {
                    nfit++;
                    srenew(fitgrp, nfit);
                    fitgrp[nfit - 1] = itot;
                    real m = atoms->atom[iatom].m;
                    w_fit[itot] = wr->inputParams->bMassWeightedFit ? m : 1.;
                }
            }
        }

        isum += nMols * natoms_mol;
    }

    *nind_RotFit = nfit;
    *ind_RotFit = fitgrp;

    printf("done. Using %d fitting atoms.\n", *nind_RotFit);
}


/**
 * Allocate memory for the local storage of the averages A(q), B(q)
 *   A(q) - B(q), grad[k](A * (q)), and A(q) * grad[k](A * (q))
 * Allocating for waxsType t
 */
static void alloc_waxs_datablock(t_commrec* cr, t_waxsrec* wr, int nindA_prot, int t) {
    t_waxsrecType* wt = &wr->wt[t];
    int qhomenr = wt->qvecs->qhomenr;

    waxs_datablock* wd;
    snew(wd, 1);

    int nIvalues = -1;

    /** Store intensity for each |q| */
    nIvalues = wt->qvecs->nabs;

    wd->nIvalues = nIvalues;
    int nabs = wt->qvecs->nabs;

    /** Init norm of cumulative average to zero */
    wd->normA = 0.;
    wd->normB = 0.;

    /** Init average overall densities */
    wd->avAsum = 0;
    wd->avBsum = 0;

    int64_t nByte = 0;

    if (wr->inputParams->bCalcPot) {
        snew(wd->vAver, nIvalues);
        snew(wd->vAver2, nIvalues);
        // nByte += 2 * nIvalues * sizeof(real);
        nByte += nIvalues * (sizeof(wd->vAver[0]) + sizeof(wd->vAver2[0]));
    }

    wd->B                           = nullptr;
    wd->IA = wd->IB = wd->Ibulk     = nullptr;
    wd->I_scaleI0                   = nullptr;
    wd->I_varA = wd->I_varB         = nullptr;
    wd->varI = wd->varIA            = nullptr;
    wd->varIB = wd->varIbulk        = nullptr;
    wd->I_errSolvDens               = nullptr;
    wd->varI_avAmB2                 = nullptr;
    wd->varI_varA = wd->varI_varB   = nullptr;
    wd->avA = wd->avB = wd->avAcorr = nullptr;
    wd->re_avBbar_AmB               = nullptr;
    wd->avBsq                       = nullptr;
    wd->Orientational_Av            = nullptr;
    wd->avAqabs_re = wd->avBqabs_re = nullptr;
    wd->avAqabs_im = wd->avBqabs_im = nullptr;
    wd->Nindep                      = nullptr;
    wd->Dglobal                     = nullptr;
    wd->DerrSolvDens                = nullptr;
    wd->avAglobal                   = nullptr;
    wd->avAsqglobal                 = nullptr;
    wd->avA4global                  = nullptr;
    wd->av_ReA_2global              = nullptr;
    wd->av_ImA_2global              = nullptr;
    wd->avBglobal                   = nullptr;
    wd->avBsqglobal                 = nullptr;
    wd->avB4global                  = nullptr;
    wd->av_ReB_2global              = nullptr;
    wd->av_ImB_2global              = nullptr;

    snew(wd->A,          qhomenr);
    snew(wd->avA,        qhomenr);
    snew(wd->av_ReA_2,   qhomenr);
    snew(wd->av_ImA_2,   qhomenr);
    snew(wd->avAsq,      qhomenr);
    snew(wd->avA4,       qhomenr);
    snew(wd->D,          qhomenr);

    snew(wd->I,          nIvalues);
    snew(wd->varI,       nIvalues);
    snew(wd->IA,         nIvalues);
    snew(wd->varIA,      nIvalues);
    snew(wd->avAqabs_re, nIvalues);
    snew(wd->avAqabs_im, nIvalues);

    if (wr->inputParams->solventDensRelErr > 0) {
        snew(wd->DerrSolvDens, qhomenr);
        snew(wd->I_errSolvDens, nIvalues);
    }

    nByte += qhomenr*(sizeof(wd->A[0]) + sizeof(wd->avA[0]) + sizeof(wd->av_ReA_2[0]) + sizeof(wd->av_ImA_2[0]) +
            sizeof(wd->avAsq[0]) + sizeof(wd->avA4[0]) + sizeof(wd->D[0]) );
    nByte += nIvalues*(sizeof(wd->I[0]) + sizeof(wd->varI[0]) + sizeof(wd->IA[0]) +
            sizeof(wd->varIA[0]) + sizeof(wd->avAqabs_re[0]) + sizeof(wd->avAqabs_im[0]));

    for (int i = 0; i < qhomenr; i++) {
        wd->avA     [i] = cnul_d;
        wd->avAsq   [i] = 0.;
        wd->avA4    [i] = 0.;
        wd->av_ReA_2[i] = 0.;
        wd->av_ImA_2[i] = 0.;
    }

    if (!wr->inputParams->bVacuum) {
        snew(wd->IB,          nIvalues);
        snew(wd->Ibulk,       nIvalues);
        snew(wd->varIB,       nIvalues);
        snew(wd->varIbulk,    nIvalues);
        snew(wd->avBqabs_re,  nIvalues);
        snew(wd->avBqabs_im,  nIvalues);
        snew(wd->I_avAmB2,    nIvalues);
        snew(wd->I_varA,      nIvalues);
        snew(wd->I_varB,      nIvalues);

        if (wr->inputParams->bScaleI0) {
            snew(wd->I_scaleI0, nIvalues);
        }

        snew(wd->varI_avAmB2,   nIvalues);
        snew(wd->varI_varA,     nIvalues);
        snew(wd->varI_varB,     nIvalues);
        snew(wd->B,             qhomenr);
        snew(wd->avB,           qhomenr);
        snew(wd->av_ReB_2,      qhomenr);
        snew(wd->av_ImB_2,      qhomenr);
        snew(wd->avBsq,         qhomenr);
        snew(wd->avB4,          qhomenr);
        snew(wd->re_avBbar_AmB, qhomenr);

        nByte += qhomenr
                * (sizeof(wd->B[0]) + sizeof(wd->avB[0]) + sizeof(wd->av_ReB_2[0]) + sizeof(wd->av_ImB_2[0])
                        + sizeof(wd->avBsq[0]) + sizeof(wd->avB4[0]) + sizeof(wd->re_avBbar_AmB[0]));
        nByte += nIvalues
                * (sizeof(wd->IB[0]) + sizeof(wd->Ibulk[0]) + sizeof(wd->varIB[0]) + sizeof(wd->varIbulk[0])
                        + sizeof(wd->avBqabs_re[0]) + sizeof(wd->avBqabs_im[0]) + sizeof(wd->I_avAmB2[0])
                        + sizeof(wd->I_varA[0]) + sizeof(wd->I_varB[0]) + sizeof(wd->varI_avAmB2[0])
                        + sizeof(wd->varI_varA[0]) + sizeof(wd->varI_varB[0]));

        for (int i = 0; i < qhomenr; i++) {
            wd->avB[i] = cnul_d;
            wd->avBsq[i] = 0;
            wd->avB4[i] = 0.;
            wd->av_ReB_2[i] = 0.;
            wd->av_ImB_2[i] = 0.;
            wd->re_avBbar_AmB[i] = 0.;
        }
    }

    int64_t nByteInten = nByte;
    int64_t nByteForce = 0;

    if (wr->inputParams->bCalcForces) {
        snew(wd->dkI, nIvalues * nindA_prot);
        nByteForce += nIvalues * nindA_prot * sizeof(wd->dkI[0]);

        /** Now of size qhomenr*nindA_prot for gradients */
        snew(wd->dkAbar, qhomenr * nindA_prot);
        nByteForce += qhomenr * nindA_prot * sizeof(wd->dkAbar[0]);

        snew(wd->Orientational_Av, nindA_prot * nabs);
        nByteForce += nindA_prot * DIM * nIvalues;

        nByte += nByteForce;
    }

    wd->nByteAlloc = nByte;
    wt->wd = wd;

    for (int i = 0; i < (cr->nnodes - cr->npmenodes); i++) {
        if (cr->nodeid == i) {
            if (wr->inputParams->bCalcForces) {
                fprintf(stderr, "Node %2d) Allocated %8g MB to store averages "
                        "(%4g MiB for intensities, %8g MB for forces) (scattering type %d).\n",
                        cr->nodeid, 1.0 * wd->nByteAlloc / 1024 / 1024, 1.0 * nByteInten / 1024 / 1024,
                        1.0 * nByteForce / 1024 / 1024, t);
            } else {
                fprintf(stderr, "Node %2d) Allocated %8g MiB to store averages (scattering type %d)\n",
                        cr->nodeid, 1.0 * wd->nByteAlloc / 1024 / 1024, t);
            }
        }

        if (PAR(cr)) {
            gmx_barrier(cr->mpi_comm_mygroup);
        }
    }
}

void done_waxs_datablock(t_waxsrec* wr) {
    waxs_datablock* wd;

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        wd = wr->wt[t].wd;

        sfree(wd->A);
        sfree(wd->avA);
        sfree(wd->av_ReA_2);
        sfree(wd->av_ImA_2);
        sfree(wd->avAsq);
        sfree(wd->avA4);
        sfree(wd->D);
        sfree(wd->I);
        sfree(wd->varI);
        sfree(wd->IA);
        sfree(wd->varIA);
        sfree(wd->avAqabs_re);
        sfree(wd->avAqabs_im);

        if (!wr->inputParams->bVacuum) {
            sfree(wd->IB);
            sfree(wd->Ibulk);
            sfree(wd->varIB);
            sfree(wd->varIbulk);
            sfree(wd->avBqabs_re);
            sfree(wd->avBqabs_im);
            sfree(wd->I_avAmB2);
            sfree(wd->I_varA);
            sfree(wd->I_varB);
            sfree(wd->I_scaleI0);
            sfree(wd->varI_avAmB2);
            sfree(wd->varI_varA);
            sfree(wd->varI_varB);
            sfree(wd->B);
            sfree(wd->avB);
            sfree(wd->av_ReB_2);
            sfree(wd->av_ImB_2);
            sfree(wd->avBsq);
            sfree(wd->avB4);
            sfree(wd->re_avBbar_AmB);
        }

        if (wr->inputParams->bCalcForces) {
            sfree(wd->dkI);
            sfree(wd->dkAbar);
            sfree(wd->Orientational_Av);
        }

        sfree(wd);
        wr->wt[t].wd = nullptr;
    }
}

/** Compute and write how many q-vectors are handled on each node */
static void calculate_node_qload(int nodeid, int nnodes, int nq, int* qhomenr, int* qstart, int* ind, int nabs) {
    /* Make sure that q-vectors of the same |q| are on the same node, needed when using multiple GPUs */
    int qAbsPerNode = nabs / nnodes;
    int rem = nabs - qAbsPerNode * nnodes;

    int iabsHomenr, iabsStart;

    if (nodeid >= rem) {
        iabsStart = nodeid * qAbsPerNode + rem;
        iabsHomenr = qAbsPerNode;
    } else {
        iabsStart = nodeid * (qAbsPerNode + 1);
        iabsHomenr = qAbsPerNode + 1;
    }

    *qstart = ind[iabsStart];
    *qhomenr = ind[iabsStart + iabsHomenr] - ind[iabsStart];
    fflush(stdout);

    if (*qhomenr > 1) {
        fprintf(stderr, "Node %2d) |q| indices range: %2d - %2d    q-vec indices range: %4d - %4d (of %d total)\n",
                nodeid, iabsStart, iabsStart + iabsHomenr - 1, *qstart, *qstart + *qhomenr - 1, nq);
    } else {
        fprintf(stderr, "Node %2d) No q-vectors on this node.\n", nodeid);
    }

    if ((*qstart + *qhomenr > nq) or (*qhomenr < 0) or (*qstart < 0)) {
        gmx_fatal(FARGS, "Inconsistent q vector distribution on node %d: qstart = %d  qhomenr = %d  nq = %d\n",
                nodeid, *qstart, *qhomenr, nq);
    }
}

static void gen_qvecs_accounting(t_waxsrec* wr, t_commrec* cr, int nnodes, int t) {
    waxs_datablock* wd = wr->wt[t].wd;
    int nq = wr->wt[t].qvecs->n;

    /** For the master, generate a comprehensive list */
    if (MASTER(cr)) {
        snew(wd->masterlist_qhomenr, nnodes);
        snew(wd->masterlist_qstart, nnodes);

        for (int i = 0; i < nnodes; i++) {
            int qstart, qhomenr;

            calculate_node_qload(i, nnodes, nq, &qhomenr, &qstart, wr->wt[t].qvecs->ind, wr->wt[t].qvecs->nabs);

            wd->masterlist_qhomenr[i] = qhomenr;
            wd->masterlist_qstart[i] = qstart;
        }

        fprintf(stderr, "Size and offsets of q-vecs constructed on master node for communications.\n");
    } else {
        wd->masterlist_qhomenr = nullptr;
        wd->masterlist_qstart = nullptr;
    }
}

static void free_qvecs_accounting(waxs_datablock* wd) {
    if (wd) {
        sfree(wd->masterlist_qhomenr);
        wd->masterlist_qhomenr = nullptr;
    }

    if (wd) {
        sfree(wd->masterlist_qstart);
        wd->masterlist_qstart = nullptr;
    }
}

/** Generate the list of q-vectors where we compute A(q), B(q), and D(q) */
t_spherical_map* gen_qvecs_map(real minq, real maxq, int nqabs, int J, gmx_bool bDebug, t_commrec* cr,
        gmx_envelope* envelope, int Jmin, real Jalpha, gmx_bool bVerbose) {

    int nWaxsNodes, nodeid;
    if (cr) {
        nWaxsNodes = cr->nnodes - cr->npmenodes;
        nodeid = cr->nodeid;
    } else {
        nWaxsNodes = 1;
        nodeid = 0;
    }

    if (nodeid >= nWaxsNodes) {
        gmx_fatal(FARGS, "Inconsistency in WAXS code: nodeid = %d, nWaxsNodes = %d\n", nodeid, nWaxsNodes);
    }

    t_spherical_map* qvecs;
    snew(qvecs, 1);

    real dq = (nqabs > 1) ? (maxq - minq) / (nqabs - 1) : 0.0;

    snew(qvecs->abs, nqabs);
    snew(qvecs->ind, nqabs + 1);

    qvecs->ind[0] = 0;
    qvecs->q = nullptr;
    qvecs->iTable = nullptr;
    qvecs->n = 0;
    qvecs->nabs = nqabs;

    real D = 0.;

    if (J <= 0) {
        /** Automatic detemination of J = alpha * (D*q)^2, but not smaller than wr->Jmin */
        if (!gmx_envelope_bHaveSurf(envelope)) {
            gmx_fatal(FARGS, "mdp option waxs-nsphere is zero, meaning that the number of q-vectors per |q|\n"
                    "is determined automatically. For that purpose, however, the envelope must be generated\n"
                    "before starting mdrun. Use g_genenv and provide the envelope file with the environment\n"
                    "variable GMX_ENVELOPE_FILE.\n");
        }

        /** Get maximum diameter of envelope */
        D = gmx_envelope_diameter(envelope);

        if (bVerbose) {
            printf("\nAutomatic selection of number of q-vectors per |q|: Using Jmin = %d, alpha = %g, D = %g nm:\n",
                    Jmin, Jalpha, D);
        }
    }

    int thisJ;

    for (int iabs = 0; iabs < nqabs; iabs++) {
        real qabs = minq + iabs * dq;

        if (qabs < 1e-5) {
            /** At q==0. we always use only J = 1 */
            thisJ = 1;
        } else {
            if (J > 0) {
                /** constant J over the entire q-range, given by waxs-nsphere */
                thisJ = J;
            } else {
                /** automatic selection of J = MAX(Jmin , alpha*(D*J)^2 */
                thisJ = round(Jalpha * gmx::square(qabs * D));

                if (thisJ < Jmin) {
                    thisJ = Jmin;
                }
            }
        }

        if (bVerbose and J <= 0) {
            printf("\tq = %8g   J = %d\n", qabs, thisJ);
        }

        /** Store absolute q, extend ind and q */
        qvecs->abs[iabs] = qabs;
        qvecs->ind[iabs + 1] = qvecs->ind[iabs] + thisJ;
        qvecs->n += thisJ;

        srenew(qvecs->q, qvecs->n);
        srenew(qvecs->iTable, qvecs->n);

        double fact = sqrt(M_PI * thisJ);

        for (int j = 0; j < thisJ; j++) {
            /** Spiral method */
            double tmp = (2.0 * (j + 1) - 1. - thisJ) / thisJ;

            double theta = acos(tmp);
            double phi = fact * asin(tmp);

            real qx = qabs * cos(phi) * sin(theta);
            real qy = qabs * sin(phi) * sin(theta);
            real qz = qabs * cos(theta);

            /** Fill qvecs->q vector by thisJ vectors */
            int jj = qvecs->ind[iabs] + j;

            qvecs->q[jj][XX] = qx;
            qvecs->q[jj][YY] = qy;
            qvecs->q[jj][ZZ] = qz;

            /**
             * Store the index of the absolute value. Used for table of atomic scattering factors as a function
             * of qabs: aff_table[sftype][iabs]
             */
            qvecs->iTable[jj] = iabs;
        }
    }

    if (cr == nullptr) {
        qvecs->qstart = 0;
        qvecs->qhomenr = qvecs->n;
    } else {
        /** Store for which q the scattering will be computed on this node */
        calculate_node_qload(nodeid, nWaxsNodes, qvecs->n, &(qvecs->qhomenr), &(qvecs->qstart), qvecs->ind, qvecs->nabs);

        if (PAR(cr)) {
            gmx_barrier(cr->mpi_comm_mygroup);
        }

        for (int j = 0; j < nWaxsNodes; j++) {
            if (nodeid == j) {
                fprintf(stderr, "Node %d uses %d q's between %d and %d\n",
                        nodeid, qvecs->qhomenr, qvecs->qstart, qvecs->qstart + qvecs->qhomenr - 1);
            }

            if (PAR(cr)) {
                gmx_barrier(cr->mpi_comm_mygroup);
            }
        }

        if (PAR(cr)) {
            gmx_barrier(cr->mpi_comm_mygroup);
        }
    }

    if (cr == nullptr or MASTER(cr)) {
        fprintf(stderr,"\n");
    }

    /** Output the spheremap vectors to assigned debug file. */
    if (bDebug and (cr == nullptr or MASTER(cr))) {
        FILE* fp = fopen("debug_spiral.xyz", "w");

        fprintf(fp, "# The spiral method of the spheremap\n");
        fprintf(fp, "# n = %i\n", qvecs->n);

        for (int j = 0; j < qvecs->n; j++) {
            fprintf(fp, "%g %g %g \n", qvecs->q[j][XX], qvecs->q[j][YY], qvecs->q[j][ZZ]);
        }

        fclose(fp);
        fp = fopen("debug_spiral.pdb","w");

        for (int j = qvecs->ind[nqabs - 1]; j < qvecs->ind[nqabs]; j++) {
            /** Make sure the coordinates are not getting too large for a PDB file */
            double fact = 50. / maxq;

            fprintf(fp, "ATOM  %5d %4s %3s %1s%4d    %8.3f%8.3f%8.3f%6.2f%6.2f\n",
                    j % 99999 + 1, "XX", "XXX", "", 1, fact * qvecs->q[j][XX], fact * qvecs->q[j][YY], fact * qvecs->q[j][ZZ], 1.0, 0.0);
        }

        fclose(fp);
        fprintf(stderr, "Wrote debug_spiral.xyz and debug_spiral.pdb for debugging spiral method.\n");
    }

    return qvecs;
}


void init_waxs_md(t_forcerec* fr, t_commrec* cr, const gmx_multisim_t* ms, t_inputrec* ir, const gmx_mtop_t* top_global,
        const gmx_output_env_t* oenv, double t0, const char* runFilePath, const char* trajectoryFilePath,
        const char* fnOut, const char* fnScatt, t_state* state_local, gmx_bool bRerunMD, gmx_bool bWaterOptSet,
        gmx_bool bReadI, gmx_bool started_from_checkpoint, gmx_bool use_gpu) {
    if (t0 > 0.) {
        gmx_fatal(FARGS, "Starting from t is currently not implemented.\n");
    }

    int nsltype;
    swaxs::Solvent* solvent = nullptr;

    if (started_from_checkpoint) {
        /* At present, you need to rerun grompp to restart a SAXS-driven MD. This is required
           to make sure that the posres reference coordinates are updated. Posres is used during
           tau < simtime < 2tau to make sure the structure is not drifting due to the
           absence of the SAXS-derived forces */
        gmx_fatal(FARGS, "With WAXS-MD, you cannot restart from a checkpoint file. Rerun grompp and start with a new tpr file.\n"
                "This is required to make sure that the posres reference coordinates are updated.\n");
    }

    fr->waxsrec = init_t_waxsrec();
    fr->waxsrec->inputParams = std::make_unique<swaxs::InputParams>();

    fr->waxsrec->inputParams->bUseGPU = use_gpu;

#if GMX_GPU != GMX_GPU_CUDA
    fr->waxsrec->inputParams->bUseGPU = FALSE;
#endif

    gmx_warning("GROMACS-SWAXS: %s\n", fr->waxsrec->inputParams->bUseGPU ? "going to use GPU" : "will not use GPU");

    t_waxsrec* wr = fr->waxsrec;

    wr->inputParams->bDoingMD = (ir != nullptr);

    /** As soon as we have a targt SAXS curve, we compute the SAXS potential */
    wr->inputParams->bCalcPot = bReadI;

    /** Compute forces only if waxs_tau is positive, we don't do a rerun, and we have a target SAXS curve */
    wr->inputParams->bCalcForces = (!bRerunMD and (ir->waxs_tau > 0) and bReadI);

#ifdef RERUN_CalcForces
    // JUST FOR TEST - use only in case you know what you are doing!
    bRerunMD = FALSE ;
    wr->inputParams->bCalcForces = TRUE;  // TEST!!!
#endif

    if (ir == nullptr) {
        gmx_fatal(FARGS, "Error in init_waxs_md(), input record not initialized\n");
    }

    if (MASTER(cr) and (!bRerunMD and ir->waxs_tau <= 0)) {
        fprintf(stderr, "WAXS-MD: Found waxs-tau <= 0 : will NOT calculate forces.\n");
    }

    if (MASTER(cr)) {
        fprintf(stderr, "WAXS-MD: Using SIMD level: %s\n", waxs_simd_string());
    }

    if (bReadI and !bRerunMD and !(ir->waxs_tau > 0)) {
        gmx_fatal(FARGS, "You are running an MD simulation with a target SAXS curve, but waxs-tau is <=0 (%g)\n"
                "For a SAXS-driven MD simulation, you should use a positive waxs-tau (e.g. 200ps)\n", ir->waxs_tau);
    }

    if (MASTER(cr)) {
        fprintf(stderr, "WAXS-MD: We do %scalculate the potential\n", wr->inputParams->bCalcPot ? "" : "NOT ");
        fprintf(stderr, "WAXS-MD: We do %scalculate forces.\n", wr->inputParams->bCalcForces ? "" : "NOT ");
        printf("WAXS-MD: Will compute WAXS patterns on %d nodes\n", cr->nnodes - cr->npmenodes);
    }

    if (cr->nodeid >= (cr->nnodes - cr->npmenodes)) {
        gmx_fatal(FARGS, "Inconsnsitency in init_waxs_md, nodeid = %d, but expected only PP %d nodes (nnodes = %d, npmenodes = %d)\n",
                cr->nodeid, cr->nnodes - cr->npmenodes, cr->nnodes, cr->npmenodes);
    }

    /* For the moment has to be implemented later */
    if (gmx_omp_nthreads_get(emntDefault) > 1) {
        printf("WAXS-MD: NOTE: OpenMP paralellization is supported but not fully optimized yet!\n");
    }

    /**
     * We compute the WAXS forces after calculating the virial in do_force(). Therefore,
     * it is anyway not added to the virial. No need to set fr->bF_NoVirSum=TRUE
     * Note: If we decide to compute WAXS forces before computing the virial, we must
     * add the WAXS force to f_novirsum and set fr->bF_NoVirSum = TRUE NOT here but in
     * forcerec.c. Otherwise a segfault would occur in do_force().
     */
    // fr->bF_NoVirSum = fr->bF_NoVirSum or wr->inputParams->bCalcForces;

    /** Store a pointer on the local state - required for dd_collect_state() within do_force */
    if (state_local) {
        wr->local_state = state_local;
    }

    /** Copy values to waxsrec to make the rest source-independent. */
    if (wr->inputParams->bDoingMD) {
        copy_ir2waxsrec(wr, ir, cr);
    }

    if (MASTER(cr)) {
        if (!wr->inputParams->bVacuum) {
            solvent = new swaxs::Solvent(runFilePath, trajectoryFilePath, oenv, TRUE, wr->inputParams->nfrsolvent);

            wr->solvent = solvent;
            wr->inputParams->nfrsolvent = solvent->nFrames;

            if (wr->inputParams->bDoingNeutron and !solvent->hasNeutron()) {
                gmx_fatal(FARGS, "Doing neutron scattering, but found no neutron scattering length in solvent tpr file.\n"
                        "Use `scatt-coupl = neutron` in your solvent .mdp file.");
            }
        } else if (wr->inputParams->bDoingMD and bWaterOptSet) {
            gmx_fatal(FARGS, "No waxs_solvent was specified in main simulation, but options\n"
                    " to provide excluded solvent was passed to mdrun\n");
        }
    }

    /* Broadcast values that were changed on MASTER after reading waxs solvent.
     * Required for all nodes to keep in step when determining bDoingSolvent */
    if (PAR(cr)) {
        gmx_bcast(sizeof(int), &wr->inputParams->nfrsolvent, cr->mpi_comm_mygroup);
    }

    /* read scattering curve to which we couple and broadcast it */
    if (wr->inputParams->bCalcPot) {
        read_waxs_curves(fnScatt, wr, cr);
    }

    /* Redirect the atomtypes in waxs-solv to existing types and allocate waxsrec to receive these types. */
    if (MASTER(cr) and solvent) {
        redirect_md_solventtypes(solvent->topology.get(), &top_global->scattTypes);
    }

    /* init envelope */
    if (getenv("GMX_ENVELOPE_FILE") == nullptr) {
        gmx_fatal(FARGS, "Environment varialbe GMX_ENVELOPE_FILE not found. Please define it to the file name of\n"
                "the evelope (such as envelope.py), written by g_genenv. In addition, define GMX_WAXS_FIT_REFFILE\n"
                "to the reference file written by g_genenv.\n");
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        wr->wt[t].envelope = gmx_envelope_init_md(-1, cr, MASTER(cr));
    }

    swaxs::readEnvironmentVariables(wr, cr);

    /* setup q */
    /* boolean == TRUE means write debug output */
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        if (wr->wt[t].nq <= 0) {
            gmx_fatal(FARGS, "Number of q vales must be >= 1. Fixs waxs-nq mdp option.\n");
        }

        wr->wt[t].qvecs = gen_qvecs_map(wr->wt[t].minq, wr->wt[t].maxq, wr->wt[t].nq, wr->inputParams->J,
                t == 0 and wr->inputParams->verbosityLevel > 1, cr, wr->wt[t].envelope, wr->inputParams->Jmin, wr->inputParams->Jalpha, MASTER(cr) and t == 0);
    }

    if (wr->inputParams->nfrsolvent > 0) {
#if GMX_GPU_CUDA
        /** Unit FT is only calculated once. It is therefore also only copied once to GPU */
        if (wr->inputParams->bUseGPU == TRUE) {
            push_unitFT_to_GPU(wr);
        }
#endif
    }

    if (MASTER(cr)) {
        /** Init output stuff */
        if (fnOut) {
            wr->swaxsOutput = std::make_unique<swaxs::SwaxsOutput>(wr, fnOut, oenv);
        }
    }

    if (MASTER(cr)) {
        /* Memory for atom coordinates x, so we can overwrite the coordinates when making the solute whole, etc. */
        wr->x.resizeWithPadding(top_global->natoms);

        /* Short cut array with number of electrons per atom (size top_global->natoms) */
        wr->nElectrons = swaxs::countElectronsPerAtom(top_global);

        prep_md_indices(&wr->indA_prot, &wr->nindA_prot, &wr->indA_sol, &wr->nindA_sol, top_global, TRUE, !wr->inputParams->bVacuum);

        if (solvent) {
            prep_md_indices(nullptr, nullptr, &wr->indB_sol, &wr->nindB_sol, solvent->topology.get(), FALSE, TRUE);
        }

        /** Pick cmtypes and nsltypes from mtop (main simulation and solvent) into continuous array in waxsrec */
        wr->inputParams->cmtypeList = mtop2scattTypeList(top_global, FALSE);

        if (wr->inputParams->bDoingNeutron) {
            wr->inputParams->nsltypeList = mtop2scattTypeList(top_global, TRUE);
        }

        if (solvent) {
            solvent->cromerMannTypes = mtop2scattTypeList(solvent->topology.get(), FALSE);

            if (wr->inputParams->bDoingNeutron) {
                solvent->neutronScatteringLengthTypes = mtop2scattTypeList(solvent->topology.get(), TRUE);
            }
        }

        /**
         * Write protein atoms into beginning of waxsrec->indexA. These will not change
         * throughout the calculations, but solvation shell atoms will be added to indexA.
         */
        snew(wr->indexA, wr->nindA_prot);
        wr->isizeA = wr->nindA_prot;
        wr->indexA_nalloc = wr->nindA_prot;

        for (int i = 0; i < wr->nindA_prot; i++) {
            wr->indexA[i] = wr->indA_prot[i];
        }

        /** Array for atomic coordinates inside the envelope, update every each step */
        snew(wr->atomEnvelope_coord_A, wr->nindA_prot);

        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            snew(wr->wt[t].atomEnvelope_scatType_A, wr->nindA_prot);
        }

        /** Arrays for solute and solvent atom shifts that are inside the envelope */
        snew(wr->shiftsInsideEnvelope_A_prot, wr->nindA_prot);
        snew(wr->shiftsInsideEnvelope_A_sol, wr->nindA_sol);

        if (solvent) {
            snew(wr->shiftsInsideEnvelope_B, wr->nindB_sol);
        }

        /** Init PBC atom. Number-wise center if not defined. If defined, -1 for mdrun-interal numbering */
        if (wr->inputParams->npbcatom == 0) {
            gmx_fatal(FARGS, "Something has gone wrong - pbcatom information is missing!\n");
        }

        /** Get # of molecules in Solute - if ==1, we can make the solute whole using gmx internal routines */
        wr->nSoluteMols = nSoluteMolecules(top_global);

        /** Check for the type of pbcatom fitting. */
        if (wr->pbcatoms[0] == -1) {
            wr->pbcatoms[0] = (wr->nindA_prot + 1) / 2 - 1;
        } else if (wr->pbcatoms[0] < -1) {
            if (wr->nSoluteMols > 1) {
                gmx_fatal(FARGS, "Found waxs-pbc atom = %d, meaning that the protein is made whole using\n"
                        "the distances between number-wise atomic neighbors. However, this is only possible\n"
                        "when there is only 1 molecule in the solute group (found %d)\n",
                        wr->pbcatoms[0] + 1, wr->nSoluteMols);
            }
        }

        fprintf(stderr, "Using %d PBC atoms for WAXS Solute. First atom index (global): %d\n", wr->inputParams->npbcatom,
                wr->pbcatoms[0]);

        /* fprintf(stderr,"Using PBC atom id (mdrun-internal) for WAXS solute: %d\n",wr->pbcatom); */

        /* Rotational-fitting requires the solute indices to be defined. */
        if (wr->inputParams->bRotFit) {
            snew(wr->x_ref, top_global->natoms);
            snew(wr->w_fit, top_global->natoms);

            for (int i = 0; i < top_global->natoms; i++) {
                clear_rvec(wr->x_ref[i]);
            }

            prep_fitting_weights(wr, &wr->ind_RotFit, &wr->nind_RotFit, wr->w_fit, top_global);

            if (wr->inputParams->bHaveRefFile) {
                read_fit_reference(wr->x_ref_file, wr->x_ref, top_global->natoms, wr->indA_prot, wr->nindA_prot,
                        wr->ind_RotFit, wr->nind_RotFit);

                reset_x(wr->nind_RotFit, wr->ind_RotFit, top_global->natoms, nullptr, wr->x_ref, wr->w_fit);
                fprintf(stderr, "Loaded reference file into x_ref.\n");
            } else {
                gmx_fatal(FARGS, "Environtment variable GMX_WAXS_FIT_REFFILE not defined. Define "
                        "it with the fit reference coordinate file written by g_genenv\n");
            }
        }

        /* TODO: Pass seed from command-line */

        uint64_t seed = 0;

        if (seed == 0) {
            seed = gmx::makeRandomSeed();
        }

        wr->rng = new gmx::DefaultRandomEngine(seed);

        if (!wr->inputParams->bVacuum) {
            wr->solvent->getDensity(wr->nindB_sol, wr->indB_sol, wr->shiftsInsideEnvelope_B, wr->wt[0].envelope,
                    wr->inputParams->bFixSolventDensity, wr->inputParams->givenElecSolventDensity, wr->inputParams->verbosityLevel);

            wr->solElecDensAv_SysB = wr->solvent->averageDropletDensity;
        }
    }

    if (PAR(cr)) {
        gmx_bcast(sizeof(real), &wr->solElecDensAv_SysB, cr->mpi_comm_mygroup);
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        /* allocate memory for cumulative averages and init wd->norm (for averaging) to zero. */
        if (PAR(cr)) {
            gmx_bcast(sizeof(int), &wr->nindA_prot, cr->mpi_comm_mygroup);
        }

        alloc_waxs_datablock(cr, wr, wr->nindA_prot, t);

        t_waxsrecType* wt = &wr->wt[t];

        if (wt->type == escatterXRAY) {
            /** Build table of atomic form factors: 1st index: cmtype. 2nd index: absolute value of q. */
            compute_atomic_form_factor_table(&top_global->scattTypes, wt, cr);
        } else if (wt->type == escatterNEUTRON) {
            /** Build table of NSLs, taking deuterium concentration into account. */
            fill_neutron_scatt_len_table(&top_global->scattTypes, wt, wr->inputParams->backboneDeuterProb);
        }

        /** Store qstart and qhomenr on Master. Requires both alloc_datablock and gen_qvecs */
        gen_qvecs_accounting(wr, cr, cr->nnodes - cr->npmenodes, t);
    }

    if (wr->inputParams->bCalcForces) {
        if (PAR(cr)) {
            if (wr->indA_prot == nullptr) {
                snew(wr->indA_prot, wr->nindA_prot);
            }

            gmx_bcast(wr->nindA_prot * sizeof(int), wr->indA_prot, cr->mpi_comm_mygroup);
        }

        snew(wr->fLast, wr->nindA_prot);

        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            snew(wr->wt[t].fLast, wr->nindA_prot);
        }
    }

    /** Count total number of electrons in A system */
    if (MASTER(cr)) {
        for (int i = 0; i < wr->nindA_prot; i++) {
            wr->nElecTotA += wr->nElectrons[wr->indA_prot[i]];
        }

        wr->nElecProtA = wr->nElecTotA;

        for (int i = 0; i < wr->nindA_sol; i++) {
            wr->nElecTotA += wr->nElectrons[wr->indA_sol[i]];
        }

        printf("There are %g electrons in total in solute + solvent (%g per atom on average)\n", wr->nElecTotA,
                wr->nElecTotA / (wr->nindA_prot + wr->nindA_sol));
    }

    /**
     * Save sum of scattering lengths of solute and solvent (# of electrons or # of NSLs).
     * This will be used to estimate the contrast of the solute
     */
    gmx_bool bStochDeuterBackup = wr->inputParams->bStochasticDeuteration;
    wr->inputParams->bStochasticDeuteration = FALSE;

    if (MASTER(cr) and solvent) {
        snew(solvent->averageScatteringLengthDensity, wr->inputParams->nTypes);
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        if (MASTER(cr)) {
            t_waxsrecType* wt = &wr->wt[t];

            if (wt->type == escatterXRAY) {
                wt->soluteSumOfScattLengths = wr->nElecProtA;

                if (solvent) {
                    solvent->averageScatteringLengthDensity[t] = solvent->nElectrons * solvent->averageInverseVolume;
                }
            } else {
                /** Sum NSLs of solute without stochastic deuteration */
                wt->soluteSumOfScattLengths = 0;

                for (int i = 0; i < wr->nindA_prot; i++) {
                    int j = wr->indA_prot[i];

                    nsltype = get_nsl_type(wr, t, wr->inputParams->nsltypeList[j]);
                    wt->soluteSumOfScattLengths += wt->nsl_table[nsltype];
                }

                if (solvent) {
                    double sum = 0;

                    for (int i = 0; i < wr->nindB_sol; i++) {
                        int j = wr->indB_sol[i];

                        nsltype = get_nsl_type(wr, t, solvent->neutronScatteringLengthTypes[j]);
                        sum += wt->nsl_table[nsltype];
                    }

                    solvent->averageScatteringLengthDensity[t] = sum * solvent->averageInverseVolume;
                }
            }
        }
    }

    wr->inputParams->bStochasticDeuteration = bStochDeuterBackup;

    if (MASTER(cr)) {
        printf("\nSum of scattering lengths of solute:\n");

        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            t_waxsrecType* wt = &wr->wt[t];

            printf("\tType %d (%s) sum = %10g %s\n", t, wt->saxssansStr, wt->soluteSumOfScattLengths,
                    (wt->type == escatterXRAY) ? "electrons" : "NSLs");
        }

        printf("\n");

        if (solvent) {
            printf("\nDensities of pure-solvent system:\n");

            for (int t = 0; t < wr->inputParams->nTypes; t++) {
                t_waxsrecType* wt = &wr->wt[t];

                printf("\tType %d (%s)  density = %10g %s/nm3\n",
                        t, wt->saxssansStr, solvent->averageScatteringLengthDensity[t], (wt->type == escatterXRAY) ? "electrons" : "NSLs");
            }

            printf("\n");
        }
    }

    /** Get scattering intensity of the pure buffer. Used to add back oversubtracted buffer */
    if (MASTER(cr)) {
        printf("WAXS-MD: %s back oversubtracted buffer intensity\n",
                (wr->inputParams->bCorrectBuffer and !wr->inputParams->bVacuum) ? "Adding" : "Not adding");
    }

    if (wr->inputParams->bCorrectBuffer and !wr->inputParams->bVacuum) {
        if (wr->inputParams->bDoingNeutron) {
            gmx_fatal(FARGS, "For neutron scattering, only the simple buffer subtraction scheme "
                    "I_sam - I_buf is supported so far.\nChange mdp option waxs-correct-buffer to no.");
        }

        swaxs::solventScatteringFromRdfs(wr, cr, trajectoryFilePath, top_global);
    }

    if (wr->inputParams->isSwaxsEnsemble() and wr->inputParams->ensembleType == swaxs::EnsembleType::None) {
        gmx_fatal(FARGS, "Found > 1 number of states in ensemble (%d), but ensemble type is none.\n", wr->inputParams->ensemble_nstates);
    }

    if (wr->inputParams->isSwaxsEnsemble()) {
        /** For Bayesian ensemble refinement (Shevchuk, Hub, Plos Comp Biol 2017), read the intensities of the other fixed states */
        if (wr->inputParams->ensembleType == swaxs::EnsembleType::BayesianOneRefined)
        {
            swaxs::init_bayesian_ensemble_refinement(wr, cr);
        }
        else if (wr->inputParams->ensembleType == swaxs::EnsembleType::MaxEnt)
        {
            swaxs::init_maxent_ensemble_refinement(wr, ms);
        }
        else
        {
            gmx_fatal(FARGS, "Selected ensemble type %d is not supported.", static_cast<int>(wr->inputParams->ensembleType));
        }
    }

    if (ir->cutoff_scheme == ecutsVERLET) {
#if GMX_GPU_CUDA
        /** Here all data is copied to GPU that will remain constantly there. It initializes the GPU for all scattering types. */
        if (wr->inputParams->bUseGPU) {
            init_gpudata_type(wr);
        }

        fprintf(stderr, "Initiated data for SAXS/SANS calculations on the GPU.\n");
#endif
    }

    /** Init computing time measurements */
    if (MASTER(cr)) {
        wr->timing = std::make_unique<swaxs::Timing>();
    }
}

void done_waxs_md(t_waxsrec* wr) {
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        free_qvecs_accounting(wr->wt[t].wd);

        if (wr->wt[t].wd) {
            done_waxs_datablock(wr);

            if (wr->solvent) {
                delete wr->solvent;
                wr->solvent = nullptr;
            }
        }
    }
}


int countGroups(const SimulationGroups& groups, SimulationAtomGroupType groupType) {
    int n = groups.groups[groupType].size();

    if (n == 1) {
        /* Can be either all or nothing. */
        if (strncmp("rest", *groups.groupNames[groups.groups[groupType][0]], 4) != 0) {
            /* Found real group. */
            n++;
        } else {
            /* Is FAKE. */
        }
    }

    if (n <= 0) {
        gmx_fatal(FARGS, "Got impossible (negative) number of groups. Terminating.\n");
    }

    return n - 1;  /** Subtracts the fake group. */
}
