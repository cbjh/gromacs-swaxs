#pragma once

#include <gromacs/fileio/oenv.h>  // gmx_output_env_t
#include <gromacs/mdlib/forcerec.h>  // t_forcerec
#include <gromacs/mdtypes/commrec.h>  // t_commrec

void done_swaxs(t_forcerec* fr, t_commrec* cr, const gmx_output_env_t* oenv);
