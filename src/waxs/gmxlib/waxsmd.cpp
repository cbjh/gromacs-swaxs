/*
 *  This source file was written by Jochen Hub, with contributions from Po-chia Chen.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>  // GMX_GPU, GMX_MPI, GMX_DOUBLE
#endif

#include "./waxsmd.h"

#include <cstring>  // strlen, strncpy
#include <ctime>  // time, time_t
#include <memory>  // std::make_unique

#include <gromacs/fileio/confio.h>  // write_sto_conf_indexed
#include <gromacs/fileio/trrio.h>  // gmx_trr_write_frame
#include <gromacs/gmxlib/network.h>  // gmx_bcast, gmx_barrier
#include <gromacs/simd/simd.h>  // gmx::SimdReal, gmx_simd_mul_r
#include <gromacs/simd/simd_math.h>  // gmx::sincos
#include <gromacs/topology/mtop_util.h>  // t_atoms, gmx_mtop_global_atoms
#include <gromacs/utility/fatalerror.h>  // gmx_fatal
#include <gromacs/utility/smalloc.h>  // snew, sfree

#include <swaxs/bayesian_ensemble.h> // maximum_likelihood_est_fc_generic, bayesian_gibbs_sampling
#include <swaxs/maxent_ensemble.h>  // maxent_ensemble_average
#include <waxs/envelope/density.h>  // swaxs::envelope_solvent_density_next_frame, swaxs::envelope_solvent_density_add_atom, swaxs::envelope_solvent_density_bcast, swaxs::envelope_solvent_ft, swaxs::envelope_have_solvent_ft, swaxs::envelope_get_solvent_nelec
#include <waxs/envelope/envelope.h>  // gmx_envelope
#include <waxs/envelope/grid_density.h>  // envelope_grid_density_next_frame, envelope_grid_density_add_atom, envelope_grid_density_close_frame
#include <waxs/envelope/volume.h>  // swaxs::envelope_volume
#include <waxs/gmxcomplex.h>  // t_complex_d
#include <waxs/gmxlib/env.h>  // swaxs::read_env_variables
#include <waxs/gmxlib/gmx_envelope.h>
#include <waxs/gmxlib/names.h>  // EWAXS*
#include <waxs/gmxlib/pbcshifts.h>  // get_solvation_shell_periodic, setAtomShiftsInsideEnvelope, summarizeAtomShifts
#include <waxs/gmxlib/sftypeio.h>  // init_t_waxsrecType
#include <waxs/gmxlib/solvent.h>  // swaxs::Solvent
#include <waxs/gmxlib/swaxs_datablock.h>  // waxs_datablock
#include <waxs/gmxlib/swaxs_output.h>  // swaxs::SwaxsOutput
#include <waxs/gmxlib/timing.h>  // swaxs::Timing, SWAXS_STEPS_RESET_TIME_AVERAGES
#include <waxs/gmxlib/waxsmd_utils.h>  // average_stddev_d, average_x2_d, average_xy_d, CMSF_q, swaxs::countElectronsPerAtom, nIndep_Shannon_Nyquist, pearson_d, read_fit_reference, soluteRadiusOfGyration, sum_array_d, sum_squared_residual_d, waxs_prepareSoluteFrame
#include <waxs/gmxlib/init_waxs.h> // read_waxs_curves, read_intensity_and_interpolate
#include <waxs/types/enums.h>  // escatter
#include <waxs/types/waxsrec.h>  // t_waxsrec
#include <waxs/types/waxstop.h>  // t_scatt_types
#include <waxs/gmxlib/cuda_tools/scattering_amplitude_cuda.cuh>  // calculate_dkI_GPU, init_gpudata_type, push_unitFT_to_GPU, update_envelope_solvent_density_GPU, compute_scattering_amplitude_cuda

#define UPDATE_CUMUL_AVERAGE(f, w, fadd) (f = 1.0 / (w) * ((w - 1.0) * (f) + 1.0 * (fadd)))

#define WAXS_WARN_BOX_DIST 0.5


#include <waxs/debug.h>  // swaxs_debug

/** Uncomment some of the following definitions if you want more debugging output */
// #define RERUN_CalcForces
// #define PRINT_A_dkI


static real envelope_volume_wrapper(t_waxsrec* wr) {
    return swaxs::envelope_volume(wr->wt[0].envelope);
}

/** Get total number of electrons or NSLs inside the enelope */
static real number_of_electrons_or_NSLs(t_waxsrecType* wt, int* scatTypeIndex, int isize, const gmx_mtop_t* mtop) {
    real sum = 0;

    for (int i = 0; i < isize; i++) {
        if (wt->type == escatterXRAY) {
            sum += CROMERMANN_2_NELEC(mtop->scattTypes.cromerMannParameters[scatTypeIndex[i]]);
        } else {
            sum += wt->nsl_table[scatTypeIndex[i]];
        }
    }

    return sum;
}

/* Estimate the contrast of the solute. This is used to scale down the SAXS-derived forces.

   Rationale:

   SAXS: When we dispalce a solute groups by some SAXS-derived forces, any gaps will be
   filled by solvent, hence the change in the intensity is smaller than expected from the
   solute atoms alone. A good estimate for this reduced effect of conformational transitions
   on the SAXS/SANS curves is given by the contrast
     (rho_solute-rho_solvent) / rho_solvent.
   In other words: when computing the intensity gradients on solute atoms along, we would
   overestimate the true gradients.

   SANS: This effect is most apparent when doing SANS, here, if we have negative contrast
   in case high D2O concentration, the correcting factor is even negative, flipping the direction
   of SANS-derived forces. In other words: In this case, when the solute moves left, the
   contrast moves to the right.

   KNOWN ISSUES: If different domains have a different perdeuteration level, this
   correction does no work. In this case, we would have to work with buffer-subtracted
   form factors ("reduced form factors"). This may be the best solution for computing
   intensity gradients anyway, but we would need atomic volumes.
*/
static void updateSoluteContrast(t_waxsrec* wr, const t_commrec* cr) {
    if (MASTER(cr) and !wr->solvent) {
        gmx_incons("waxs_solv not available in updateSoluteContrast()\n");
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];

        if (MASTER(cr)) {
            double soluteDensity = wt->soluteSumOfScattLengths / wr->soluteVolAv;
            double solventDensity = wr->solvent->averageScatteringLengthDensity[t];
            wt->soluteContrast = soluteDensity - solventDensity;
            wt->contrastFactor = wt->soluteContrast / soluteDensity;
        }

        if (PAR(cr)) {
            gmx_bcast(sizeof(double), &wt->soluteContrast, cr->mpi_comm_mygroup);
            gmx_bcast(sizeof(double), &wt->contrastFactor, cr->mpi_comm_mygroup);
        }
    }
}

/** Return average electron density inside the envelope (for A or B system) */
static real droplet_density(t_waxsrec* wr, int* index, int isize, double* nElectrons, real* nelecRet) {
    double nelec = 0.;

    for (int i = 0; i < isize; i++) {
        double nel = nElectrons[index[i]];

        if (nel < 0) {
            gmx_fatal(FARGS, "While computing the density of the droplet in droplet_density(), stepped over"
                    " atom %d does not have Cromer-Mann parameters (nElectrons = %g)\n", index[i] + 1, nel);
        }

        nelec += nel;
    }

    /** There is no volume with bVacuum */
    real dens = wr->inputParams->bVacuum ? -1. : nelec / envelope_volume_wrapper(wr);

    *nelecRet = nelec;

    return dens;
}


#define GMX_WAXS_SOLVENT_ROUGHNESS 0.4

static void check_selected_solvent(gmx::ArrayRef<gmx::RVec> xA, gmx::ArrayRef<gmx::RVec> xB, int* indexA, int isizeA,
        int* indexB, int isizeB, gmx_bool bFatal) {
    rvec maxA = { -1e20, -1e20, -1e20 };
    rvec minA = {  1e20,  1e20,  1e20 };
    rvec maxB = { -1e20, -1e20, -1e20 };
    rvec minB = {  1e20,  1e20,  1e20 };

    rvec* x;
    auto* xARaw = as_rvec_array(xA.data());
    auto* xBRaw = as_rvec_array(xB.data());

    /** find smallest and largest coordinate of solvation layer and excluded solvent */
    for (int i = 0; i < isizeA; i++) {
        x = &xARaw[indexA[i]];

        for (int d = 0; d < DIM; d++) {
            if ((*x)[d] < minA[d]) {
                minA[d] = (*x)[d];
            }

            if ((*x)[d] > maxA[d]) {
                maxA[d] = (*x)[d];
            }
        }
    }

    for (int i = 0; i < isizeB; i++) {
        x = &xBRaw[indexB[i]];

        for (int d = 0; d < DIM; d++) {
            if ((*x)[d] < minB[d]) {
                minB[d] = (*x)[d];
            }

            if ((*x)[d] > maxB[d]) {
                maxB[d] = (*x)[d];
            }
        }
    }

    for (int d = 0; d < DIM; d++) {
        if (fabs(maxA[d] - maxB[d]) > GMX_WAXS_SOLVENT_ROUGHNESS or fabs(minA[d] - minB[d]) > GMX_WAXS_SOLVENT_ROUGHNESS) {
            char buf[1024];
            const char dimLetter[3][2] = { "x", "y", "z" };

            sprintf(buf, "Solvation layer and excluded solvent are different in direction %s\n"
                    "Found minA / minB = %g / %g -- maxA / maxB = %g / %g\n", dimLetter[d], minA[d], minB[d], maxA[d], maxB[d]);

            if (bFatal) {
                gmx_fatal(FARGS, "%s", buf);
            } else {
                fprintf(stderr, "\n\nWARNING - WARNING - WARNING \n\n%s\n\nWARNING - WARNING - WARNING\n\n", buf);
            }
        }
    }
}

/* Get solvent atoms inside the envelope of solute/solvent and pure-solvent system */
static void get_solvation_shell(gmx::ArrayRef<gmx::RVec> x, gmx::ArrayRef<gmx::RVec> xex, t_waxsrec* wr,
        const gmx_mtop_t* mtop, gmx_mtop_t* mtopex, PbcType pbcType, const matrix box) {
    swaxs_debug("Begin of get_solvation_shell()\n");

    int nOutside = 0;
    const int nWarnOutsideMax = 10;

    /** Check if all atoms solute atoms are within envelope */
    for (int j = 0; j < wr->nindA_prot; j++) {
        if (!gmx_envelope_isInside(wr->wt[0].envelope, x[wr->indA_prot[j]])) {
            nOutside++;

            if (nOutside <= nWarnOutsideMax) {
                fprintf(stderr, "WARNING, atom %d of solute x = (%6.2f %6.2f %6.2f) is outside of the envelope\n",
                        wr->indA_prot[j] + 1, x[wr->indA_prot[j]][XX], x[wr->indA_prot[j]][YY], x[wr->indA_prot[j]][ZZ]);
            }

            if (nOutside == nWarnOutsideMax) {
                fprintf(stderr, "       NOTE: Will not report more warnings of atoms outside of envelope\n");
            }
        }
    }

    if (nOutside) {
        fprintf(stderr, "WARNING, %d atoms outside of envelope\n", nOutside);
    }

    /** Check if solvation layer too thin */
    real mindist;
    int iMindist;
    gmx_bool bMinDistToOuter;
    gmx_envelope_minimumDistance(wr->wt[0].envelope, x, wr->indA_prot, wr->nindA_prot, &mindist, &iMindist, &bMinDistToOuter);

    if (mindist < wr->inputParams->solv_warn_lay) {
        wr->nSolvWarn++;

        fprintf(stderr, "\n\n*** WARNING ***\n"
                "The current solvation layer is only %.3f nm thick.\n"
                "\tWarning at %g\n"
                "\tClosest atom #: %d, distance to %s envelope surface = %g\n\n",
                mindist, wr->inputParams->solv_warn_lay, wr->indA_prot[iMindist], bMinDistToOuter ? "outer" : "inner", mindist);
    }

    real envMaxR2 = gmx::square(gmx_envelope_maxR(wr->wt[0].envelope));

    /* Construct solvation layer around solute */
    wr->isizeA = wr->nindA_prot;

    const int allocBlocksize = 100;

    for (int j = 0; j < wr->nindA_sol; j++) {
        int k = wr->indA_sol[j];

        gmx_bool bInside = gmx_envelope_isInside(wr->wt[0].envelope, x[k]);

        if (norm2(x[k]) > envMaxR2 and bInside) {
            gmx_fatal(FARGS, "Envelope says inside, but diff = %g (maxR = %g)\n", sqrt(norm2(x[k])), sqrt(envMaxR2));
        }

        if (bInside) {
            wr->isizeA++;

            if (wr->isizeA >= wr->indexA_nalloc) {
                wr->indexA_nalloc += allocBlocksize;
                srenew(wr->indexA, wr->indexA_nalloc);
            }

            wr->indexA[wr->isizeA - 1] = k;
        }
    }

    /** Get atoms of pure-solvent system inside the envelope. */
    if (wr->bDoingSolvent) {
        wr->isizeB = 0;

        for (int j = 0; j < wr->nindB_sol; j++) {
            int k = wr->indB_sol[j];

            gmx_bool bInside = gmx_envelope_isInside(wr->wt[0].envelope, xex[k]);

            if (norm2(xex[k]) > envMaxR2 and bInside) {
                gmx_fatal(FARGS, "Envelope says inside, but diff = %g (maxR = %g)\n", sqrt(norm2(xex[k])), sqrt(envMaxR2));
            }

            if (bInside) {
                wr->isizeB++;

                if (wr->isizeB >= wr->indexB_nalloc) {
                    wr->indexB_nalloc += allocBlocksize;
                    srenew(wr->indexB, wr->indexB_nalloc);
                }

                wr->indexB[wr->isizeB - 1] = k;
            }
        }
    }

    if (wr->inputParams->verbosityLevel > 1 or wr->waxsStep == 0) {
        char fileName[256];

        sprintf(fileName, "prot+solvlayer_%d.pdb", wr->waxsStep);

        t_atoms* atoms = nullptr;

        snew(atoms, 1);
        *atoms = gmx_mtop_global_atoms(mtop);
        write_sto_conf_indexed(fileName, "Protein/Solute within the envelope",
                atoms, as_rvec_array(x.data()), nullptr, pbcType, box, wr->isizeA, wr->indexA);
        sfree(atoms);

        printf("Wrote %s\n", fileName);

        if (wr->bDoingSolvent) {
            sprintf(fileName, "excludedvolume_%d.pdb", wr->waxsStep);

            snew(atoms, 1);
            *atoms = gmx_mtop_global_atoms(mtopex);
            write_sto_conf_indexed(fileName, "Excluded solvent within the envelope",
                    atoms, as_rvec_array(xex.data()), nullptr, pbcType, box, wr->isizeB, wr->indexB);
            sfree(atoms);

            printf("Wrote %s\n", fileName);
        }
    }

    swaxs_debug("End of get_solvation_shell()\n");
}


/** Updating the electron density inside the envelope. This we do only with scattering type 0 */
static void update_envelope_griddensity(t_waxsrec* wr, gmx::ArrayRef<gmx::RVec> x) {
    swaxs::envelope_grid_density_next_frame(wr->wt[0].envelope, wr->inputParams->scale);

    int i0 = 0, ifinal = 0;

    switch (wr->inputParams->gridDensityMode) {
        case 0:
            /* Average density of solute and hydration layer */
            i0 = 0;
            ifinal = wr->isizeA;
            break;
        case 1:
            /* Average density of solute only */
            i0 = 0;
            ifinal = wr->nindA_prot;
            break;
        case 2:
            /* Average density of hydration layer only only */
            i0 = wr->nindA_prot;
            ifinal = wr->isizeA;
            break;
        default:
            gmx_fatal(FARGS, "Invalid mode for averaging grid density. Found %d, allowed: 0, 1, or 2\n", wr->inputParams->gridDensityMode);
    }

    for (int i = i0; i < ifinal; i++) {
        double nelec = wr->nElectrons[wr->indexA[i]];
        swaxs::envelope_grid_density_add_atom(wr->wt[0].envelope, x[wr->indexA[i]], nelec);
    }

    swaxs::envelope_grid_density_close_frame(wr->wt[0].envelope);
}

static void update_envelope_solvent_density(t_waxsrec* wr, const t_commrec* cr, gmx::ArrayRef<gmx::RVec> x, int t) {
    t_waxsrecType* wt = &wr->wt[t];

    /* Update solvent density around the protein inside envelope (cumulative average) */
    swaxs::envelope_solvent_density_next_frame(wt->envelope, wr->inputParams->scale);

    /** Master adds new electrons to the sphere */
    if (MASTER(cr)) {
        /** Loop over solvation shell atoms */
        for (int i = wr->nindA_prot; i < wr->isizeA; i++) {
            /** No matter if wt->type is XRAY or Neutron, we here collect the electron density. */
            double nelec = wr->nElectrons[wr->indexA[i]];
            swaxs::envelope_solvent_density_add_atom(wt->envelope, x[wr->indexA[i]], nelec);
        }
    }

    swaxs::envelope_solvent_density_bcast(wt->envelope, cr);

    /** Update FT of solvent density in the first steps a few times */
    bool recalculate_solvent_ft = (!swaxs::envelope_have_solvent_ft(wt->envelope) or wr->waxsStep <= 5
            or wr->waxsStep == 10 or wr->waxsStep == 15 or wr->waxsStep == 20 or wr->waxsStep == wr->inputParams->nfrsolvent - 1);

    if (wr->inputParams->tau > 0.) {
        /** with exponential averaging, update FT once every tau/4 (first time after tau/8) */
        recalculate_solvent_ft = recalculate_solvent_ft or ((wr->waxsStep + ((int)wr->inputParams->tausteps) / 8) % ((int)wr->inputParams->tausteps / 4)) == 0;
    } else if (wr->inputParams->tau < -1e-6) {
        /** With non-weighted average (typically in a rerun) update the FT a couple of times */
        recalculate_solvent_ft = recalculate_solvent_ft or wr->waxsStep == 30 or wr->waxsStep == 50
                or wr->waxsStep == 100 or wr->waxsStep == 300 or wr->waxsStep == 1000;
    } else if (fabs(wr->inputParams->tau) < 1e-6) {
        recalculate_solvent_ft = TRUE;
    }

    if (recalculate_solvent_ft and MASTER(cr)) {
        printf("Recalculating the FT of the solvent density around solute in waxs step %d (tau = %g, scattering type %d)\n",
                wr->waxsStep, wr->inputParams->tau, t);
    }

    /* Update the FT of the solvent in A if recalculate_solvent_ft */
    if (recalculate_solvent_ft) {
        wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::Fourier, *cr);
    }

    int qstart = wt->qvecs->qstart;
    int qhomenr = wt->qvecs->qhomenr;
    rvec* q = wt->qvecs->q;
    real* ft_re;
    real* ft_im;
    swaxs::envelope_solvent_ft(wt->envelope, q + qstart, qhomenr, recalculate_solvent_ft, &ft_re, &ft_im);

    if (recalculate_solvent_ft) {
        wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::Fourier, *cr);
    }

    wr->bRecalcSolventFT_GPU = recalculate_solvent_ft;
}

/* If set to 1, then I(0) is shifted to the requested value by adding electrons to A-system (biomolecule+water).
   We typically rather add or remove a uniform density to the B-system (pure solvent), so let us set to 0.
*/
#define WAXS_SCALEI0_ADD_TO_SOLVENT_OF_A 0

static void scaleI0_getAddedDensity(t_waxsrec* wr, const t_commrec* cr) {
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        /* A few checks first */
        if (wt->targetI0 == -1.) {
            gmx_fatal(FARGS, "Trying to scale I(q=0), but no target I(q=0) found. Provide input intensity\n or "
                    "an environment varialbe GMX_WAXS_SCALE_I0, or set mdp option waxs-scale-i0 to no\n");
        }

        if (wt->qvecs->abs[0] != 0.) {
            gmx_fatal(FARGS, "Trying to scale I(q=0) to target I(q=0), but q = 0 is not computed\n");
        }

        if (wr->inputParams->bVacuum) {
            gmx_fatal(FARGS, "Cannot scale I(q=0) in vacuum since electron density is added to the solvent aound the protein\n");
        }

        /* Should this be taken from an input or from the experimental curve? It is here a 2nd time for rerun purposes. MH*/
        char* buf = nullptr;

        if ((buf = getenv("GMX_WAXS_I0")) != nullptr) {
            wt->targetI0 = atof(buf);
            fprintf(stderr, "\nWAXS-MD rerunpart: Found environment variable GMX_WAXS_I0 = %.1f\n", wt->targetI0);
        } else {
            wt->targetI0 = wt->Iexp[0];

            if (MASTER(cr)) {
                fprintf(stderr, "\nWAXS-MD rerunpart: Did not  found GMX_WAXS_I0, taking the experimental value: %.1f\n",
                        wt->targetI0);
            }
        }

        if (wt->targetI0 == 0) {
            fprintf(stderr, "\nTarget I0 is: %.1f. This should not happen.\n", wt->targetI0);
        }

        /* End of new part MH*/
        int qstart = wt->qvecs->qstart;
        int qhomenr = wt->qvecs->qhomenr;

        double c = 0.;

        if (qstart == 0) {
            /* This should be on the master with the smallest |q|, but double-check: */
            if (!MASTER(cr)) {
                gmx_fatal(FARGS, "scaleI0_getAddedDensity(): expected to be here on the Master\n");
            }

            if (norm2(wt->qvecs->q[0]) != 0.) {
                gmx_fatal(FARGS, "On this node, have qstart = 0, but norm2(q[0]) = %g\n", norm2(wt->qvecs->q[0]));
            }

            double Inow = gmx::square(wd->avA[0].re - wd->avB[0].re) + (wd->avAsq[0] - gmx::square(wd->avA[0].re))
                                                                        - (wd->avBsq[0] - gmx::square(wd->avB[0].re));
            double deltaI = wt->targetI0 - Inow;
            double deltaAq0 = -(wd->avA[0].re - wd->avB[0].re) + sqrt(gmx::square(wd->avA[0].re - wd->avB[0].re) + deltaI);

            if (WAXS_SCALEI0_ADD_TO_SOLVENT_OF_A) {
                double envelope_nelec = swaxs::envelope_get_solvent_nelec(wt->envelope);

                /* Add density of c * rho[solvent] to A system*/
                c = deltaAq0 / envelope_nelec;

                printf("Adding %g electrons to A (%.3f %% increase) in order to scale I(q=0) to %g\n",
                        deltaAq0, 100. * c, wt->targetI0);

                if (wt->type == escatterNEUTRON) {
                    gmx_fatal(FARGS, "WAXS_SCALEI0_ADD_TO_SOLVENT_OF_A is not implemented with Neutron scattering.");
                }
            } else {
                c = deltaAq0 / swaxs::envelope_volume(wt->envelope);
                printf("Removing %g electrons from B (%.3f %% decrease) in order to scale I(q=0) to %g\n",
                        deltaAq0, 100. * c / wr->solvent->averageDensity, wt->targetI0);
            }
        }

        if (PAR(cr)) {
            gmx_bcast(sizeof(double), &c, cr->mpi_comm_mygroup);
        }

        real* ft_re;
        real* ft_im;

        /** Pick up Fourier transform of solvent within envelope */
        if (WAXS_SCALEI0_ADD_TO_SOLVENT_OF_A) {
            swaxs::envelope_solvent_ft(wt->envelope, wt->qvecs->q + qstart, qhomenr, FALSE, &ft_re, &ft_im);
        } else {
            swaxs::envelope_unit_ft(wt->envelope, wt->qvecs->q + qstart, qhomenr, &ft_re, &ft_im);
        }

        if (wd->avAcorr == nullptr) {
            snew(wd->avAcorr, qhomenr);
        }

        for (int i = 0; i < qhomenr; i++) {
            wd->avAcorr[i].re = c * ft_re[i];
            wd->avAcorr[i].im = c * ft_im[i];
        }
    }
}

/*
 * Fixing the bulk solvent density to the value specified with mdp option waxs-solvdens.
 * See: Chen and Hub, Biophys. J., 107, 435-447 (2014), page 438
 */
static void fix_solvent_density(t_waxsrec* wr, const t_commrec* cr, int t) {
    t_waxsrecType* wt = &wr->wt[t];
    double elec2NSL_factor;

    /** Factor to translate electron density into NSL density at this deuterium concentration */
    if (wt->type == escatterXRAY) {
        elec2NSL_factor = 1;
    } else {
        /**
         * Water has 10 electrons per molecule, translating into NSL per molecule.
         *
         * NOTE: Strictly speaking, we would have to take this from the pure-solvent trajectory
         *       However, since the solvent is mainly water, this would make only a tiny difference.
         */
        double nslPerMol = NEUTRON_SCATT_LEN_O + 2 * ((1. - wt->deuter_conc) * NEUTRON_SCATT_LEN_1H + wt->deuter_conc * NEUTRON_SCATT_LEN_2H);
        elec2NSL_factor = nslPerMol / 10;
    }

    int qstart = wt->qvecs->qstart;
    int qhomenr = wt->qvecs->qhomenr;
    rvec* q = wt->qvecs->q;

    /**
     * Pick up Fourier transform of solvent electron density in A-system, that is of the solvent inside the envelope around the protein.
     * Important: ft_re and ft_im is always in unites of electron density (not NSL density)
     */
    real* ftSolvent_re;
    real* ftSolvent_im;
    swaxs::envelope_solvent_ft(wt->envelope, q + qstart, qhomenr, FALSE, &ftSolvent_re, &ftSolvent_im);

    /** Add a small density to solvent around protein, proportional to the density already present */
    if (PAR(cr)) {
        gmx_bcast(sizeof(double), &wr->solElecDensAv, cr->mpi_comm_mygroup);
    }

    /** Factor required to get *electron* density to the given density */
    real delta_rho_over_rho_bulk = (wr->inputParams->givenElecSolventDensity - wr->solElecDensAv) / wr->solElecDensAv;

    if (fabs(delta_rho_over_rho_bulk) > 0.05) {
        fprintf(stderr, "\nWARNING, you are correcting the density of the PROTEIN SYSTEM by more than 5%%.\n"
                "   delta_rho / rho = %g %%\n"
                "This may indicate that your waxs-solvent group is lacking atoms, that your\n"
                "waxs-solvdens is wrong, or that you have some other problem. The solvent density\n"
                "correction is meant for small corrections, typically by 1%% or less.\n\n", delta_rho_over_rho_bulk * 100);
    }

    if (wr->inputParams->bUseGPU == FALSE) {
        for (int i = 0; i < qhomenr; i++) {
            /** ftSolvent_re/ftSolvent_im are in units "number of electrons". For neutron scattering, translate to units number of NSL */
            wt->wd->A[i].re += delta_rho_over_rho_bulk * elec2NSL_factor * ftSolvent_re[i];
            wt->wd->A[i].im += delta_rho_over_rho_bulk * elec2NSL_factor * ftSolvent_im[i];
        }
    }

    int wstep = wr->waxsStep + 1;

    if (MASTER(cr) and norm2(wt->qvecs->q[0]) == 0. and wt->qvecs->qstart == 0) {
        wr->nElecAddedA = 1.0 / wstep * ((wstep - 1) * wr->nElecAddedA + delta_rho_over_rho_bulk * ftSolvent_re[0]);

        if (wt->type == escatterXRAY) {
            fprintf(wr->swaxsOutput->fpLog, "Solvent density correction to %g e/nm3:\n    Added %g electrons to solvation shell around solute"
                    " (added density of %g)\n", wr->inputParams->givenElecSolventDensity, delta_rho_over_rho_bulk * ftSolvent_re[0],
                    wr->inputParams->givenElecSolventDensity - wr->solElecDensAv);
        } else {
            fprintf(wr->swaxsOutput->fpLog, "Solvent density correction to %g e/nm3\n    Added %g electrons (%g NSLs) to solvation shell around solute"
                   " (added electron density of %g / NSL density of %g)\n", wr->inputParams->givenElecSolventDensity,
                    delta_rho_over_rho_bulk * ftSolvent_re[0], delta_rho_over_rho_bulk * elec2NSL_factor * ftSolvent_re[0],
                    wr->inputParams->givenElecSolventDensity - wr->solElecDensAv,
                    elec2NSL_factor * (wr->inputParams->givenElecSolventDensity - wr->solElecDensAv));
        }
    }

    real* ftEnv_re;
    real* ftEnv_im;

    /** In either of these cases, we will use the unit transform */
    if ((wr->bDoingSolvent or wt->wd->DerrSolvDens)) {
        swaxs::envelope_unit_ft(wt->envelope, q + qstart, qhomenr, &ftEnv_re, &ftEnv_im);
    }

    if (wr->bDoingSolvent) {
        /** Add a small constant density to the excluded solvent droplet */
        real delta_rho_B = (wr->inputParams->givenElecSolventDensity - wr->solElecDensAv_SysB);

        if (MASTER(cr) and fabs(delta_rho_B / wr->solElecDensAv_SysB) > 0.05) {
            fprintf(stderr, "\nWARNING, you are correcting the density of the SOLVENT SYSTEM by more than 5%%.\n"
                    "   delta_rho / rho = %g %%\n"
                    "This may indicate that your waxs-solvent group is lacking atoms, that your\n"
                    "waxs-solvdens is wrong, or that you have some other problem. The solvent density\n"
                    "correction is meant for small corrections, typically by 1%% or less.\n\n",
                    delta_rho_B / wr->solElecDensAv_SysB * 100);
        }

        if (MASTER(cr) and t == 0) {
            fprintf(wr->swaxsOutput->fpLog, "    Added %g electrons to excluded solvent (added density of %g)\n",
                    delta_rho_B * swaxs::envelope_volume(wt->envelope), delta_rho_B);
            wr->nElecAddedB = 1.0 / wstep * ((wstep - 1) * wr->nElecAddedB + delta_rho_B * swaxs::envelope_volume(wt->envelope));
        }

        /** Corrections in B takes the form of a pure homogenous background. */
        if (wr->inputParams->bUseGPU == FALSE) {
            for (int i = 0; i < qhomenr; i++) {
                /** ft_re/ft_im  are in units number of electrons. For neutron scattering, translate to units number of NSL  */
                wt->wd->B[i].re += delta_rho_B * elec2NSL_factor * ftEnv_re[i];
                wt->wd->B[i].im += delta_rho_B * elec2NSL_factor * ftEnv_im[i];
            }
        }
    }

    if (wt->wd->DerrSolvDens) {
        /**
         * Uncertainty in D(q) due to uncertainty in the relative uncertainty of the solvent density:
         * D[err] = 2 * drho / rho[bulk] * Re{ <A*-B*> . [ F(rho[s]) - rho[bulk].F(envelope) ] },
         * where  drho/rho[bulk] is the relative uncertainty of the solvent given by the mdp option waxs-solvdens-uncert.
         */
        for (int i = 0; i < qhomenr; i++) {
            /** In units electrons or NSL: */
            real AmB_re = wt->wd->avA[i].re - wt->wd->avB[i].re;
            real AmB_im = wt->wd->avA[i].im - wt->wd->avB[i].im;

            /** Always in units electrons, so translate to electrons OR NSL with factor elec2NSL_factor: */
            real FT_diff_re = (ftSolvent_re[i] - wr->inputParams->givenElecSolventDensity * ftEnv_re[i]) * elec2NSL_factor;
            real FT_diff_im = (ftSolvent_im[i] - wr->inputParams->givenElecSolventDensity * ftEnv_im[i]) * elec2NSL_factor;

            wt->wd->DerrSolvDens[i] = 2 * wr->inputParams->solventDensRelErr * (AmB_re * FT_diff_re + AmB_im * FT_diff_im);
        }
    }
}

/** Compute A(q) and B(q) of this frame on the CPU. The CUDA variant is in cuda_tools/scattering_amplitude_cuda.cu */
static void compute_scattering_amplitude(t_complex_d* scatt_amplitude, rvec* atomEnvelope_coord, int* atomEnvelope_type,
        int isize, int nprot, rvec* q, int* iTable, int qhomenr, real** aff_table, real* nsl_table,
        t_complex* grad_amplitude_bar, double* timing_ms) {
    struct timespec t_start;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t_start);

    swaxs_debug("Begin of compute_scattering_amplitude()\n");

    if (aff_table and nsl_table) {
        gmx_fatal(FARGS, "aff_table and nsl_table are both != nullptr !\n");
    }

    if (!aff_table and !nsl_table) {
        gmx_fatal(FARGS, "aff_table and nsl_table are both == nullptr !\n");
    }

    int i;

    for (i = 0; i < qhomenr; i++) {
        scatt_amplitude[i] = cnul_d;
    }

    int nReal = sizeof(gmx::SimdReal) / sizeof(real);

    /** nq2 >= qhomenr and multiple of nReal (the # of reals per register) */
    int nq2 = ((qhomenr - 1) / nReal + 1) * nReal;

    /**
     * IMPORTANT:
     * We must make sure that the float pointers are aligned in memory in same way as the
     * gmx::SimdReal pointers (e.g., gmx::SimdReal is __m256 for AVX_256 or __mm128 for FMA).
     * Otherwise, after casting a float* onto a gmx::SimdReal*, we may have in incorrect
     * aligntment of the gmx::SimdReal* variable, which may cause errors such as Segfaults.
     *
     * For instance, floats are 4-byte-aligned, where as __m256 are 16-byte-aligned.
     *
     * To make sure that the float arrays correctly aligned, they must be alloccated with
     * snew_aligned and freed with sfree_aligned.
     *
     * For an explanation, see e.g.:
     * https://stackoverflow.com/questions/25596379/simd-intrinsics-segmentation-fault
     */

    real* p_aff;
    real* p_qdotx;
    real* re;
    real* im;

    /** Allocate 32-byte alinged float real arrays */
    snew_aligned(p_aff,   nq2, 32);
    snew_aligned(p_qdotx, nq2, 32);
    snew_aligned(re,      nq2, 32);
    snew_aligned(im,      nq2, 32);

    for (int p = 0; p < isize; p++) {
        if (atomEnvelope_type[p] == 10 and p == 0 and FALSE) {
            fprintf(stderr, "scattering Amplitude AT = 10 , atom %d !\n", i);
        }

        /** Put atomic form factors or NSL and phase qdotx in a linear (real*) array */
        if (aff_table) {
            /** Xray scattering, atomic form factor depends on q */
            for (i = 0; i < qhomenr; i++) {
                p_aff[i] = aff_table[atomEnvelope_type[p]][iTable[i]];
                p_qdotx[i] = iprod(q[i], atomEnvelope_coord[p]);
            }
        } else {
            /** Neutron scattering NSL does NOT depend on q */
            for (i = 0; i < qhomenr; i++) {
                /**
                 * To keep this function generic for Xray and Neutron (for now), write the identical NSL into an array.
                 * Because the SINCOS function below is by far the most expensive anyway, this will hardly make any difference.
                 */
                p_aff[i] = nsl_table[atomEnvelope_type[p]];
                p_qdotx[i] = iprod(q[i], atomEnvelope_coord[p]);
            }
        }

        /** Cast the real* to gmx::SimdReal* arrays */
        gmx::SimdReal* m_aff = (gmx::SimdReal*)p_aff;
        gmx::SimdReal* m_qdotx = (gmx::SimdReal*)p_qdotx;
        gmx::SimdReal* mRe = (gmx::SimdReal*)re;
        gmx::SimdReal* mIm = (gmx::SimdReal*)im;

        /**
         * the SSE or AVX loop
         * SSE or AVX128: float: 4 sincos per call. double: 2 sincos per call
         * AVX256:        float: 8 sincos per call. double: 4 sincos per call
         */
        gmx::SimdReal sinm, cosm;

        for (int k = 0; k < qhomenr; k += nReal) {
            gmx::sincos(*m_qdotx, &sinm, &cosm);
            *mRe = *m_aff * cosm;
            *mIm = *m_aff * sinm;

            m_qdotx++;
            m_aff++;
            mRe++;
            mIm++;
        }

        for (i = 0; i < qhomenr; i++) {
            /* Requires a real to double conversion */
            scatt_amplitude[i].re += re[i];
            scatt_amplitude[i].im += im[i];
        }

        if (grad_amplitude_bar and p < nprot) {
            int k0 = p * qhomenr;

            /** Store: [ i . f_k(q) . exp(iq*x) ]* = (i.SF)*   */
            for (i = 0; i < qhomenr; i++) {
                /** Use [(a+i.b).i]* = -b-i*a */
                int k = k0 + i;

                grad_amplitude_bar[k].re = -im[i];
                grad_amplitude_bar[k].im = -re[i];
            }
        }
    }

    sfree_aligned(p_aff);
    sfree_aligned(p_qdotx);
    sfree_aligned(re);
    sfree_aligned(im);

    #ifdef PRINT_A_dkI
        FILE* fp = fopen("./uncorrected_scattering_amplitude_CPU.txt", "w");

        fprintf(fp, "Atom-number HOST start is: %d\n", isize);
        fprintf(fp, "qhomenr HOST after loop is: %d\n", qhomenr);

        for (int i = 0; i < qhomenr; i++) {
            fprintf(fp, "Real-part of scattering amplitude [ %d ] :  %f \n", i, scatt_amplitude[i].re);
            fprintf(fp, "Im  -part of scattering amplitude [ %d ] :  %f \n", i, scatt_amplitude[i].im);
            fprintf(fp, " \n");
        }

        fclose(fp);
    #endif

    /** Return elapsed time in milliseconds */
    struct timespec t_end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t_end);

    long int delta_us = (t_end.tv_sec - t_start.tv_sec) * 1000000 + (t_end.tv_nsec - t_start.tv_nsec) / 1000;
    *timing_ms = delta_us / 1000.;

    swaxs_debug("End of compute_scattering_amplitude()\n");
}


#ifdef __cplusplus
extern "C" {
#endif

/** Update cumulative averages: cum_sum = fac1 * sum + fac2 * cum_sum */

#if 0
static real r_accum_avg(real sum, real cum_sum, real fac1, real fac2) {
    real tmp = fac1 * sum + fac2 * cum_sum;

    return tmp;
}
#endif

static double d_accum_avg(double sum, double cum_sum, double fac1, double fac2) {
    double tmp = fac1 * sum + fac2 * cum_sum;

    return tmp;
}

#if 0
static t_complex c_accum_avg(t_complex sum, t_complex cum_sum, real fac1, real fac2) {
    t_complex tmp = cadd(rcmul(fac1, sum), rcmul(fac2, cum_sum));

    return tmp;
}
#endif

static t_complex_d cd_accum_avg(t_complex_d sum, t_complex_d cum_sum, double fac1, double fac2) {
    t_complex_d tmp = cadd_d(rcmul_d(fac1, sum), rcmul_d(fac2, cum_sum));

    return tmp;
}

#if 0
static void cvec_accum_avg(cvec* sum, cvec cum_vec, real fac1, real fac2) {
    cvec tmp;

    for (int d = 0; d < DIM; ++d) {
        tmp[d] = c_accum_avg(*sum[d], cum_vec[d], fac1, fac2);
        *sum[d] = tmp[d];
    }
}
#endif

#ifdef __cplusplus
}
#endif

/**
 * Update the average values of A(q) and B(q) or function of these, or calculate the average values.
 * Initialized to zero in alloc_waxs_datablock()
 */
static void update_AB_averages(t_waxsrec* wr) {
    #ifdef RERUN_CalcForces
        gmx_bool bCalcForces = TRUE;
    #endif

    swaxs_debug("Entering update_AB_averages()");

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        int qhomenr = wt->qvecs->qhomenr;
        int qstart = wt->qvecs->qstart;
        int nabs = wt->qvecs->nabs;

        /**
         * Get fac1 and fac2 for running weighted average
         *   <X[i+1]> = fac1 * <X[i]> + fac2 * x[i + 1] =
         *            =  (N[i + 1] - 1) / N[i + 1]) * <X[i]> + 1 / N[i + 1] * x[i + 1]
         *  This is valid for either exponential or equally-weighted averaging.
         */
        wd->normA = 1.0 + wr->inputParams->scale * wd->normA;

        double fac1A = 1.0 * (wd->normA - 1) / wd->normA;
        double fac2A = 1.0 / wd->normA;

        /** For the B system (pure solvent), always use a non-weighted average (scale = 1) */
        double fac1B = 0.;
        double fac2B = 0.;

        if (wr->bDoingSolvent) {
            wd->normB = 1.0 + wd->normB;

            fac1B = 1.0 * (wd->normB - 1) / wd->normB;
            fac2B = 1.0 / wd->normB;
        }

        /**
         * Note on precision: A[i], B[i] and dkAbar[k] are real.
         *                    All averages are double.
         */

        /** For each local q vector */
        for (int i = 0; i < qhomenr; i++) {
            t_complex_d tmp_cd;

            tmp_cd.re = wd->A[i].re;
            tmp_cd.im = wd->A[i].im;

            wd->avA     [i] = cd_accum_avg(wd->avA    [i], tmp_cd,                      fac1A, fac2A);
            double dtmp = cabs2_d(wd->A[i]);
            wd->avAsq   [i] = d_accum_avg(wd->avAsq   [i], dtmp,                        fac1A, fac2A);
            wd->avA4    [i] = d_accum_avg(wd->avA4    [i], dtmp * dtmp,                 fac1A, fac2A);
            wd->av_ReA_2[i] = d_accum_avg(wd->av_ReA_2[i], gmx::square(wd->A[i].re), fac1A, fac2A);
            wd->av_ImA_2[i] = d_accum_avg(wd->av_ImA_2[i], gmx::square(wd->A[i].im), fac1A, fac2A);

            if (wr->bDoingSolvent) {
                tmp_cd.re = wd->B[i].re;
                tmp_cd.im = wd->B[i].im;

                /**
                 * We could pick the avB from the GPU if available.
                 * But due to the low calculation but expensive data transfer cost, avB is recalculated here.
                 */

                wd->avB     [i] = cd_accum_avg(wd->avB    [i], tmp_cd,                      fac1B, fac2B);
                double dtmp = cabs2_d(wd->B[i]);
                wd->avBsq   [i] = d_accum_avg(wd->avBsq   [i], dtmp,                        fac1B, fac2B);
                wd->avB4    [i] = d_accum_avg(wd->avB4    [i], dtmp * dtmp,                 fac1B, fac2B);
                wd->av_ReB_2[i] = d_accum_avg(wd->av_ReB_2[i], gmx::square(wd->B[i].re), fac1B, fac2B);
                wd->av_ImB_2[i] = d_accum_avg(wd->av_ImB_2[i], gmx::square(wd->B[i].im), fac1B, fac2B);
            }
        }

        /** If we use the GPU, the orientiational average for grad D(q) is already done on the GPU */
        if (wr->inputParams->bCalcForces and wr->inputParams->bUseGPU == FALSE) {
            int nprot = wr->nindA_prot;

            for (int p = 0; p < nprot; p++) {
                for (int n = 0; n < nabs; n++) {
                    /** 1st summand */
                    dvec tmp_dkAbar_A_avB;
                    clear_dvec(tmp_dkAbar_A_avB);

                    /** loop over q-vec which belong to this absolute value |q_i| */
                    for (int j = wt->qvecs->ind[n]; j < wt->qvecs->ind[n + 1]; j++) {
                        if (j >= qstart and j < (qstart + qhomenr)) {
                            int jj = j - qstart;  /** index of A and avB */

                            /** index of derivatives atoms */
                            int k = p * qhomenr + jj ;

                            /**
                             * Do product of A.grad[k]*.(A(q) - B_av)  (before time average)
                             *
                             * Calculate the difference A[k] - B_av
                             * Note that after a certain number of steps <B> is not changing anymore, therefore, it can be taken as a constant
                             */
                            t_complex_d A_avB_tmp;

                            A_avB_tmp.re = wd->A[jj].re - wd->avB[jj].re;
                            A_avB_tmp.im = wd->A[jj].im - wd->avB[jj].im;

                            if (wr->inputParams->bScaleI0) {
                                A_avB_tmp.re += wd->avAcorr[jj].re - wd->avB[jj].re;
                                A_avB_tmp.im += wd->avAcorr[jj].im - wd->avB[jj].im;
                            }

                            /** grad[k](A*(q)) . (A(q) - B_av)  (before time average). Only the real part remains: */
                            double tmpc1 = +wd->dkAbar[k].re * A_avB_tmp.re
                                           -wd->dkAbar[k].im * A_avB_tmp.im;

                            for (int d = 0; d < DIM; d++) {
                                /** Multiply with \vec{q} */
                                tmp_dkAbar_A_avB[d] += tmpc1 * wt->qvecs->q[j][d];
                            }
                        }
                    }

                    int l = p * nabs + n;

                    for (int d = 0; d < DIM; d++) {
                        /** Temporal running average */
                        wd->Orientational_Av[l][d] = d_accum_avg(wd->Orientational_Av[l][d], tmp_dkAbar_A_avB[d], fac1A, fac2A);
                    }
                }
            }
        }
    }

    swaxs_debug("Leaving update_AB_averages()");
}


/** Computes I(q) and grad[k] I(q) (in parallel) */
static void calculate_I_dkI(t_waxsrec* wr, const t_commrec* cr, const gmx_multisim_t* ms) {
    swaxs_debug("Entering calculate_I_dkI()");

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        t_spherical_map* qvecs = wt->qvecs;
        waxs_datablock* wd = wt->wd;

        double Bprefact;

        if (wr->inputParams->tau < 0.) {
            /** Prefactor in front of <B^2>-<B>^2 to get an unbiased estimator. Park et al., eq. 31 */
            int nFramesSolvent = (wr->waxsStep < wr->inputParams->nfrsolvent) ? (wr->waxsStep + 1) : wr->inputParams->nfrsolvent;

            Bprefact = (nFramesSolvent > 1) ? (1. * nFramesSolvent + 1.) / (1. * nFramesSolvent - 1.) : 1;
        } else {
            Bprefact = 1.;
        }

        int nIvalues = wd->nIvalues;

        /** Clear I, grad[k] I */
        for (int i = 0; i < nIvalues; i++) {
            wd->I[i] = 0.;
            wd->IA[i] = 0.;
            wd->varIA[i] = 0.;
            wd->avAqabs_re[i] = 0.;
            wd->avAqabs_im[i] = 0.;

            if (!wr->inputParams->bVacuum) {
                wd->IB[i] = 0.;
                wd->Ibulk[i] = 0.;
                wd->varIB[i] = 0.;
                wd->varIbulk[i] = 0.;
                wd->avBqabs_re[i] = 0.;
                wd->avBqabs_im[i] = 0.;
                wd->I_avAmB2[i] = 0.;
                wd->I_varA[i] = 0.;
                wd->I_varB[i] = 0.;
                wd->varI_avAmB2[i] = 0.;
                wd->varI_varA[i] = 0.;
                wd->varI_varB[i] = 0.;

                if (wr->inputParams->bScaleI0) {
                    wd->I_scaleI0[i] = 0.;
                }
            }

            if (wd->I_errSolvDens) {
                wd->I_errSolvDens[i] = 0;
            }
        }

        int nprot = wr->nindA_prot;

        if (wr->inputParams->bCalcForces and wr->inputParams->bUseGPU == FALSE) {  /** do not clear, if dkI is calculated on GPU! */
            for (int i = 0; i < nIvalues * nprot; i++) {
                clear_dvec(wd->dkI[i]);
            }
        }

        /** Compute D(q) for home-qvalues */
        swaxs_debug("Compute D(q) for home-qvalues");

        int qhomenr = qvecs->qhomenr;

        for (int i = 0; i < qhomenr; i++) {
            if (!wr->inputParams->bVacuum) {
                /** Equivalent to equation below */
                t_complex_d this_avA;

                if (wr->inputParams->bScaleI0) {
                    this_avA = cadd_d(wd->avA[i], wd->avAcorr[i]);
                } else {
                    this_avA = wd->avA[i];
                }

                wd->D[i] = cabs2_d(csub_d(this_avA, wd->avB[i])) + (wd->avAsq[i] - cabs2_d(wd->avA[i]))
                        - Bprefact * (wd->avBsq[i] - cabs2_d(wd->avB[i]));
            } else {
                wd->D[i] = wd->avAsq[i];
            }
        }

        if (wr->inputParams->stepCalcNindep > 0
                and ((wr->waxsStep + 1) % wr->inputParams->stepCalcNindep == 0 or wr->waxsStep == 0 or wr->waxsStep == 5 or wr->waxsStep == 10 or wr->waxsStep == 20)) {
            waxsEstimateNumberIndepPoints(wr, cr, FALSE, FALSE);
        }

        int* ind = qvecs->ind;
        int nabs = qvecs->nabs;

        const int qstart = qvecs->qstart;
        // const int qend = qstart + qhomenr;

        /**
         * - spherical averging of D(\vec{q})
         * - Here we have one intensity per absolute q-value: I[0..nabs)
         */

        /** Loop over absolute values of q, |q| */
        for (int i = 0; i < nabs; i++) {
            /** Loop over q-vectors with this absolute value of q */
            for (int j = ind[i]; j < ind[i + 1]; j++) {
                /** is this q-vector on this node? */
                if (j >= qstart and j < (qstart + qhomenr)) {
                    int jj = j - qstart;
                    wd->I[i] += wd->D[jj];

                    /**
                     * Also get intensity from only atoms A or B and the third term, that is
                     *   <|A|^2>, <|B|^2>, and -2Re[ <B>.<A-B>*]
                     */
                    int stepA = wr->waxsStep + 1;
                    wd->IA[i] += wd->avAsq[jj];
                    wd->varIA[i] += (wd->avA4[jj] - gmx::square(wd->avAsq[jj])) / stepA;

                    /** Also get < A(|q|)> := int_dOmega[q] < A(q) > */
                    wd->avAqabs_re[i] += wd->avA[jj].re;
                    wd->avAqabs_im[i] += wd->avA[jj].im;

                    if (wd->I_errSolvDens) {
                        /** Uncertainty in I(q) due to uncertainty in solvent density */
                        wd->I_errSolvDens[i] += wd->DerrSolvDens[jj];
                    }

                    if (!wr->inputParams->bVacuum) {
                        int stepB = (wr->waxsStep + 1 < wr->inputParams->nfrsolvent) ? wr->waxsStep + 1 : wr->inputParams->nfrsolvent;

                        wd->IB[i] += wd->avBsq[jj];
                        wd->varIB[i] += (wd->avB4[jj] - gmx::square(wd->avBsq[jj])) / stepB;

                        t_complex_d tmpc = csub_d(wd->avA[jj], wd->avB[jj]);
                        tmpc = conjugate_d(tmpc);
                        tmpc = cmul_d(wd->avB[jj], tmpc);

                        wd->Ibulk[i] -= 2 * tmpc.re;

                        /** Also get < B(|q|)> := int_dOmega[q] < B(q) > */
                        wd->avBqabs_re[i] += wd->avB[jj].re;
                        wd->avBqabs_im[i] += wd->avB[jj].im;

                        wd->varIbulk[i] +=
                                  gmx::square( 2 * wd->avB[jj].re                     ) * (wd->av_ReA_2[jj] - gmx::square(wd->avA[jj].re)) / stepA
                                + gmx::square( 2 * wd->avB[jj].im                     ) * (wd->av_ImA_2[jj] - gmx::square(wd->avA[jj].im)) / stepA
                                + gmx::square(-2 * wd->avA[jj].re + 4 * wd->avB[jj].re) * (wd->av_ReB_2[jj] - gmx::square(wd->avB[jj].re)) / stepB
                                + gmx::square(-2 * wd->avA[jj].im + 4 * wd->avB[jj].im) * (wd->av_ImB_2[jj] - gmx::square(wd->avB[jj].im)) / stepB;

                        /**
                         * Finally, we comute the contributions using the alternative fomula by Park et al., that is
                         *   D(q) = |<A-B>|^2 + var(A) - var(B)
                         * These there terms are stored in variables AmB2, varA, varB
                         */
                        wd->I_avAmB2[i] += cabs2_d(csub_d(wd->avA[jj], wd->avB[jj]));

                        wd->I_varA[i] += wd->avAsq[jj] - cabs2_d(wd->avA[jj]);
                        wd->I_varB[i] += wd->avBsq[jj] - cabs2_d(wd->avB[jj]);

                        /**
                         * And the respective variances:
                         * Note: |<A-B>|^2 = <Re A>^2 + <Im A>^2 + <Re B>^2 - 2*<Re B>.<Re A> (using <Im B> = 0)
                         */
                        wd->varI_avAmB2[i] +=
                                  gmx::square(2 * wd->avA[jj].re - 2 * wd->avB[jj].re ) * (wd->av_ReA_2[jj] - gmx::square(wd->avA[jj].re)) / stepA
                                + gmx::square(2 * wd->avA[jj].im                      ) * (wd->av_ImA_2[jj] - gmx::square(wd->avA[jj].im)) / stepA
                                + gmx::square(2 * wd->avB[jj].re - 2 * wd->avA[jj].re ) * (wd->av_ReB_2[jj] - gmx::square(wd->avB[jj].re)) / stepB;

                        /**
                         * The standard error of the variance estimate is sigma^2 * sqrt(2 / k),
                         * where k is the number of data points used to compute the variance.
                         * Will devide by # of waxs steps (k) when writing the contrib file.
                         */
                        wd->varI_varA[i] += 2 * gmx::square(wd->av_ReA_2[jj] - gmx::square(wd->avA[jj].re)) / stepA
                                + 2 * gmx::square(wd->av_ImA_2[jj] - gmx::square(wd->avA[jj].im)) / stepA;
                        wd->varI_varB[i] += 2 * gmx::square(wd->av_ReB_2[jj] - gmx::square(wd->avB[jj].re)) / stepB;

                        if (wr->inputParams->bScaleI0) {
                            /**
                             * Additional intensity due to scaling of I(q=0)
                             *   Iscale_I0 := |<A+dA-B>|^2 - |<A-B>|^2
                             */
                            t_complex_d tmpc = csub_d(wd->avA[jj], wd->avB[jj]);
                            tmpc = cmul_d(conjugate_d(tmpc), wd->avAcorr[jj]);
                            wd->I_scaleI0[i] = 2 * tmpc.re + cabs2_d(wd->avAcorr[jj]);
                        }
                    }
                }
            }
        }

        /** Sum over the nodes and normalize */
        if (PAR(cr)) {
            gmx_sumd(nIvalues, wd->I,          cr);
            gmx_sumd(nIvalues, wd->IA,         cr);
            gmx_sumd(nIvalues, wd->varIA,      cr);
            gmx_sumd(nIvalues, wd->avAqabs_re, cr);
            gmx_sumd(nIvalues, wd->avAqabs_im, cr);

            if (!wr->inputParams->bVacuum) {
                gmx_sumd(nIvalues, wd->IB,          cr);
                gmx_sumd(nIvalues, wd->Ibulk,       cr);
                gmx_sumd(nIvalues, wd->varIB,       cr);
                gmx_sumd(nIvalues, wd->varIbulk,    cr);
                gmx_sumd(nIvalues, wd->avBqabs_re,  cr);
                gmx_sumd(nIvalues, wd->avBqabs_im,  cr);
                gmx_sumd(nIvalues, wd->I_avAmB2,    cr);
                gmx_sumd(nIvalues, wd->I_varA,      cr);
                gmx_sumd(nIvalues, wd->I_varB,      cr);
                gmx_sumd(nIvalues, wd->varI_avAmB2, cr);
                gmx_sumd(nIvalues, wd->varI_varA,   cr);
                gmx_sumd(nIvalues, wd->varI_varB,   cr);
            }

            if (wd->I_errSolvDens) {
                gmx_sumd(nIvalues, wd->I_errSolvDens, cr);
            }
        }

        /** In case with averaged over all q orientiations: divide by nr of q vectors per |q| */
        for (int i = 0; i < nabs; i++) {
            int N = ind[i + 1] - ind[i];

            wd->I         [i] /= N;
            wd->IA        [i] /= N;
            wd->varIA     [i] /= N;
            wd->avAqabs_re[i] /= N;
            wd->avAqabs_im[i] /= N;

            /** Get final variance of I based on error propagation */
            if (wr->inputParams->bVacuum) {
                wd->varI[i] = fabs(wd->varIA[i]);
            }

            if (!wr->inputParams->bVacuum) {
                wd->IB         [i] /= N;
                wd->Ibulk      [i] /= N;
                wd->varIB      [i] /= N;
                wd->varIbulk   [i] /= N;
                // wd->varI[i] += wd->varIB[i] + wd->varIbulk[i];
                wd->avBqabs_re [i] /= N;
                wd->avBqabs_im [i] /= N;
                wd->I_avAmB2   [i] /= N;
                wd->I_varA     [i] /= N;
                wd->I_varB     [i] /= N;
                wd->varI_avAmB2[i] /= N;
                wd->varI_varA  [i] /= N;
                wd->varI_varB  [i] /= N;

                wd->varI[i] = fabs(wd->varI_avAmB2[i] + wd->varI_varA[i] + wd->varI_varB[i]);
            }

            if (wr->waxsStep <= 1) {
                /**
                 * In the first step, we have no reasonable estimate for varI - just use 50% of I(q) for stddev
                 * Otherwise, we get a numercal error in the Bayesian WAXS-MD .
                 */
                if (MASTER(cr) and i == 0) {
                    printf("WAXS step %d: Using 50%% of I(q) as stddev of I(q)\n", wr->waxsStep);
                }

                wd->varI[i] = gmx::square(0.5 * wd->I[i]);
            }

            if (wd->I_errSolvDens) {
                wd->I_errSolvDens[i] /= N;
            }

            if (wr->bHaveNindep) {
                swaxs_debug("Divide by # of independent q-vectors if we have it already");

                /** Divide by # of independent q-vectors if we have it already */
                wd->varI[i] /= wd->Nindep[i];
            }
        }

        if (wr->inputParams->bCorrectBuffer) {
            if (t > 0) {
                gmx_fatal(FARGS, "In calculate_I_dkI(): correcting for oversubtracted buffer only "
                        "implemented for *one* scattering group, which must be X-ray");
            }

            /** Correcting for oversubtracted buffer, eq. 2 in Koefinger & Hummer, Phys Rev E 87 052712 (2013) */
            for (int i = 0; i < nabs; i++) {
                wd->I[i] += wr->soluteVolAv * wt->Ipuresolv[i];
            }
        }

        if (wr->inputParams->isSwaxsEnsemble()) {
            if (wr->inputParams->ensembleType == swaxs::EnsembleType::BayesianOneRefined) {
                swaxs::bayesian_ensemble_average(wr, wt, cr, wd, nabs);
            } else if (wr->inputParams->ensembleType == swaxs::EnsembleType::MaxEnt) {
                swaxs::maxent_ensemble_average(wr, wt, cr, ms);
            }
        }

        /** Compute I(q) and gradients of I(q)  atomic coordinates if not already done on GPU */
        swaxs_debug("Compute I(q) and gradients of I(q) atomic coordinates if not already done on GPU");

        if (wr->inputParams->bCalcForces) {
            swaxs_debug("wr->bCalcForces is true");

            if (wr->inputParams->bUseGPU) {
                swaxs_debug("wr->bCalcForces with GPU");

                #if GMX_GPU_CUDA
                    double GPU_time;

                    calculate_dkI_GPU(t, wd->dkI, wr->atomEnvelope_coord_A, wt->atomEnvelope_scatType_A, qstart, qhomenr, nabs, nprot, &GPU_time, cr);

                    wr->timing->add(wr->waxsStep, swaxs::Timing::Stage::dkI, GPU_time / 1000, *cr);
                #endif
            } else {
                swaxs_debug("wr->bCalcForces without gpu");

                wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::dkI, *cr);

                for (int i = 0; i < nabs; i++) {
                    int J = (ind[i + 1] - ind[i]);

                    /** Calculate grad[k] D(vec{q}) and sum immediately into grad[k] I(q) */
                    for (int p = 0; p < nprot; p++) {
                        int l = p * nabs + i;  /** index of dkI */

                        /** Now calculate dkI, the J denotes the number of q-vectors for this absolute value of q */
                        for (int d = 0; d < DIM; d++) {
                            wd->dkI[l][d] = 2. * wd->Orientational_Av[l][d] / J;
                        }
                    }
                }

                swaxs_debug("dKi done");

                /** Globally sum rvec array and divide by nr of qvecs on q-sphere */
                if (PAR(cr)) {
                    gmx_sumd(nabs * nprot * DIM, wd->dkI[0], cr);
                    swaxs_debug("after gmx_sumd");
                }

                wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::dkI, *cr);
            }

            swaxs_debug("end if bCalcForces");
        }

        #ifdef PRINT_A_dkI
            if (wr->inputParams->bCalcForces and wr->inputParams->debugLvl > 1) {
                char fn[STRLEN];
                sprintf(fn, "fn_dkI_%s_%s.txt", wr->inputParams->bUseGPU ? "GPU" : "CPU", wt->type == escatterXRAY ? "XRAY" : "Neutron");

                FILE* fdkI;
                fdkI = fopen(fn, "w");
                fprintf(fdkI, "nprot is: %d\n", nprot);
                fprintf(fdkI, "nabs  is: %d\n", nabs);

                for (int i = 0; i < nabs; i++) {
                    fprintf(fdkI, "q_abs = %f \n", wt->qvecs->abs[i]);

                    for (int p = 0; p < nprot; p++) {
                        int l = i * nprot + p; /* index of dkI */

                        for (int d = 0; d < DIM; d++) {
                            fprintf(fdkI, "dkI [ %d ][%d] =  %f \n", l, d, wd->dkI[l][d]);
                        }

                        fprintf(fdkI, "\n");
                    }
                }

                fclose(fdkI);
                printf("Debugging level %d: Wrote dkI to %s\n", wr->inputParams->debugLvl, fn);
            }

            /** Test code for comparing CPU- vs. GPU-computed scattering amplitudes */
            if (qstart > 0 and wr->inputParams->debugLvl > 1) {
                char fn_p[STRLEN], fn_b[STRLEN];
                sprintf(fn_p, "scattering_amplitude_%s_%s.txt", wr->inputParams->bUseGPU ? "GPU" : "CPU",
                        wt->type == escatterXRAY ? "XRAY" : "Neutron");
                sprintf(fn_b, "water_scattering_amplitude_%s_%s.txt", wr->inputParams->bUseGPU ? "GPU" : "CPU",
                        wt->type == escatterXRAY ? "XRAY" : "Neutron");
                FILE* fp = fopen(fn_p, "w");
                FILE* fb = fopen(fn_b, "w");

                fprintf(fp, "Atom-number HOST start is: %d\n", wr->isizeA);
                fprintf(fp, "qhomenr HOST after loop is: %d\n", qhomenr);
                fprintf(fb, "Atom-number HOST start is: %d\n", wr->isizeB);
                fprintf(fb, "qhomenr HOST after loop is %d \n", qhomenr);

                for (int i = 0; i < qhomenr; i++) {
                    fprintf(fp, "Real-part of scattering amplitude [ %d ] :  %f \n", i, wd->A[i].re);
                    fprintf(fp, "Im  -part of scattering amplitude [ %d ] :  %f \n", i, wd->A[i].im);
                    fprintf(fp, " \n");

                    fprintf(fb, "Real-part of scattering amplitude [ %d ] :  %f \n", i, wd->B[i].re);
                    fprintf(fb, "Im  -part of scattering amplitude [ %d ] :  %f \n", i, wd->B[i].im);
                    fprintf(fb, " \n");
                }

                fclose(fp);
                fclose(fb);
                printf("Debugging Level %d: Wrote some scattering amplitudes to %s and %s\n", wr->inputParams->debugLvl, fn_p, fn_b);
            }
        #endif
    }

    swaxs_debug("Leaving calculate_I_dkI()");
}

/** Return difference between calculated and experimental intensity on log or linear scale */
static inline double waxs_intensity_Idiff(t_waxsrec* wr, t_waxsrecType* wt, double* Icalc, int iq,
        gmx_bool bAllowNegativeIcalc, gmx_bool bVerbose) {
    /** Works with or without fitting. Without fitting, f=1 and c=0, with fitting only the scale, c=0 */
    double IexpFitted = wt->f_ml * wt->Iexp[iq] + wt->c_ml;
    double Idiff = 0;

    switch (wr->inputParams->potentialType) {
        case ewaxsPotentialLOG:
            if (IexpFitted <= 0 and bAllowNegativeIcalc == FALSE) {
                gmx_fatal(FARGS, "After fitting the experimental curve I(fitted) = f*I(exp)+c to the calculated curve,\n"
                        "we found negative fitted I(q) (%g) at q = %g. This will not work with a WAXS potential on the "
                        "log scale.\n"
                        "Use either a linear scale, try a lower q-range, or fit only the scale f but not the offset c. "
                        "(f = %g  c = %g)", IexpFitted, wt->qvecs->abs[iq], wt->f_ml, wt->c_ml);
            }

            if (Icalc[iq] <= 0 and bAllowNegativeIcalc == FALSE) {
                gmx_fatal(FARGS, "Found a negative computed intensity at q = %g (iq = %d): Icalc = %g.\n"
                        "This will not work when coupling on a log scale. Choose a linear coupling scale instead.\n",
                        wt->qvecs->abs[iq], iq, Icalc[iq]);
            }

            if ((Icalc[iq] <= 0 or IexpFitted <= 0) and bAllowNegativeIcalc) {
                if (bVerbose) {
                    printf("\nNOTE!!!\nFound a negative Icalc = %g or negative fitted Iexp = %g at q = %g (iq = %d), "
                           "while coupling on the log scale.\n"
                           "This is ok at the beginning of the simulation (t < waxs_tau), when Icalc might not yet be "
                           "converged.\n\n", Icalc[iq], IexpFitted, wt->qvecs->abs[iq], iq);
                }

                Idiff = 0;
            } else {
                Idiff = log(Icalc[iq] / IexpFitted);
            }

            break;
        case ewaxsPotentialLINEAR: Idiff = Icalc[iq] - IexpFitted; break;
        default:
            gmx_fatal(FARGS, "Unknown type of WAXS potential (type %d, should be < %d)\n", wr->inputParams->potentialType, ewaxsPotentialNR);
    }

    if (std::isnan(Idiff) or std::isinf(Idiff)) {
        gmx_fatal(FARGS, "Error in waxs_intensity_Idiff(), with potential type = %s :\n"
                "Found for iq = %d: I = %g -- Idiff = %g (NaN/inf) -- IexpFitted = %g -- f_ml = %g -- c_ml = %g\n",
                EWAXSPOTENTIAL(wr->inputParams->potentialType), iq, wt->wd->I[iq], Idiff, IexpFitted, wt->f_ml, wt->c_ml);
    }

    return Idiff;
}

/**
 * Return sigma that goes into the WAXS potential, depending on scale (log or linear) and weights type
 * This function is only used for non-bayesian MD.
 */
double waxs_intensity_sigma(t_waxsrec* wr, t_waxsrecType* wt, double* varIcalc, int iq) {
    double sigma = 0;

    /** Choose the variance that goes into Ewaxs */
    switch (wr->inputParams->weightsType) {
        case ewaxsWeightsUNIFORM: sigma = 1; break;
        case ewaxsWeightsEXPERIMENT: sigma = wt->f_ml * wt->Iexp_sigma[iq]; break;
        case ewaxsWeightsEXP_plus_CALC: sigma = sqrt(gmx::square(wt->f_ml * wt->Iexp_sigma[iq]) + varIcalc[iq]); break;
        case ewaxsWeightsEXP_plus_SOLVDENS:
            sigma = sqrt(gmx::square(wt->f_ml * wt->Iexp_sigma[iq]) + gmx::square(wr->epsilon_buff * wt->wd->I_errSolvDens[iq]));
            break;
        case ewaxsWeightsEXP_plus_CALC_plus_SOLVDENS:
            sigma = sqrt(gmx::square(wt->f_ml * wt->Iexp_sigma[iq]) + varIcalc[iq] + gmx::square(wr->epsilon_buff * wt->wd->I_errSolvDens[iq]));
            break;
        default:
            gmx_fatal(FARGS, "Unknown type of WAXS weights type (type %d, should be < %d)\n", wr->inputParams->weightsType, ewaxsWeightsNR);
    }

    if (wr->inputParams->potentialType == ewaxsPotentialLOG and wr->inputParams->weightsType != ewaxsWeightsUNIFORM) {
        /* With a potential on a log scale, the uncertainty is delta[log I] = (delta I)/I */
        sigma /= wt->Iexp[iq];
    }

    return sigma;
}

static void clear_vLast_fLast(t_waxsrec* wr) {
    /** Clear waxs potential */
    wr->vLast = 0;

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        wr->wt[t].vLast = 0;
    }

    int nprot = wr->nindA_prot;

    /** Clear waxs forces */
    if (wr->fLast) {
        for (int p = 0; p < nprot; p++) {
            clear_rvec(wr->fLast[p]);

            for (int t = 0; t < wr->inputParams->nTypes; t++) {
                clear_rvec(wr->wt[t].fLast[p]);
            }
        }
    }
}

/** Compute the SAXS/SANS-derived potentials and forces */
static void waxs_md_pot_forces(const t_commrec* cr, t_waxsrec* wr, real simtime, matrix* Rinv) {
    swaxs_debug("Entering waxs_md_pot_forces()");

    if (wr->inputParams->isSwaxsEnsemble() and wr->inputParams->nTypes > 1) {
        gmx_fatal(FARGS, "Ensemble refinement so far only with one scattering type (found wr->inputParams->nTypes = %d)\n",
                wr->inputParams->nTypes);
    }

    if (!wr->inputParams->bCalcPot) {
        gmx_incons("wr->inputParams->bCalcPot false in waxs_md_pot_forces()");
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        int nprot = wr->nindA_prot;

        /**
         * Depending on whether we do ensemble refinement, the calculated intensity
         * is the ensemble-averaged I(q) or the simply I(q) computed from the MD.
         * So let's define two pointer on the respective arrays.
         */

        double* Icalc;
        double* IcalcVar;

        if (wr->inputParams->isSwaxsEnsemble()) {
            if (wr->inputParams->ensembleType == swaxs::EnsembleType::MaxEnt) {
                Icalc = wt->wd->maxEntEnsemble.ensemble_I_Aver;
                IcalcVar = wt->wd->maxEntEnsemble.ensemble_varI_Aver;
            } else {
                Icalc = wt->bayesianEnsemble.sum_I;
                IcalcVar = wt->bayesianEnsemble.sum_Ivar;
            }
        } else {
            Icalc = wd->I;
            IcalcVar = wd->varI;
        }

        /** For cumulative (non-weighted) average of potentials vAver */
        double fac1 = 1.0 * (wr->waxsStep) / (wr->waxsStep + 1);
        double fac2 = 1.0 / (wr->waxsStep + 1);

        /** Smoothly switching on the force between tau < simtime < 2tau. This
            makes sure that we don't apply force while the I(q) are not yet converged */

        double this_simtime = simtime;

        real fForceSwitch;

        if (this_simtime > 2.0 * wr->inputParams->tau or !wr->inputParams->bSwitchOnForce) {
            fForceSwitch = 1.;
        } else if (this_simtime < wr->inputParams->tau) {
            fForceSwitch = 0.;
        } else {
            fForceSwitch = 0.5 * (1. - cos(M_PI * (this_simtime - wr->inputParams->tau) / (wr->inputParams->tau)));
        }

        if (MASTER(cr) and wr->swaxsOutput->fpLog and fForceSwitch < 1.0) {
            fprintf(wr->swaxsOutput->fpLog, "While switching on potential and force: using factor of %g\n", fForceSwitch);
        }

        /**
         * Get relative weights of current I(q) and target I(q), which enter the I(q) to which we couple.
         * Here we use the total simulation time, not the simulation time from the last restart.
         */
        real fact_target_switch;

        if (simtime > wr->inputParams->t_target or wr->inputParams->t_target <= 0.) {
            /** Only use input pattern */
            fact_target_switch = 1.;
        } else {
            /**
             * Linearly switch from current to target pattern.
             * Note:  I_couple = (1 - f)I_sim + f * I_target.
             *        -> V = k * (I_sim - I_couple)^2 = k * f^2 * (I_sim - I_target)^2
             *        -> So we only get an additional factor of f^2
             */
            fact_target_switch = simtime / wr->inputParams->t_target;
        }

        if (MASTER(cr) and wr->swaxsOutput->fpLog and fact_target_switch < 1.0) {
            fprintf(wr->swaxsOutput->fpLog, "Relative weight of target pattern vs. current pattern %g\n", fact_target_switch);
        }

        /**
         * Get maximum likelihood estimate for f (overall scale) and c (offset)
         *
         * At the beginning, we don't have any estimate for f yet, so we cannot compute the scaling factor entering
         * the experimental errors. Therefore, get a first good guess for f using uniform weights:
         */
        if (wt->f_ml == 0) {
            swaxs::maximum_likelihood_est_fc_generic(wr, Icalc, wt->Iexp, nullptr, wt->nq, &wt->f_ml, &wt->c_ml, nullptr);
        }

        int nabs = wt->nq;

        double* tau = nullptr;
        snew(tau, nabs);

        /** Now we have a reasonable estimate for f, and we can compute the statistical weights (or "precisions")
            tau_i = 1 / sigma_i^2 */
        for (int i = 0; i < nabs; i++) {
            tau[i] = 1. / gmx::square(waxs_intensity_sigma(wr, wt, IcalcVar, i));
        }

        /** Get fitting parameters f/c, or purely f, or no fitting (dependig on waxs-Iexp-fit) */
        double sumSquaredResiduals = -1;
        swaxs::maximum_likelihood_est_fc_generic(wr, Icalc, wt->Iexp, tau, wt->nq, &wt->f_ml, &wt->c_ml, &sumSquaredResiduals);

        double nShannonFactorUsed = (wr->inputParams->bDoNotCorrectForceByNindep ? 1. : wt->nShannon);

        /**
         * Overall factor for pot and forces
         * Update in Dec. 2017. Multiply with Nindep, such that we follow a Bayesian logic if the force constant is fc == 1
         */
        real fact0 = fForceSwitch * wt->fc * wr->inputParams->kT * gmx::square(fact_target_switch) * nShannonFactorUsed / wt->nq;

        /**
         * At the beginning of the simulation (t < waxs_tau), the Icalc may be negative simply because Icalc is not yet
         * converged. However, negative Icalc would lead to an error when coupling on a log scale. To avoid this error,
         * allow negative Icalc while t < waxs_tau, while fact0 is anyway still zero:
         */
        gmx_bool bAllowNegativeIcalc = (fForceSwitch == 0.);

        double* diffI = nullptr;
        double* sigma = nullptr;
        snew(diffI, nabs);
        snew(sigma, nabs);

        /** Get delta_I = I_calc - I_exp and sigmas */
        for (int i = 0; i < nabs; i++) {
            diffI[i] = waxs_intensity_Idiff(wr, wt, Icalc, i, bAllowNegativeIcalc, TRUE);
            sigma[i] = waxs_intensity_sigma(wr, wt, IcalcVar, i);
        }

        /** WAXS potential energy is only computed on the master and =0 on all slave nodes */
        if (MASTER(cr) and wr->inputParams->bCalcPot) {
            if (wr->swaxsOutput->fpPot[t]) {
                fprintf(wr->swaxsOutput->fpPot[t], "%8g", simtime);
            }

            /** Recomputing the residuals, to make sure they agree with the result from maximum_likelihood_est_fc_generic() */
            double sumSquaredResidualsRecomputed = 0;

            for (int i = 0; i < nabs; i++) {
                /**
                 * The SAXS/SANS potential, and cumulative averages
                 *
                 * The factor of 0.5 comes from the 1/2 in Eq. 17, Shevchuk & Hub, Plos Comp Biol 2017.
                 * This factor ensures that we follow the Bayesian refinemnt with a force constant of fc=1.
                 */
                real thisv = 0.5 * fact0 * gmx::square(diffI[i] / sigma[i]);

                wt->vLast += thisv;
                wd->vAver[i] = d_accum_avg(wd->vAver[i], (double)thisv, fac1, fac2);
                wd->vAver2[i] = d_accum_avg(wd->vAver2[i], gmx::square(thisv), fac1, fac2);

                sumSquaredResidualsRecomputed += gmx::square(diffI[i] / sigma[i]);

                if (wr->swaxsOutput->fpPot[t]) {
                    fprintf(wr->swaxsOutput->fpPot[t], "  %8g", thisv);
                }
            }

            if (wr->swaxsOutput->fpPot[t]) {
                fprintf(wr->swaxsOutput->fpPot[t], "\n");
                fflush(wr->swaxsOutput->fpPot[t]);
            }

            printf("Type %1d (%s): Fit params: f = %8g  c = %8g  mean residual = %8g  V [kJ/mol] = %8g\n", t,
                    wt->saxssansStr, wt->f_ml, wt->c_ml, sqrt(sumSquaredResiduals / wt->nq), wt->vLast);

            if (wr->inputParams->potentialType == ewaxsPotentialLINEAR
                    and fabs(sumSquaredResidualsRecomputed / nabs - sumSquaredResiduals / nabs) > 0.2 and wr->waxsStep > 30) {
                /** Residuals computed in two ways may slightly differ, because we update the f_ml * sigma[exp] after fitting f_ml */
                fprintf(stderr, "\nWARNING, the residuals computed in two ways are inconsistent. This should not happen:\n"
                        "Via max-likelihood calculation: %g  ---  directly: %g\n",
                        sqrt(sumSquaredResidualsRecomputed / nabs), sqrt(sumSquaredResiduals / nabs));
            }

            /** Add potential of this scattering type to total potential */
            wr->vLast += wt->vLast;
        }

        /** Each node computes all forces at the moment */
        if (wr->inputParams->bCalcForces) {
            /**
             * At negative contrast, moving an atom by +deltaR will shift the contrast by -deltaR,
             * since the empty space will be filled by water. This should approximately lead to
             * a reduction of the force relative to the contrast. For more details, see the comment
             * at the function updateSoluteContrast().
             */
            real contrastFactorUsed;

            if (wr->inputParams->bDoNotCorrectForceByContrast) {
                contrastFactorUsed = 1.;
            } else {
                contrastFactorUsed = wt->contrastFactor;
            }

            /** Should this be parallelized? - But then we need another communication. What is faster? */
            for (int p = 0; p < nprot; p++) {
                rvec ftmp;
                clear_rvec(ftmp);

                for (int i = 0; i < nabs; i++) {
                    /**
                     * We used to have a factor of 2 here - this was removed to follow the Bayesian formalism
                     * if the force constant is fc=1.
                     */
                    real fact1 = -fact0 * contrastFactorUsed * diffI[i] / gmx::square(sigma[i]);

                    if (wr->inputParams->potentialType == ewaxsPotentialLOG) {
                        fact1 /= Icalc[i];
                    }

                    int l = p * nabs + i;

                    for (int d = 0; d < DIM; d++) {
                        ftmp[d] += fact1 * wd->dkI[l][d];
                    }
                }

                if (wr->inputParams->bRotFit) {
                    rvec ftmp2;
                    copy_rvec(ftmp, ftmp2);
                    mvmul(*Rinv, ftmp2, ftmp);
                }

                if (wr->inputParams->ensembleType == swaxs::EnsembleType::MaxEnt)
                {
                    /** Scale the force constant by the number of replicas */

                    wt->fLast[p][XX] = wr->inputParams->ensemble_nstates * ftmp[XX];
                    wt->fLast[p][YY] = wr->inputParams->ensemble_nstates * ftmp[YY];
                    wt->fLast[p][ZZ] = wr->inputParams->ensemble_nstates * ftmp[ZZ];

                    rvec_inc(wr->fLast[p], wt->fLast[p]);
                }
                else
                {
                    /** Store current force of this scattering type, and add to the total force */
                    copy_rvec(ftmp, wt->fLast[p]);
                    rvec_inc(wr->fLast[p], ftmp);
                }
            }
        }

        sfree(diffI);
        sfree(sigma);
        sfree(tau);
    }

    swaxs_debug("Leaving waxs_md_pot_forces()");
}

void do_waxs_md_low(const t_commrec* cr, const gmx_multisim_t* ms, gmx::ArrayRef<gmx::RVec> x, double simtime,
        int64_t step, t_waxsrec* wr, const gmx_mtop_t* mtop, const matrix box, PbcType pbcType, gmx_bool bDoLog) {
    #ifdef RERUN_CalcForces
        wr->inpatParams.bCalcForces = TRUE;
    #endif

    swaxs_debug("Entering do_waxs_md_low()");

    /** Doing this step? */
    gmx_bool bTimeOK = ((wr->inputParams->calcWAXS_begin == -1) or ((simtime + 1e-5) >= wr->inputParams->calcWAXS_begin))
            and ((wr->inputParams->calcWAXS_end == -1) or ((simtime - 1e-5) <= wr->inputParams->calcWAXS_end));

    if (PAR(cr)) {
        gmx_bcast(sizeof(gmx_bool), &bTimeOK, cr->mpi_comm_mygroup);
    }

    if (!bTimeOK) {
        if (MASTER(cr)) {
            fprintf(wr->swaxsOutput->fpLog, "Skipping time %f - outside of time boundaries: begin %g, end %g\n",
                    simtime, wr->inputParams->calcWAXS_begin, wr->inputParams->calcWAXS_end);
            printf("Skipping time %f - outside of time boundaries: begin %g, end %g\n",
                    simtime, wr->inputParams->calcWAXS_begin, wr->inputParams->calcWAXS_end);
        }

        clear_vLast_fLast(wr);

        return;
    }

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::Step, *cr);

    /** Doing solvent in this frame? */
    wr->bDoingSolvent = (!wr->inputParams->bVacuum and wr->waxsStep < wr->inputParams->nfrsolvent);

    if (!wr->inputParams->bVacuum and wr->waxsStep == wr->inputParams->nfrsolvent and MASTER(cr)) {
        printf("\nNote: Reached the requested nr of pure solvent frames (%d)."
               "\n      Will not compute any more pure solvent scattering amplitudes.\n\n", wr->inputParams->nfrsolvent);
    }

    /* = = = = = MASTER does all the preparations before bcast. = = = = = */

    real nelecA = 0.;
    real nelecB = 0.;
    matrix Rinv;
    double time_ms;

    if (MASTER(cr)) {
        wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::PrepareSolute, *cr);

        printf("\nSWAXS/SANS step %d at MD step ", wr->waxsStep);
        printf("%" PRId64, step);
        printf(" (time %6g ps)\n", simtime);
        fprintf(wr->swaxsOutput->fpLog, "\nSWAXS/SANS step %d at MD step ", wr->waxsStep);
        fprintf(wr->swaxsOutput->fpLog, "%" PRId64, step);
        fprintf(wr->swaxsOutput->fpLog, " (time %g ps)\n", simtime);

        /**
         * Do the required steps with the solute before getting the solvation shell:
         *  1. Make solute/protein whole.
         *  2. Get bounding sphere around solute
         *  3. Move this sphere to the center of the box - this maximises the solvent coverage within the unit cell.
         *  3. Put all solvent atoms into the compact box.
         *  4. Obtain the vector between box_center and solute COG. This will become the newBoxCent a the end.
         *  5. Shift fit-group COG to origin.
         *  6. If requested, do a rotation of the solute (using the fitgroup atoms such as Backbone),
         *     and return the inverse rotation matrix.
         *  7. After rotation, move the solute COG to the origin.
         */
        waxs_prepareSoluteFrame(wr, mtop, x, box, pbcType, wr->swaxsOutput->fpLog, Rinv);

        wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::PrepareSolute, *cr);

        /** Pick the pure water box and shift coordinates it to the center of the protein box */

        if (wr->bDoingSolvent) {
            wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::PrepareSolvent, *cr);

            /** Shift pure-solvent frame (frame nr iFrameUsedThisStep) onto envelope and store in 'ws->xPrepared' */
            wr->solvent->prepareNextFrame(wr->wt[0].envelope, wr->inputParams->verbosityLevel);

            wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::PrepareSolvent, *cr);
        }

        /* = = = = = All atoms in place for buffer subtraction = = = = = */

        /* = = = = = Obtain the atoms inside the envelope = = = = =
           Search solvent molecules in solvation layer and in the excluded volume and
           store in index_A (protein+solvation layer) and index_B (excluded solvent),
           respectively. */
        if (!wr->inputParams->bVacuum) {
            auto xExclSolventRef = gmx::arrayRefFromArray(reinterpret_cast<gmx::RVec*>(wr->solvent->xPrepared),
                    wr->solvent->topology->natoms);

            // The new way of getting the solvation shell. Included because some routines already use it.
            // Do not write PDB files (prot+solvationlayer.pdb, ...) since this is still done by the
            // old get_solvation_shell
            //
            // wr->waxs_solv->box[wr->waxsStep%wr->waxs_solv->nframes] picks the box of this solvent frame. Ugly, improve later.
            get_solvation_shell_periodic(x, xExclSolventRef, wr, mtop, wr->solvent->topology.get(), pbcType, box,
                    wr->solvent->boxPrepared, FALSE);

            // The old way of getting solvent inside the envelope. This will be fully replaced by
            // get_solvation_shell_periodic()
            get_solvation_shell(x, xExclSolventRef, wr, mtop, wr->solvent->topology.get(), pbcType, box);

            fprintf(wr->swaxsOutput->fpLog, "Atoms in solute          %5d\n", wr->nindA_prot);
            fprintf(wr->swaxsOutput->fpLog, "Atoms in solvation shell %5d\n", wr->isizeA - wr->nindA_prot);
            fprintf(wr->swaxsOutput->fpLog, "Atoms in excluded volume %5d\n", wr->isizeB);

            /** Check if solvation layer and excluded solvent fill roughly the same volume */
            if (wr->bDoingSolvent) {
                check_selected_solvent(x, xExclSolventRef, wr->indexA, wr->isizeA, wr->indexB, wr->isizeB, TRUE);
            }
        }

        /** For wr->inputParams->bVacuum == TRUE, nothing must be done because indexA and isizeA were initialized to the protein
            in prep_md_scattering_data() */

        /** Get number of electrons and electron density of A and B systems */
        real densA = droplet_density(wr, wr->indexA, wr->isizeA, wr->nElectrons, &nelecA);

        fprintf(wr->swaxsOutput->fpLog, "Density of A [e/nm3] = %10g -- %8g electrons -- A(q=0) = %8g\n",
                densA, nelecA, nelecA * nelecA);

        if (wr->bDoingSolvent) {
            real densB = droplet_density(wr, wr->indexB, wr->isizeB, wr->solvent->nElectronsPerAtom, &nelecB);
            fprintf(wr->swaxsOutput->fpLog, "Density of B [e/nm3] = %10g -- %8g electrons -- B(q=0) = %g\n",
                    densB, nelecB, nelecB * nelecB);
        }

        /* = = = = = Statistics: MASTER calculates solvation shell stats assuming equal averages = = = = = */

        /** Get Radius of Gyration of Solute (without solvation layer) */
        double Rg = soluteRadiusOfGyration(wr, x);
        UPDATE_CUMUL_AVERAGE(wr->RgAv, wr->waxsStep + 1, Rg);

        /** Averages of nr of atoms in surface layer and excluded volume */
        UPDATE_CUMUL_AVERAGE(wr->nAtomsLayerAver, wr->waxsStep + 1, wr->isizeA - wr->nindA_prot);
        UPDATE_CUMUL_AVERAGE(wr->nAtomsExwaterAver, wr->waxsStep + 1, wr->isizeB);
        UPDATE_CUMUL_AVERAGE(wr->nElecAvA, wr->waxsStep + 1, nelecA);
        UPDATE_CUMUL_AVERAGE(wr->nElecAv2A, wr->waxsStep + 1, nelecA * nelecA);
        UPDATE_CUMUL_AVERAGE(wr->nElecAv4A, wr->waxsStep + 1, nelecA * nelecA * nelecA * nelecA);

        if (wr->bDoingSolvent) {
            UPDATE_CUMUL_AVERAGE(wr->nElecAvB, wr->waxsStep + 1, nelecB);
            UPDATE_CUMUL_AVERAGE(wr->nElecAv2B, wr->waxsStep + 1, nelecB * nelecB);
            UPDATE_CUMUL_AVERAGE(wr->nElecAv4B, wr->waxsStep + 1, nelecB * nelecB * nelecB * nelecB);
        }

        if (!wr->inputParams->bVacuum) {
            // printf("Current volume of box = %g nm3\n", det(wr->local_state->box));
            real solDen = (wr->nElecTotA - nelecA) / (det(wr->local_state->box) - envelope_volume_wrapper(wr));

            UPDATE_CUMUL_AVERAGE(wr->solElecDensAv, wr->waxsStep + 1, solDen);
            UPDATE_CUMUL_AVERAGE(wr->solElecDensAv2, wr->waxsStep + 1, solDen * solDen); /* needed for fix_solvent_density */
            UPDATE_CUMUL_AVERAGE(wr->solElecDensAv4, wr->waxsStep + 1, solDen * solDen * solDen * solDen);

            wr->soluteVolAv = envelope_volume_wrapper(wr) - (wr->nElecAvA - wr->nElecProtA) / wr->solElecDensAv;
        }

        /* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
    }

    /* = = = = = END of MASTER preparations = = = = = */

    /** Distribute atoms inside the envelope over the nodes */
    if (PAR(cr)) {
        gmx_bcast(sizeof(int), &wr->isizeA, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &wr->isizeB, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &wr->indexA_nalloc, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(int), &wr->indexB_nalloc, cr->mpi_comm_mygroup);
        gmx_bcast(sizeof(double), &wr->soluteVolAv, cr->mpi_comm_mygroup);
    }

    /** Broadcast the Rotational Matrix for force rotation. */
    if (wr->inputParams->bRotFit and PAR(cr)) {
        gmx_bcast(sizeof(matrix), Rinv, cr->mpi_comm_mygroup);
    }

    /** Possibly expand allocated memory for atomic coordinates and scattering types */
    srenew(wr->atomEnvelope_coord_A, wr->indexA_nalloc);

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        srenew(wr->wt[t].atomEnvelope_scatType_A, wr->indexA_nalloc);
    }

    if (wr->bDoingSolvent) {
        srenew(wr->atomEnvelope_coord_B, wr->indexB_nalloc);

        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            srenew(wr->wt[t].atomEnvelope_scatType_B, wr->indexB_nalloc);
        }
    }

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        /* Compute Fourier Transforms of solvent density, used for solvent density correction in
           solute system */
        if (wr->inputParams->bFixSolventDensity or wr->inputParams->bScaleI0) {
            update_envelope_solvent_density(wr, cr, x, t);
        }
    }

    #if GMX_GPU_CUDA
        /**
         * The envelope of the solvent density is needed on the GPU already in the first WAXS-step.
         * Attention: On the GPU we never recalculate the FT we only pass the values of the solvent FT by reference!
         */
        if (wr->inputParams->bUseGPU and wr->bRecalcSolventFT_GPU) {
            update_envelope_solvent_density_GPU(wr);
        }
    #endif

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::ScattAmplitude, *cr);

    /** The main loop over the scattering groups (xray, neutron1, neutron2,...) */
    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];
        waxs_datablock* wd = wt->wd;

        if (MASTER(cr)) {
            int nDeuter = 0;
            int nHyd = 0;

            for (int i = 0; i < wr->isizeA; i++) {
                int j = wr->indexA[i];

                if (wt->type == escatterXRAY) {
                    assert_scattering_type_is_set(wr->inputParams->cmtypeList[j], wt->type, j);
                    wt->atomEnvelope_scatType_A[i] = wr->inputParams->cmtypeList[j];
                } else {
                    /* get the NSL type, taking this deuterium conc into account */
                    assert_scattering_type_is_set(wr->inputParams->nsltypeList[j], wt->type, j);

                    wt->atomEnvelope_scatType_A[i] = get_nsl_type(wr, t, wr->inputParams->nsltypeList[j]);
                    nDeuter += (wt->atomEnvelope_scatType_A[i] == wt->nnsl_table - 1);
                    nHyd += (wt->atomEnvelope_scatType_A[i] == wt->nnsl_table - 2);

                    // printf("atom %d, nsltype = %d - t = %d\n", j, wr->redA[i].t, t);
                }

                copy_rvec(x[j], wr->atomEnvelope_coord_A[i]);
            }

            UPDATE_CUMUL_AVERAGE(wt->n2HAv_A, wr->waxsStep + 1, nDeuter);
            UPDATE_CUMUL_AVERAGE(wt->n1HAv_A, wr->waxsStep + 1, nHyd);
            UPDATE_CUMUL_AVERAGE(wt->nHydAv_A, wr->waxsStep + 1, nHyd + nDeuter);
        }

        if (PAR(cr)) {
            gmx_bcast(wr->isizeA * sizeof(rvec), wr->atomEnvelope_coord_A, cr->mpi_comm_mygroup);
            gmx_bcast(wr->isizeA * sizeof(int), wt->atomEnvelope_scatType_A, cr->mpi_comm_mygroup);
        }

        int qstart = wt->qvecs->qstart;
        int qhomenr = wt->qvecs->qhomenr;

        /** B-part: scattering of excluded solvent */
        if (wr->bDoingSolvent) {
            if (MASTER(cr)) {
                int nDeuter = 0;
                int nHyd = 0;

                for (int i = 0; i < wr->isizeB; i++) {
                    int j = wr->indexB[i];

                    if (wt->type == escatterXRAY) {
                        assert_scattering_type_is_set(wr->solvent->cromerMannTypes[j], wt->type, j);
                        wt->atomEnvelope_scatType_B[i] = wr->solvent->cromerMannTypes[j];
                    } else {
                        /** Get the NSL type, taking this deuterium conc into account */
                        assert_scattering_type_is_set(wr->solvent->neutronScatteringLengthTypes[j], wt->type, j);

                        wt->atomEnvelope_scatType_B[i] = get_nsl_type(wr, t, wr->solvent->neutronScatteringLengthTypes[j]);
                        nDeuter += (wt->atomEnvelope_scatType_B[i] == wt->nnsl_table - 1);
                        nHyd += (wt->atomEnvelope_scatType_B[i] == wt->nnsl_table - 2);
                    }

                    copy_rvec(wr->solvent->xPrepared[j], wr->atomEnvelope_coord_B[i]);
                }

                UPDATE_CUMUL_AVERAGE(wt->n2HAv_B, wr->waxsStep + 1, nDeuter);
                UPDATE_CUMUL_AVERAGE(wt->n1HAv_B, wr->waxsStep + 1, nHyd);
                UPDATE_CUMUL_AVERAGE(wt->nHydAv_B, wr->waxsStep + 1, nHyd + nDeuter);
            }

            if (PAR(cr)) {
                gmx_bcast(wr->isizeB * sizeof(rvec), wr->atomEnvelope_coord_B, cr->mpi_comm_mygroup);
                gmx_bcast(wr->isizeB * sizeof(int), wt->atomEnvelope_scatType_B, cr->mpi_comm_mygroup);
            }

            /** Compute instantanous scattering amplitude of pure-solvent sytem, B(\vec{q}) */
            if (wr->inputParams->bUseGPU) {
                #if GMX_GPU_CUDA
                    compute_scattering_amplitude_cuda(wr, cr, t, wd->B, wr->atomEnvelope_coord_B, wt->atomEnvelope_scatType_B,
                            wr->isizeB, 0, qhomenr, wt->type == escatterXRAY ? wt->aff_table : nullptr,
                            wt->type == escatterNEUTRON ? wt->nsl_table : nullptr, wt->qvecs->nabs, wd->normA, wd->normB,
                            wr->inputParams->scale, wr->inputParams->bCalcForces, wr->inputParams->bFixSolventDensity, &time_ms);
                #endif
            } else {
                compute_scattering_amplitude(wd->B, wr->atomEnvelope_coord_B, wt->atomEnvelope_scatType_B, wr->isizeB, 0,
                        wt->qvecs->q + qstart, wt->qvecs->iTable + qstart, qhomenr,
                        wt->type == escatterXRAY ? wt->aff_table : nullptr,
                        wt->type == escatterNEUTRON ? wt->nsl_table : nullptr, nullptr, &time_ms);
            }

            wr->timing->add(wr->waxsStep, swaxs::Timing::Stage::OneScattAmplitude, time_ms / 1000, *cr);
        }

        /** Compute instantanous scattering amplitude of protein + hydration layer, A(\vec{q}) */
        if (wr->inputParams->bUseGPU) {
            #if GMX_GPU_CUDA
                compute_scattering_amplitude_cuda(wr, cr, t, wd->A, wr->atomEnvelope_coord_A, wt->atomEnvelope_scatType_A,
                        wr->isizeA, wr->nindA_prot, qhomenr, wt->type == escatterXRAY ? wt->aff_table : nullptr,
                        wt->type == escatterNEUTRON ? wt->nsl_table : nullptr, wt->qvecs->nabs, wd->normA, wd->normB,
                        wr->inputParams->scale, wr->inputParams->bCalcForces, wr->inputParams->bFixSolventDensity, &time_ms);
            #endif
        } else {
            compute_scattering_amplitude(wd->A, wr->atomEnvelope_coord_A, wt->atomEnvelope_scatType_A, wr->isizeA,
                    wr->nindA_prot, wt->qvecs->q + qstart, wt->qvecs->iTable + qstart, qhomenr,
                    wt->type == escatterXRAY ? wt->aff_table : nullptr,
                    wt->type == escatterNEUTRON ? wt->nsl_table : nullptr, wd->dkAbar, &time_ms);
        }

        wr->timing->add(wr->waxsStep, swaxs::Timing::Stage::OneScattAmplitude, time_ms / 1000, *cr);

        /** Extra-check: Make sure that A[0]/B[0] is in agreement with the total number of electrons in the envelope */
        if (wt->qvecs->qstart == 0 and norm2(wt->qvecs->q[0]) == 0.0 and wt->type == escatterXRAY and wr->inputParams->bUseGPU == FALSE) {
            if (fabs(wd->A[0].re - nelecA) / nelecA > 1e-6 and MASTER(cr)) {
                gmx_fatal(FARGS, "A(q=0) = %g is inconsistent with # of electrons in A: %g\n", wd->A[0].re, nelecA);
            }

            if (wr->bDoingSolvent and MASTER(cr) and fabs(wd->B[0].re - nelecB) / nelecB > 1e-6) {
                gmx_fatal(FARGS, "B(q=0) is inconsistent with # of electrons in B: %g / %g\n", wd->B[0].re, nelecB);
            }
        }

        /** Update total number of electrons or NSLs of A and B systems */
        real tmp = number_of_electrons_or_NSLs(wt, wt->atomEnvelope_scatType_A, wr->isizeA, mtop);
        UPDATE_CUMUL_AVERAGE(wd->avAsum, wr->waxsStep + 1, tmp);

        if (wr->bDoingSolvent) {
            tmp = number_of_electrons_or_NSLs(wt, wt->atomEnvelope_scatType_B, wr->isizeB, mtop);
            UPDATE_CUMUL_AVERAGE(wd->avBsum, wr->waxsStep + 1, tmp);
        }

        wd = nullptr;
        wt = nullptr;
    }

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::ScattAmplitude, *cr);

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::SolvDensCorr, *cr);

    for (int t = 0; t < wr->inputParams->nTypes; t++) {
        if (wr->inputParams->bFixSolventDensity and !wr->inputParams->bVacuum) {
            /**
             * This function we need also to execute in case of GPU calculation to get estimation of added electrons etc..
             * However, in case of GPU calculation the actual fix_solvent_density correction will be done on the GPU!"
             */
            fix_solvent_density(wr, cr, t);
        }
    }

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::SolvDensCorr, *cr);

    if (MASTER(cr) and wr->inputParams->bGridDensity) {
        update_envelope_griddensity(wr, x);
    }

    if (wr->inputParams->bScaleI0) {
        scaleI0_getAddedDensity(wr, cr);
    }

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::ScattUpdates, *cr);

    /** Update average of scattering amplitudes A(q) and B(q) */
    update_AB_averages(wr);

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::ScattUpdates, *cr);

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::ComputeIdkI, *cr);

    /** Compute I(q) and gradients of I(q) wrt. atomic positions */
    calculate_I_dkI(wr, cr, ms);

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::ComputeIdkI, *cr);

    /** Updating estimates for the solute contrast wrt. to the buffer */
    updateSoluteContrast(wr, cr);

    #ifndef RERUN_CalcForces

    if (wr->inputParams->bCalcForces or wr->inputParams->bCalcPot) {
        wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::PotForces, *cr);

        /* Clear old potential and forces */
        clear_vLast_fLast(wr);

        /* Update number of independent data points (Shannon-Nyquist theorem) */
        nIndep_Shannon_Nyquist(wr, x, cr, wr->waxsStep == 0 and (MASTER(cr)));

        /**
         * GIBBS SAMPLING:
         *    - Sample the uncertainty factor for the buffer density using Gibbs sampling
         *    - With ensemble refinment, in addition sample the weights of the different states
         *
         * On exit: New buffer epsilon and state weights present, already broadcastet over the nodes
         *          wr->bayesianEnsemble.sum_I and wr->bayesianEnsemble.sum_Ivar are updated.
         */
        if (wr->inputParams->ensembleType == swaxs::EnsembleType::BayesianOneRefined or wr->inputParams->bBayesianSolvDensUncert) {
            swaxs::bayesian_gibbs_sampling(wr, simtime, cr);
        }

        /** Compute SAXS-drived potential and forces */
        waxs_md_pot_forces(cr, wr, simtime, &Rinv);

        if (MASTER(cr) and wr->inputParams->bCalcForces) {
            write_total_force_torque(wr, x);
        }

        if (wr->inputParams->verbosityLevel > 1 and wr->inputParams->bRotFit) {
            fprintf(stderr, "Node rotations: Rinv(row1) = %g %g %g\n", Rinv[0][0], Rinv[0][1], Rinv[0][2]);
        }

        wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::PotForces, *cr);
    }

    #endif

    /** Output to log file */
    if (bDoLog and MASTER(cr)) {
        for (int t = 0; t < wr->inputParams->nTypes; t++) {
            t_waxsrecType* wt = &wr->wt[t];

            fprintf(wr->swaxsOutput->fpSpectra[t], "\n&\n# Intensity %d at simulation step ", wr->waxsStep);
            fprintf(wr->swaxsOutput->fpSpectra[t], "%" PRId64, step);

            write_sas_intensity(wr->swaxsOutput->fpSpectra[t], wr, t);

            if (wr->inputParams->bCalcPot and wr->inputParams->weightsType != ewaxsWeightsUNIFORM) {
                write_sas_curve_errors(wr, step, t);
            }

            if ((wr->waxsStep % 10) == 0) {
                fflush(wr->swaxsOutput->fpSpectra[t]);
            }

            if (wr->inputParams->isSwaxsEnsemble() and wr->inputParams->ensembleType == swaxs::EnsembleType::BayesianOneRefined) {
                /** Write ensemble-averaged I(q) */
                fprintf(wr->swaxsOutput->fpSpecEns[t], "\n&\n# Intensity %d at simulation step ", wr->waxsStep);
                fprintf(wr->swaxsOutput->fpSpecEns[t], "%" PRId64, step);
                fprintf(wr->swaxsOutput->fpSpecEns[t], "\n@type xydy\n");

                for (int i = 0; i < wt->nq; i++) {
                    fprintf(wr->swaxsOutput->fpSpecEns[t], "%8g  %12g %12g\n", wt->qvecs->abs[i],
                            wt->bayesianEnsemble.sum_I[i], sqrt(wt->bayesianEnsemble.sum_Ivar[i]));
                }

                if ((wr->waxsStep % 10) == 0) {
                    fflush(wr->swaxsOutput->fpSpecEns[t]);
                }
            }
        }

        if (wr->inputParams->bPrintForces) {
            rvec* x_red = nullptr;
            snew(x_red, wr->nindA_prot);

            if (wr->inputParams->bRotFit) {
                for (int j = 0; j < wr->nindA_prot; j++) {
                    rvec tmpvec;
                    rvec tmpvec2;

                    /** Rotate x to the original frame */
                    rvec_sub(x[wr->indA_prot[j]], wr->origin, tmpvec);

                    /** v' = R(v0 - c0) + c' ; Rotate and re-center. */
                    mvmul(Rinv, tmpvec, tmpvec2);
                    rvec_add(tmpvec2, wr->origin, x_red[j]);
                }
            } else {
                for (int j = 0; j < wr->nindA_prot; j++) {
                    copy_rvec(x[wr->indA_prot[j]], x_red[j]);
                }
            }

            fprintf(stderr, "Writing positions and forces of frame %i into trr file\n", wr->waxsStep);

            for (int t = 0; t < wr->inputParams->nTypes; t++) {
                /* Write coordinate and forces from all scattering groups into separate trr files */
                gmx_trr_write_frame(wr->swaxsOutput->xfout[t], wr->waxsStep, simtime, wr->wt[t].vLast, box, wr->nindA_prot,
                        x_red, nullptr, wr->wt[t].fLast);
            }

            sfree(x_red);
        }
    }

    if (PAR(cr)) {
        gmx_barrier(cr->mpi_comm_mygroup);
    }

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::Step, *cr);

    if (MASTER(cr) and wr->swaxsOutput->fpLog and wr->waxsStep >= SWAXS_STEPS_RESET_TIME_AVERAGES) {
        wr->timing->writeLast(wr->swaxsOutput->fpLog);
    }

    wr->waxsStep++;

    swaxs_debug("Leaving do_waxs_md_low()");
}
