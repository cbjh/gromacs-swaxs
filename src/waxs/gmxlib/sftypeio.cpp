#include "./sftypeio.h"

#include <cstring>  // strcpy, strncmp

#include <gromacs/gmxpreprocess/notset.h>  // NOTSET
#include <gromacs/math/vec.h>  // clear_rvec
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/smalloc.h>  // snew, srenew, sfree
#include <gromacs/random/uniformrealdistribution.h>  // UniformRealDistribution

#include <waxs/types/waxstop.h>  // t_scatt_types
#include <waxs/types/waxsrec.h>  // t_waxsrec

/* This section contains all base structure factor operations. */

void print_cmsf(FILE* out, t_cromer_mann* cm, int nTab) {
    for (int i = 0; i < nTab; i++) {
        fprintf(out, "\t");
    }

    for (int i = 0; i < 4; i++) {
        fprintf(out, " %8g", cm->a[i]);
    }

    for (int i = 0; i < 4; i++) {
        fprintf(out, " %8g", cm->b[i]);
    }

    fprintf(out, " %8g\n", cm->c);
}

void init_scattering_types(t_scatt_types* t) {
    t->nCromerMannParameters = 0;
    t->nNeutronScatteringLengths = 0;

    t->cromerMannParameters = nullptr;
    t->neutronScatteringLengths = nullptr;
}

void done_scattering_types(t_scatt_types* t) {
    sfree(t->cromerMannParameters);
    sfree(t->neutronScatteringLengths);

    t->nCromerMannParameters = 0;
    t->nNeutronScatteringLengths = 0;
}


#if 0
/* A shorthand way of setting individual factors */
static t_cromer_mann cromer_mann(real a[], real b[], real c) {
    t_cromer_mann cm;

    for (int i = 0; i < 4; i++) {
        cm.a[i] = a[i];
        cm.b[i] = b[i];
    }

    cm.c = c;

    return cm;
}
#endif

static int is_eq_cmsf(t_cromer_mann* x, t_cromer_mann* y) {
    const double eps = 1e-5;

    for (int i = 0; i < 4; i++) {
        if (fabs(x->a[i] - y->a[i]) > eps or fabs(x->b[i] - y->b[i]) > eps) {
            return FALSE;
        }
    }

    if (fabs(x->c - y->c) > eps) {
        return FALSE;
    }

    return TRUE;
}

static int is_eq_nsl(t_neutron_sl* x, t_neutron_sl* y) {
    const double eps = 1e-6;

    if (fabs(x->cohb - y->cohb) > eps) {
        return FALSE;
    }

    return TRUE;
}

/* Look for the Cromer-Mann types cm_new in the list scattTypes */
int search_scattTypes_by_cm(const t_scatt_types* scattTypes, t_cromer_mann* cm_new) {
    if (cm_new == nullptr) {
        gmx_fatal(FARGS, "Trying to search structure-factor table using a null-CMSF!\n");
    }

    for (int i = 0; i < scattTypes->nCromerMannParameters; i++) {
        if (is_eq_cmsf(&scattTypes->cromerMannParameters[i], cm_new)) {
            return i;
        }
    }

    return NOTSET;
}

/* Look for the NSL types nsl_new in the list scattTypes */
int search_scattTypes_by_nsl(const t_scatt_types* scattTypes, t_neutron_sl* nsl_new) {
    if (nsl_new == nullptr) {
        gmx_fatal(FARGS, "Trying to search structure-factor table, but nsl_new = nullptr!\n");
    }

    for (int i = 0; i < scattTypes->nNeutronScatteringLengths; i++) {
        if (is_eq_nsl(&scattTypes->neutronScatteringLengths[i], nsl_new)) {
            return i;
        }
    }

    return NOTSET;
}


int add_scatteringType(t_scatt_types* scattTypes, t_cromer_mann* cm, t_neutron_sl* nsl) {
    if (cm and nsl) {
        gmx_fatal(FARGS, "Can only add a Cromer-Mann or a NSL in add_scatteringType\n");
    }

    if (cm) {
        int icm = scattTypes->nCromerMannParameters;

        scattTypes->nCromerMannParameters++;
        srenew(scattTypes->cromerMannParameters, scattTypes->nCromerMannParameters);

        scattTypes->cromerMannParameters[icm] = *cm;

        return icm;
    } else if (nsl) {
        int insl = scattTypes->nNeutronScatteringLengths;

        scattTypes->nNeutronScatteringLengths++;
        srenew(scattTypes->neutronScatteringLengths, scattTypes->nNeutronScatteringLengths);

        scattTypes->neutronScatteringLengths[insl] = *nsl;

        return insl;
    }

    return -1;
}

#if 0
/**
 * return Cromer-Mann fit for the atomic scattering factor:
 * sin_theta is the sine of half the angle between incoming and scattered
 * vectors. See g_sq.h for a short description of CM fit.
 */
static double md_CMSF(t_cromer_mann cmsf, real lambda, real sin_theta, real alpha, real delta) {
    /**
     * f0[k] = c + [SUM a_i * EXP(-b_i * (k^2))]
     *             i = 1,4
     */

    /** k2 is in units A^-2 (according to parameters b[i]), whereas lambda in nm */
    real k2 = (gmx::square(sin_theta) / gmx::square(10.0 * lambda));
    real tmp = cmsf.c;

    for (int i = 0; (i < 4); i++) {
        tmp += cmsf.a[i] * exp(-cmsf.b[i] * k2);
    }

    /* scale AFF by [ 1 + alpha*exp(-Q^2/2delta^2) ]  --- only used for water.
       Now this is done on the force field level, however. Therefore, alpha is always == 0.
     * Note: we use alpha instead of the (alpha-1) used in eq. 5, Sorenson et al, JCP 113:9194, 2000
     * Note: Q2 is in units nm^-2, delta in nm^-1 */

    if (alpha != 0) {
        real Q2 = gmx::square(4 * M_PI * sin_theta / lambda);
        tmp *= (1 + alpha * exp(-Q2 / (2 * delta * delta)));
    }

    return tmp;
}
#endif

/** Make a table of NSLs found in the topology, and add NSLs of 1H and 2H */
void fill_neutron_scatt_len_table(const t_scatt_types* scattTypes, t_waxsrecType* wt, real backboneDeuterProb) {
    wt->nnsl_table = scattTypes->nNeutronScatteringLengths + 4;
    real nslH = NEUTRON_SCATT_LEN_1H;
    real nslD = NEUTRON_SCATT_LEN_2H;

    snew(wt->nsl_table, wt->nnsl_table);

    int i;
    for (i = 0; i < scattTypes->nNeutronScatteringLengths; i++) {
        wt->nsl_table[i] = scattTypes->neutronScatteringLengths[i].cohb;
    }

    wt->nsl_table[i++] = nslH;
    wt->nsl_table[i++] = nslD;

    /**
     * Optionally, we may assign a mixed NSL of 1H and 2H, corresponding to the deuterium concentration.
     * These are used if wr->inputParams->bStochasticDeuteration == FALSE.
     */

    real probD, nsl;

    /** First: deuteratable groups: */
    probD = wt->deuter_conc;
    nsl = probD * nslD + (1.0 - probD) * nslH;
    wt->nsl_table[i++] = nsl;

    /** Next: deuteratable backbone hydrogens: */
    probD = wt->deuter_conc * backboneDeuterProb;
    nsl = probD * nslD + (1.0 - probD) * nslH;
    wt->nsl_table[i++] = nsl;
}

/**
 * Return the NSL type index, taking the deuterium concentration into account.
 *      wr->inputParams->bStochasticDeuteration == TRUE:
 *
 * If the NSl is NSL_H_DEUTERATABLE and NSL_H_DEUTERATABLE_BACKBONE, they are randomly assigned to 1H or 2H.
 *      wr->inputParams->bStochasticDeuteration == FALSE:
 *
 * If the NSl is NSL_H_DEUTERATABLE and NSL_H_DEUTERATABLE_BACKBONE, a linear interpolation between 1H and 2H is assigned.
 */
int get_nsl_type(t_waxsrec* wr, int t, int nslTypeIn) {
    t_waxsrecType* wt = &wr->wt[t];

    /** Types of 1H, 2H, deuteratable, and deuteratalbe backbone are always at the end of the nsl_table,
        see fill_neutron_scatt_len_table() */
    int nslType_1H                 = wt->nnsl_table - 4;
    int nslType_2H                 = wt->nnsl_table - 3;
    int nslType_deuteratable_mixed = wt->nnsl_table - 2;
    int nslType_deutBackbone_mixed = wt->nnsl_table - 1;

    gmx::UniformRealDistribution<real> uniform_distribution;

    if (fabs(wt->nsl_table[nslTypeIn] - NSL_H_DEUTERATABLE_BACKBONE) < 1e-5) {
        /** Backbone hydrogen */
        if (wr->inputParams->bStochasticDeuteration) {
            double probDeuter = wt->deuter_conc * wr->inputParams->backboneDeuterProb;

            return (uniform_distribution(*(wr->rng)) < probDeuter) ? nslType_2H : nslType_1H;
        } else {
            return nslType_deutBackbone_mixed;
        }
    } else if (fabs(wt->nsl_table[nslTypeIn] - NSL_H_DEUTERATABLE) < 1e-5) {
        /** Other deuteratable hydrogen (typically polar hydrogen) */
        if (wr->inputParams->bStochasticDeuteration) {
            return (uniform_distribution(*(wr->rng)) < wt->deuter_conc) ? nslType_2H : nslType_1H;
        } else {
            return nslType_deuteratable_mixed;
        }
    } else {
        return nslTypeIn;
    }
}

void assert_scattering_type_is_set(int stype, int type, int atomno) {
    if (stype == NOTSET) {
        gmx_fatal(FARGS, "Found a scattering type (%s) that is NOTSET, atom no %d. This should not happen.\n",
                (type == escatterXRAY) ? "Cromer-Mann type" : "Neutron scatterling length type", atomno + 1);
    }
}


double CMSF_q(t_cromer_mann cmsf, real q) {
    real f = cmsf.c;

    /**
     * Note: Cromer-Mann parameters use inverse Angstroem, but our q is in inv. nm.
     * Also note: q / 4pi = sin(theta) / lambda
     */
    real k2 = gmx::square(q / (4 * M_PI * 10));

    for (int i = 0; (i < 4); i++) {
        f += cmsf.a[i] * exp(-cmsf.b[i] * k2);
    }

    return f;
}


/** Prepare the atomic form factor tables (aff_table), one for each Cromer-Mann type */
void compute_atomic_form_factor_table(const t_scatt_types* scattTypes, t_waxsrecType* wt, t_commrec* cr) {
    wt->naff_table = scattTypes->nCromerMannParameters;
    snew(wt->aff_table, wt->naff_table);

    if (!wt->qvecs) {
        gmx_fatal(FARGS, "Atomic form factors cannot be calculated without a defined set of q-vectors."
                " Please initialise them first!\n");
    }

    if (MASTER(cr)) {
        printf("Building atomic form factor table for %d SF-tytpes and %d |q|\n", wt->naff_table, wt->nq);
    }

    for (int i = 0; (i < wt->naff_table); i++) {
        snew(wt->aff_table[i], wt->nq);

        for (int j = 0; j < wt->nq; j++) {
            wt->aff_table[i][j] = CMSF_q(scattTypes->cromerMannParameters[i], wt->qvecs->abs[j]);
        }
    }
}



