#include "./maxent_ensemble.h"

#include <gromacs/gmxlib/network.h>  // gmx_bcast, gmx_barrier
#include <gromacs/mdrunutility/multisim.h>  // gmx_multisim_t
#include <gromacs/mdtypes/commrec.h>  // t_commrec
#include <gromacs/utility/cstringutil.h>  // STRLEN
#include <gromacs/utility/fatalerror.h>  // gmx_fatal, FARGS

#include <waxs/types/waxsrec.h>  // t_waxsrec

void swaxs::init_maxent_ensemble_refinement(t_waxsrec* wr, const gmx_multisim_t* ms) {
    const auto& inputParams = wr->getInputParams();

    if (inputParams.ensemble_nstates < 2 or inputParams.ensemble_nstates > 50) {
        gmx_fatal(FARGS, "Error in init_maxent_ensemble_refinement(), inputParams.ensemble_nstates = %d\n",
                  inputParams.ensemble_nstates);
    }

    if (inputParams.ensembleType == swaxs::EnsembleType::MaxEnt) {
        if (!isMultiSim(ms)) {
            gmx_fatal(FARGS, "For Maximum Entropy ensemble refinement, use the gmx mdrun -multisim functionality");
        }

        if (inputParams.ensemble_nstates < 2) {
            gmx_fatal(FARGS, "For Maximum Entropy ensemble refinement, expected waxs-ensemble-nstates >= 2 (found %d)",
                      inputParams.ensemble_nstates);
        }

        if (ms->numSimulations_ != inputParams.ensemble_nstates) {
            gmx_fatal(FARGS, "For Maximum Entropy ensemble refinement, the number of states in the ensemble\n"
                      "(specified with the mdp option waxs-ensemble-nstates) must equal to the number of simulations\n"
                      "invoked with the gmx mdrun -multisim functionality. Found %d states, %d simulations",
                      inputParams.ensemble_nstates, ms->numSimulations_);
        }
    }

    char typeStr[STRLEN];

    for (int t = 0; t < inputParams.nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];

        if (inputParams.nTypes > 1) {
            sprintf(typeStr, "_scatt%d", t);
        } else {
            strcpy(typeStr, "");
        }

        if (inputParams.ensembleType == swaxs::EnsembleType::MaxEnt) {
            snew(wt->wd->maxEntEnsemble.ensemble_I_Aver, wt->qvecs->nabs);
            snew(wt->wd->maxEntEnsemble.ensemble_varI_Aver, wt->qvecs->nabs);
        } else {
            gmx_fatal(FARGS, "Unsupported ensemble type in init_maxent_ensemble_refinement()\n");
        }
    }
}

void swaxs::maxent_ensemble_average(t_waxsrec* wr, t_waxsrecType* wt, const t_commrec* cr, const gmx_multisim_t* ms) {
    const auto& inputParams = wr->getInputParams();

    /** Consistency check if ensemble_nstates from the .mdp file matches the number of running replicas. */
    if (inputParams.ensemble_nstates != ms->numSimulations_) {
        gmx_fatal(FARGS, "Number of states of ensemble (%d) does not match number of replicas (%d).",
                  inputParams.ensemble_nstates, ms->numSimulations_);
    }

    /** Consistency check if we have stats with weights (close to) zero and if the stats sum up to one. */
    if (SIMMASTER(cr)) {
        double sum = 0;

        for (int i = 0; i < ms->numSimulations_; i++) {
            sum += wr->ensemble_weights[i];

            if (wr->ensemble_weights[i] < 0.005) {
                gmx_fatal(FARGS, "The weight for replica %d is too small (%.3f). We do not allow this. \n", i,
                          wr->ensemble_weights[i]);
            }
        }

        if (fabs(sum - 1) > 0.0001) {
            gmx_fatal(FARGS, "The weights for the replicas does not sum up to 1. Sum is: %g \n", sum);
        }
    }

    gmx_barrier(cr->mpiDefaultCommunicator);

    /** We make a local copy of wt->wd->I[i] in each replica so not to overwrite wt->wd->I[i]. We also scale it by the weight. */
    for (int i = 0; i < wt->qvecs->nabs; i++) {
        /** We add the weights here. Then sum up in the next step*/
        wt->wd->maxEntEnsemble.ensemble_I_Aver[i] = wr->ensemble_weights[ms->simulationIndex_] * wt->wd->I[i];
        wt->wd->maxEntEnsemble.ensemble_varI_Aver[i] = wr->ensemble_weights[ms->simulationIndex_] * wt->wd->varI[i];
    }

    /** Only master can use gmx_sumd_comm (gmx_sumd_sim) */
    if (MASTER(cr)) {
        /** We just sum up the vectors here. As they are weighted in the previous step no additional action needed.*/
        gmx_sumd_sim(wt->qvecs->nabs, wt->wd->maxEntEnsemble.ensemble_I_Aver, ms);
        gmx_sumd_sim(wt->qvecs->nabs, wt->wd->maxEntEnsemble.ensemble_varI_Aver, ms);
    }

    if (PAR(cr)) {
        /* broadcast ensemle average of I(q) */
        gmx_bcast(wt->qvecs->nabs * sizeof(wt->wd->maxEntEnsemble.ensemble_I_Aver[0]), wt->wd->maxEntEnsemble.ensemble_I_Aver, cr->mpi_comm_mygroup);
        gmx_bcast(wt->qvecs->nabs * sizeof(wt->wd->maxEntEnsemble.ensemble_varI_Aver[0]), wt->wd->maxEntEnsemble.ensemble_varI_Aver, cr->mpi_comm_mygroup);
    }
}
