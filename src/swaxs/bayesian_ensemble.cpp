#include "./bayesian_ensemble.h"

#include <gromacs/fileio/gmxfio.h>  // t_fileio, gmx_ffopen, gmx_ffclose, gmx_fexist
#include <gromacs/gmxlib/network.h>  // gmx_bcast, gmx_sumd
#include <gromacs/random/uniformrealdistribution.h>  // UniformRealDistribution
#include <gromacs/utility/fatalerror.h>  // gmx_fatal

#include <waxs/gmxlib/init_waxs.h>  // read_intensity_and_interpolate
#include <waxs/gmxlib/swaxs_datablock.h>  // waxs_datablock
#include <waxs/gmxlib/waxsmd.h>  // waxs_intensity_sigma
#include <waxs/gmxlib/waxsmd_utils.h>
#include <waxs/types/waxstop.h>  // t_scatt_types

/**
 * Maxiumum-likelihodd estimate for f and c, for fitting f * Iexp + c to Icalc.
 * Eqs. 13 and 14 in Shevchuk & Hub, Plos Comp Biol https://doi.org/10.1371/journal.pcbi.1005800
 */
static void maximum_likelihood_est_fc(double* Icalc, double* Iexp, double* tau, int nq, double* f_ml, double* c_ml,
        double* sumSquaredResiduals) {
    double av_c, av_e, sig_c, sig_e;

    /** These functions also work with tau == nullptr */
    average_stddev_d(Icalc, nq, &av_c, &sig_c, tau);
    average_stddev_d(Iexp, nq, &av_e, &sig_e, tau);
    double P = pearson_d(nq, Icalc, Iexp, av_c, av_e, sig_c, sig_e, tau);

    /** Maximum likelihood estimates for f and c */
    *f_ml = P * sig_c / sig_e;
    *c_ml = av_c - (*f_ml) * av_e;

    if (sumSquaredResiduals) {
        double sum_tau = tau ? sum_array_d(nq, tau) : nq;
        /** This sumSquaredResiduals calculation is accurate up to at least 10 digits, checked. */
        *sumSquaredResiduals = sum_tau * (sig_c * sig_c * (1.0 - P * P));
    }
}

/**
 * ML estimate for f only (but not c), for fitting f*Iexp to Icalc.
 * Eq. above Eq. 23 in Shevchuk & Hub, Plos Comp Biol https://doi.org/10.1371/journal.pcbi.1005800
 */
static void maximum_likelihood_est_f(double* Icalc, double* Iexp, double* tau, int nq, double* f_ml, double* sumSquaredResiduals) {
    double avY2, avXY, avX2, sum_tau;

    /** These functions also work with tau == nullptr */
    average_x2_d(Iexp, nq, &avY2, tau);
    average_xy_d(Iexp, Icalc, nq, &avXY, tau);

    /**
     * Maximum likelihood estimates for scale
     * Note that we here fit the experiment to the calculated curve. Then we have:
     */
    *f_ml = avXY / avY2;

    if (sumSquaredResiduals) {
        average_x2_d(Icalc, nq, &avX2, tau);
        sum_tau = tau ? sum_array_d(nq, tau) : nq;
        *sumSquaredResiduals = sum_tau * (avX2 - (*f_ml) * avXY);
    }
}

void swaxs::maximum_likelihood_est_fc_generic(t_waxsrec* wr, double* Icalc, double* Iexp, double* tau, int nq,
        double* f_ml, double* c_ml, double* sumSquaredResiduals) {
    const auto& inputParams = wr->getInputParams();

    switch (inputParams.ewaxs_Iexp_fit) {
        case ewaxsIexpFit_NO:
            if (sumSquaredResiduals) {
                sum_squared_residual_d(Icalc, Iexp, nq, sumSquaredResiduals, tau);
            }

            *f_ml = 1;
            *c_ml = 0;

            break;
        case ewaxsIexpFit_SCALE:
            maximum_likelihood_est_f(Icalc, Iexp, tau, nq, f_ml, sumSquaredResiduals);
            *c_ml = 0;
            break;
        case ewaxsIexpFit_SCALE_AND_OFFSET:
            maximum_likelihood_est_fc(Icalc, Iexp, tau, nq, f_ml, c_ml, sumSquaredResiduals);
            break;
        default:
            gmx_fatal(FARGS, "Invalid value for inputParams.ewaxs_Iexp_fit (%d)\n", inputParams.ewaxs_Iexp_fit);
    }
}

/** Init the Bayesian refinement of one state together with the weights of several other fixed states.
 *
 *  Read the SAXS curve of the other states.
 *  See Shevchuk and Hub, Plos Comp Biol, 2017
 */
void swaxs::init_bayesian_ensemble_refinement(t_waxsrec* wr, t_commrec* cr) {
    const auto& inputParams = wr->getInputParams();

    if (inputParams.ensemble_nstates < 2 or inputParams.ensemble_nstates > 50) {
        gmx_fatal(FARGS, "Error in init_ensemble_stuff(), inputParams.ensemble_nstates = %d\n", inputParams.ensemble_nstates);
    }

    for (int t = 0; t < inputParams.nTypes; t++) {
        t_waxsrecType* wt = &wr->wt[t];

        /** Read and broadcast intensities of fixed states from files intensity_stateXX.dat */
        snew(wt->bayesianEnsemble.I, inputParams.ensemble_nstates - 1);
        snew(wt->bayesianEnsemble.Ivar, inputParams.ensemble_nstates - 1);

        char typeStr[1024];

        if (inputParams.nTypes > 1) {
            sprintf(typeStr, "_scatt%d", t);
        } else {
            strcpy(typeStr, "");
        }

        /* Refining only one state with the MD, while reading the SAXS curves of all other states from a file. */
        if (inputParams.ensembleType == swaxs::EnsembleType::BayesianOneRefined) {
            for (int m = 0; m < inputParams.ensemble_nstates - 1; m++) {
                if (MASTER(cr)) {
                    char fn[STRLEN];

                    sprintf(fn, "intensity%s_state%02d.dat", typeStr, m);

                    if (!gmx_fexist(fn)) {
                        gmx_fatal(FARGS, "For ensemble refinement with %d states, you need to place %d file(s) with name(s) "
                                "intensity_stateXX.dat\n"
                                "into the current directory, where XX are two digits (00, 01, 02, ...)\n"
                                "If you have multiple scattering groups (xray, neutron, etc.), then the file names "
                                "are\n"
                                "intensity_scattY_stateXX.dat, where Y = 0, 1, 2, etc. for each scatterig group "
                                "defined in the mdp file (option scatt-coupl)\n\n"
                                "However, %s was not found.\n",
                                inputParams.ensemble_nstates, inputParams.ensemble_nstates - 1, fn);
                    }

                    read_intensity_and_interpolate(fn, wt->minq, wt->maxq, wt->nq, true, true,
                            &wt->bayesianEnsemble.I[m], &wt->bayesianEnsemble.Ivar[m]);

                    printf("WAXS-MD: Read intensity of fixed state %d from file %s (scattering group %d)\n", m, fn, t);
                }

                /* Broadcast it */
                if (!MASTER(cr)) {
                    snew(wt->bayesianEnsemble.I[m], wt->nq);
                    snew(wt->bayesianEnsemble.Ivar[m], wt->nq);
                }

                if (PAR(cr)) {
                    gmx_bcast(wt->nq * sizeof(double), wt->bayesianEnsemble.I[m], cr->mpi_comm_mygroup);
                    gmx_bcast(wt->nq * sizeof(double), wt->bayesianEnsemble.Ivar[m], cr->mpi_comm_mygroup);
                }
            }
        } else {
            gmx_fatal(FARGS, "Unsupported ensemble type in  init_ensemble_stuff()\n");
        }

        /* init intensity sum arrays for ensemble refinement */
        snew(wt->bayesianEnsemble.sum_I, wt->nq);
        snew(wt->bayesianEnsemble.sum_Ivar, wt->nq);
    }
}

/**
 * Computes the log Likelihood, after marginalizing the fitting parameters (f and c, only f, or none).
 * - tau-array must be allocated before entry.
 * - Icalc / IcalcVar is the overall calculated intensities, which may contain contributions
 *   from a heterogenous ensmble or not.
 */
static double bayesian_md_calc_logLikelihood(t_waxsrec* wr, int t, double* Icalc, double* IcalcVar, double* tau) {
    const auto& inputParams = wr->getInputParams();

    if (inputParams.nTypes > 1) {
        gmx_fatal(FARGS, "Do now allow more than one scattering type with Bayesian stuff\n");
    }

    if (!tau) {
        gmx_fatal(FARGS, "Found tau == nullptr in bayesian_md_calc_logLikelihood()\n");
    }

    if (t >= inputParams.nTypes) {
        gmx_fatal(FARGS, "Invalid waxs type number (%d)\n", t);
    }

    t_waxsrecType* wt = &wr->wt[t];
    double zeta = wt->nShannon / wt->nq;

    for (int i = 0; i < wt->nq; i++) {
        /** Compute the statistical "precisions", or 1 / sigma^2 */
        tau[i] = 1. / gmx::square(waxs_intensity_sigma(wr, wt, IcalcVar, i));
    }

    /** Get \hat{chi}^2, that is the sum of squared residuals (weighted by tau) */
    double sumSquaredResiduals = 0;
    swaxs::maximum_likelihood_est_fc_generic(wr, Icalc, wt->Iexp, tau, wt->nq, &wt->f_ml, &wt->c_ml, &sumSquaredResiduals);

    double logL = -0.5 * zeta * sumSquaredResiduals;

    if (std::isnan(logL) or std::isinf(logL)) {
        gmx_fatal(FARGS, "Encountered an invalid log-likilihood (logL = %g)\n", logL);
    }

    return logL;
}

/**
 * Do Gibbs sampling of the uncertainty epsilon_buff in the buffer density
 *
 *  epsilon_buff = 0  -> means no uncertainty due to the buffer density
 *  epsilon_buff = 1  -> means the uncertainty is the one given by the mdp option "waxs-solvdens-uncert"
 *  epsilon_buff = 2  -> uncertainty 2 times waxs-solvdens-uncert, etc.
 *
 * As prior for epsilon_buff, we use a Gaussian with width = 1
 */
#define WAXS_BAYESIAN_EPSBUFF_MAX 6  /** Maximum epsilon (or number of sigmas) sampled */
#define GIBBS_NST_OUTPUT 10          /** Write to waxs_gibbs.dat every XX steps */

static int bayesian_sample_buffer_uncertainty(t_waxsrec* wr, double simtime, double* Icalc, double* IcalcVar, int nMCmove) {
    const auto& inputParams = wr->getInputParams();

    if (inputParams.nTypes > 1) {
        gmx_fatal(FARGS, "Do now allow more than one scattering type with Bayesian stuff\n");
    }

    int t = 0;
    t_waxsrecType* wt = &wr->wt[t];

    double* tau = nullptr;
    snew(tau, wt->nq);

    double logLprev = bayesian_md_calc_logLikelihood(wr, t, Icalc, IcalcVar, tau);
    double eps_prev = wr->epsilon_buff;

    FILE* fp = wr->swaxsOutput->fpGibbsSolvDensRelErr[t];
    fprintf(fp, "%8g", simtime);

    gmx::UniformRealDistribution<real> uniform_distribution;

    int nAccept = 0;

    for (int i = 0; i < nMCmove; i++) {
        /** Propose update of epsilon_buff */
        wr->epsilon_buff = uniform_distribution(*(wr->rng)) * WAXS_BAYESIAN_EPSBUFF_MAX;

        /** Compute new likelihood */
        double logL = bayesian_md_calc_logLikelihood(wr, t, Icalc, IcalcVar, tau);

        /** Prior for epsilon is a Gaussian of width 1: pi(eps) = exp(-eps^2/2) */
        double priorFact = exp(-0.5 * (gmx::square(wr->epsilon_buff) - gmx::square(eps_prev)));
        double pjointRel = exp(logL - logLprev) * priorFact;

        gmx_bool bAccept;

        /** Accept according to Metropolis criterium */
        if (pjointRel > 1) {
            bAccept = TRUE;
        } else {
            double r = uniform_distribution(*(wr->rng));
            bAccept = (r < pjointRel);
        }

        if (bAccept) {
            logLprev = logL;
            eps_prev = wr->epsilon_buff;
            nAccept++;
        } else {
            /** Put back the old value */
            wr->epsilon_buff = eps_prev;
            logL = logLprev;
        }

        if ((i % GIBBS_NST_OUTPUT) == 0) {
            /* Writing the current relative uncertainty of the solvent density (e.g. 0.01 measn 1% uncertainty) */
            fprintf(fp, " %g", wr->epsilon_buff * inputParams.solventDensRelErr);
        }
    }

    fprintf(fp, "\n");

    sfree(tau);

    return nAccept;
}

/**
 * Ensemble-average intensities, averaged over M-1 fixed states plus the state of the MD simulation .
 * Weights don't have to be normalized - however, so far they are already normalized upon entry.
 */
static void intensity_ensemble_average(int nq, int M, double* w, double** I_fixed, double** Ivar_fixed, double* I_MD,
        double* Ivar_MD, double* Icalc, double* IcalcVar) {
    double wsum = 0;

    for (int m = 0; m < M; m++) {
        wsum += w[m];
    }

    /** Loop over q-vectors */
    for (int i = 0; i < nq; i++) {
        Icalc[i] = 0;
        IcalcVar[i] = 0;

        /** Loop over fixed states */
        for (int m = 0; m < M - 1; m++) {
            Icalc[i] += w[m] * I_fixed[m][i];
            IcalcVar[i] += gmx::square(w[m]) * Ivar_fixed[m][i];  /** Gaussian error propagation */
        }

        /** Add I / varI of MD simulation state */
        int m = M - 1;
        Icalc[i] += w[m] * I_MD[i];
        IcalcVar[i] += gmx::square(w[m]) * Ivar_MD[i];

        /** Normalize */
        Icalc[i] /= wsum;
        IcalcVar[i] /= gmx::square(wsum);
    }
}


#if 0
static int compare_double(const void* a, const void* b) {
    double diff = *(double*)a - *(double*)b;

    if (diff > 0) {
        return 1;
    } else if (diff < 0) {
        return -1;
    } else {
        return 0;
    }
}
#endif

#define GMX_WAXS_ENSWEIGHT_MOVE_MAX 0.1

/** Allow weights also < 0 or > 1, so to avoid artifacts at the 0/1 boundary in weights space. */
static int bayesian_sample_ensemble_weights(t_waxsrec* wr, double simtime, int nMCmove) {
    const auto& inputParams = wr->getInputParams();

    gmx::UniformRealDistribution<real> uniform_distribution;

    if (inputParams.nTypes > 1) {
        gmx_fatal(FARGS, "Do now allow more than one scattering type with Bayeisan stuff\n");
    }

    int t = 0;
    t_waxsrecType* wt = &wr->wt[t];
    int M = inputParams.ensemble_nstates;

    double* Icalc = nullptr;
    double* IcalcVar = nullptr;
    snew(Icalc, wt->nq);
    snew(IcalcVar, wt->nq);

    double* R = nullptr;
    snew(R, M + 1);

    /** Get placeholder for weights, and initiate it with the old weights */
    double* wNew;
    snew(wNew, M);
    memcpy(wNew, wr->ensemble_weights, M * sizeof(double));

    intensity_ensemble_average(wt->nq, M, wr->ensemble_weights, wt->bayesianEnsemble.I, wt->bayesianEnsemble.Ivar,
            wt->wd->I, wt->wd->varI, Icalc, IcalcVar);

    double* tau = nullptr;
    snew(tau, wt->nq);

    double logL = bayesian_md_calc_logLikelihood(wr, t, Icalc, IcalcVar, tau);

    /**
     * We need to draw random weights, such that the sum of the weigths is one
     *
     * To generate such N uniformly distributed random numbers with a sum of 1, one typically does the following:
     *  1) Draw N-1 random numbers R between 0 and 1 and put them in a list L.
     *  2) Also add 0 and 1 to the list.
     *  3) Sort the list.
     *  4) The N random numbers are given by L1-L0, L2-L1, ... L[N]-L[N-1]
     *
     *  Note: I tested this against the Python code given on the Wikipedia site, all fine.
     *
     *  So, in terms of a MC step, we can randomly move the N-1 random numbers R.
     *
     *  Addition: Due to the memory effect in our calculated SAXS curves, we get artifacts
     *            at the hard boundaries at 0 and 1 of the weights. Therefore, extend the
     *            flat Dirichlet prior such that weights slightly smalle 0 and larger 1
     *            are possible as well. This is achieved as follows:
     *
     *            1) Compute the weights \vec{w0} according to the Dirichelet distribution, see above.
     *            2) Multiply the weights vector \vec{w0} by (1+xi*N) and shift it back along the vector
     *               -xi-(1,....1), such that ||w0||_1 = 1, so the weights remain normalized:
     *
     *               \vec{wc} = (1+xi*N) * \vec{w0} - xi*(1,...,1)
     *
     *              Then, the new weight vector \vec{wc} is still normalized, but its elements are within
     *              the element:
     *
     *                  wc_i \in [-xi, 1 + N*xi - xi]
     *
     *              for example with N = 2, wc_i \in [-xi, 1 + xi]  (instead of [0,1])
     */

    R[0] = 0;
    R[M] = 1;

    int nAccept = 0;

    /** Allow state weights in interval [-xi, 1 + M*xi - xi] */
    double xi = inputParams.stateWeightTolerance;

    for (int i = 0; i < nMCmove; i++) {
        /** Set back weights */
        memcpy(wNew, wr->ensemble_weights, M * sizeof(double));

        /**
         * Set the list of random numbers between 0 and 1
         *   R[1]      is the weight of state 0
         *   R[2]-R[1] is the weight of state 1, etc
         */

        for (int m = 0; m < M - 1; m++) {
            /** Weights wNew[m] can be <0 or >1 if inputParams->stateWeightTolerance > 0, so map back to the [0,1] interval. */
            double wNonScaled = (wNew[m] + xi) / (1 + M * xi);
            R[m + 1] = R[m] + wNonScaled;

            if (R[m + 1] > 1.001 or R[m + 1] < -0.001) {
                gmx_fatal(FARGS, "Error while initiating R (should be 0<= R <= 1), found %g\n", R[m + 1]);
            }
        }

        /** Do random moves of the R */
        gmx_bool bOutsideBounds = FALSE;

        for (int m = 0; m < M - 1; m++) {
            R[m + 1] += (2 * uniform_distribution(*(wr->rng)) - 1) * GMX_WAXS_ENSWEIGHT_MOVE_MAX;

            if (R[m + 1] >= 1. or R[m + 1] < 0.) {
                bOutsideBounds = TRUE;
            }
        }

        if (bOutsideBounds) {
            /** Reject move when one of the R[] is outside [0,1] */
            continue;
        }

        /** Sort R - use qsort_threadsafe() defined in gmx_sort.h */

        // `gmx_qsort_threadsafe` have been removed in Gromacs 2019.
        // I am not sure if _threadsafe_ version have been used here on purpose or _just in case_.
        // Calculations give expected result with STL sort.

        // TODO: Verify that there is no need for more sophisticated approach, then remove this comment.
        // `qsort_threadsafe.cpp/h` can be found in Gromacs 2018 if needed.

        // gmx_qsort_threadsafe(R, M+1, sizeof(double), compare_double);
        std::sort(R, R + M + 1, std::less<double>());

        /** Pick new weights as differences between neighboring elements in sorted R */
        for (int m = 0; m < M; m++) {
            wNew[m] = R[m + 1] - R[m];
        }

        /**
         * Allow weights slightly larger 1 or smaller 0, so to avoid artifacts
         * at the boundary. This still yields a normalized weight vector (sum_i w_i = 1),
         * that is still uniformly distributed.
         */
        for (int m = 0; m < M; m++) {
            wNew[m] = (1 + M * xi) * wNew[m] - xi;
        }

        /** Compute new calculated intensity */
        intensity_ensemble_average(wt->nq, M, wNew, wt->bayesianEnsemble.I, wt->bayesianEnsemble.Ivar,
                wt->wd->I, wt->wd->varI, Icalc, IcalcVar);

        /** Compute new likelihood */
        double logLNew = bayesian_md_calc_logLikelihood(wr, t, Icalc, IcalcVar, tau);

        /** Relative probabilities = P[new]/P[old] */
        double pjointRel = exp(logLNew - logL);

        /** Add contribution to P[new]/P[old] from umbrella potential on weights */
        if (inputParams.ensemble_weights_fc > 0) {
            double deltaVweights = 0;

            /** We sum over M-1 states only, since the M'th state is fixed by the other M-1 states anyway */

            for (int m = 0; m < M - 1; m++) {
                /** Change in umbrella potential fc / 2 * (w - w0)^2 on weights */
                deltaVweights += gmx::square(wNew[m] - inputParams.ensemble_weights_init[m])
                        - gmx::square(wr->ensemble_weights[m] - inputParams.ensemble_weights_init[m]);
            }

            deltaVweights *= 0.5 * inputParams.ensemble_weights_fc;
            pjointRel *= exp(-deltaVweights / inputParams.kT);
        }

        gmx_bool bAccept;

        /** Accept according to Metropolis criterium */
        if (pjointRel > 1) {
            bAccept = TRUE;
        } else {
            double r = uniform_distribution(*(wr->rng));
            bAccept = (r < pjointRel);
        }

        if (bAccept) {
            logL = logLNew;
            memcpy(wr->ensemble_weights, wNew, M * sizeof(double));
            nAccept++;
        }

        if ((i % GIBBS_NST_OUTPUT) == 0) {
            FILE* fp = wr->swaxsOutput->fpGibbsEnsW[t];

            fprintf(fp, "%g ", simtime);

            for (int m = 0; m < M; m++) {
                fprintf(fp, " %g", wr->ensemble_weights[m]);
            }

            fprintf(fp, "\n");
        }

        // printf("\tGibbs weights: %g %g | %g - %d | w =", logL, logLNew, pjointRel, bAccept);
        //
        // for (int m = 0; m < M; m++) {
        //     printf(" %g", wr->ensemble_weights[m]);
        // }
        //
        // printf("\n");
        // printf("\tTry was: ");
        //
        // for (int m = 0; m < M; m++) {
        //     printf(" %g", wNew[m]);
        // }
        //
        // printf("\n");
    }

    sfree(tau);
    sfree(Icalc);
    sfree(IcalcVar);
    sfree(wNew);
    sfree(R);

    return nAccept;
}

/** Specify number of Monte-Carlo Moves */
#define WAXS_GIBBS_N_MONTECARLO_MOVES 100  /** Buffer density only or ensemble only */
#define WAXS_GIBBS_BOTH_N_ROUNDS       20  /** Rounds of both density and ensemble */

void swaxs::bayesian_gibbs_sampling(t_waxsrec* wr, double simtime, const t_commrec* cr) {
    const auto& inputParams = wr->getInputParams();

    if (inputParams.nTypes > 1) {
        gmx_fatal(FARGS, "Do now allow more than one scattering type for a Bayesian treatment of\n"
                "systematic errors and/or for refining heterogeneous ensembles.");
    }

    wr->timing->start(wr->waxsStep, swaxs::Timing::Stage::Gibbs, *cr);

    gmx_bool bDoEnsmbleW = (inputParams.ensembleType == swaxs::EnsembleType::BayesianOneRefined);
    gmx_bool bDoSolvDens = inputParams.bBayesianSolvDensUncert;

    int t = 0;
    t_waxsrecType* wt = &wr->wt[t];

    if (wt->wd->I_errSolvDens == nullptr and bDoSolvDens) {
        gmx_incons("bayesian_gibbs_sampling(): I_errSolvDens not set, but requesting bDoSolvDens");
    }

    if (bDoSolvDens and inputParams.weightsType != ewaxsWeightsEXP_plus_CALC_plus_SOLVDENS) {
        gmx_fatal(FARGS, "Requested Bayesian sampling of solvent density uncertainty, but not using solvent\n"
                "density uncertiainty for potential and likelihood. Use waxs-weights = exp+calc+solvdens\n");
    }

    int nAcceptW, nAcceptEps;
    double relAcceptEps, relAcceptW;

    if (MASTER(cr)) {
        if (bDoSolvDens and !bDoEnsmbleW) {
            /** Gibbs sampling of the uncertainty of the buffer density */
            nAcceptEps = bayesian_sample_buffer_uncertainty(wr, simtime, wt->wd->I, wt->wd->varI, WAXS_GIBBS_N_MONTECARLO_MOVES);
            relAcceptEps = 1.0 * nAcceptEps / WAXS_GIBBS_N_MONTECARLO_MOVES;
            printf("Gibbs sampling, final bufEps = %12g - (acceptance rate %g %%)\n", wr->epsilon_buff, 100. * relAcceptEps);
        } else if (!bDoSolvDens and bDoEnsmbleW) {
            /** Gibbs sampling of only ensemble weights */
            nAcceptW = bayesian_sample_ensemble_weights(wr, simtime, WAXS_GIBBS_N_MONTECARLO_MOVES);
            relAcceptW = 1.0 * nAcceptW / WAXS_GIBBS_N_MONTECARLO_MOVES;
            printf("Gibbs sampling of ensemble weigths (acceptance rate %g %%). Final weights =", 100. * relAcceptW);

            for (int m = 0; m < inputParams.ensemble_nstates; m++) {
                printf(" %10g", wr->ensemble_weights[m]);
            }

            printf("\n");
        } else if (bDoSolvDens and bDoEnsmbleW) {
            /** Doing several rounds of Gibbs sampling of ensemble weights and buffer density uncertainty */
            nAcceptEps = 0;
            nAcceptW = 0;

            for (int i = 0; i < WAXS_GIBBS_BOTH_N_ROUNDS; i++) {
                /** Doing a few rounds of Gibbs sampling of the buffer uncertainty */
                intensity_ensemble_average(wt->nq, inputParams.ensemble_nstates, wr->ensemble_weights,
                        wt->bayesianEnsemble.I, wt->bayesianEnsemble.Ivar, wt->wd->I, wt->wd->varI,
                        wt->bayesianEnsemble.sum_I, wt->bayesianEnsemble.sum_Ivar);

                nAcceptEps += bayesian_sample_buffer_uncertainty(wr, simtime, wt->bayesianEnsemble.sum_I,
                        wt->bayesianEnsemble.sum_Ivar, WAXS_GIBBS_BOTH_N_ROUNDS);

                /** Doing a few rounds of Gibbs sampling on the weights of the states of the ensemble */
                nAcceptW += bayesian_sample_ensemble_weights(wr, simtime, WAXS_GIBBS_BOTH_N_ROUNDS);
            }

            relAcceptEps = 1.0 * nAcceptEps / (WAXS_GIBBS_BOTH_N_ROUNDS * WAXS_GIBBS_BOTH_N_ROUNDS);
            relAcceptW = 1.0 * nAcceptW / (WAXS_GIBBS_BOTH_N_ROUNDS * WAXS_GIBBS_BOTH_N_ROUNDS);

            printf("Gibbs sampling. Accept. rates: bufEps: %6.2f %%   Weights: %6.2f %%\n",
                    100. * relAcceptEps, 100. * relAcceptW);
            printf("Final bufEps = %10g      Final weights =", wr->epsilon_buff);

            for (int m = 0; m < inputParams.ensemble_nstates; m++) {
                printf(" %10g", wr->ensemble_weights[m]);
            }

            printf("\n");
        } else {
            gmx_incons("Inconsistency in bayesian_gibbs_sampling()");
        }

        if (bDoEnsmbleW) {
            /** Update the ensemble average with the new weights */
            intensity_ensemble_average(wt->nq, inputParams.ensemble_nstates, wr->ensemble_weights,
                    wt->bayesianEnsemble.I, wt->bayesianEnsemble.Ivar, wt->wd->I, wt->wd->varI,
                    wt->bayesianEnsemble.sum_I, wt->bayesianEnsemble.sum_Ivar);
        }
    }

    /** Broadcast final variables after Gibbs sampling */
    if (PAR(cr)) {
        if (bDoEnsmbleW) {
            gmx_bcast(inputParams.ensemble_nstates * sizeof(double), wr->ensemble_weights, cr->mpi_comm_mygroup);
            gmx_bcast(wt->nq * sizeof(double), wt->bayesianEnsemble.sum_I, cr->mpi_comm_mygroup);
            gmx_bcast(wt->nq * sizeof(double), wt->bayesianEnsemble.sum_Ivar, cr->mpi_comm_mygroup);
        }

        if (bDoSolvDens) {
            gmx_bcast(sizeof(double), &wr->epsilon_buff, cr->mpi_comm_mygroup);
        }
    }

    wr->timing->end(wr->waxsStep, swaxs::Timing::Stage::Gibbs, *cr);
}

void swaxs::bayesian_ensemble_average(const t_waxsrec* wr, t_waxsrecType* wt, const t_commrec* cr, const waxs_datablock* wd, int nabs) {
    const auto& inputParams = wr->getInputParams();

    if (MASTER(cr)) {
        /**
         * Compute ensemble-averaged Icalc and stddev of Icalc.
         * Note: weights are at present normalized - but let's be sure and normalize again.
         */
        double sum = 0.;

        for (int m = 0; m < inputParams.ensemble_nstates; m++) {
            sum += wr->ensemble_weights[m];
        }

        for (int i = 0; i < nabs; i++) {
            wt->bayesianEnsemble.sum_I[i] = 0;
            wt->bayesianEnsemble.sum_Ivar[i] = 0;

            /** Sum over fixed states */
            for (int m = 0; m < inputParams.ensemble_nstates - 1; m++) {
                wt->bayesianEnsemble.sum_I[i] += wr->ensemble_weights[m] * wt->bayesianEnsemble.I[m][i];
                wt->bayesianEnsemble.sum_Ivar[i] += wr->ensemble_weights[m] * wt->bayesianEnsemble.Ivar[m][i];
            }

            /** Add the state of the MD simulation */
            int m = inputParams.ensemble_nstates - 1;
            wt->bayesianEnsemble.sum_I[i] += wr->ensemble_weights[m] * wd->I[i];
            wt->bayesianEnsemble.sum_Ivar[i] += wr->ensemble_weights[m] * wd->varI[i];

            /** Normalize */
            wt->bayesianEnsemble.sum_I[i] /= sum;
            wt->bayesianEnsemble.sum_Ivar[i] /= sum;
        }
    }

    if (PAR(cr)) {
        gmx_bcast(nabs * sizeof(double), wt->bayesianEnsemble.sum_I, cr->mpi_comm_mygroup);
        gmx_bcast(nabs * sizeof(double), wt->bayesianEnsemble.sum_Ivar, cr->mpi_comm_mygroup);
    }
}
