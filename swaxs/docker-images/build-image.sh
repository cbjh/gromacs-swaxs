#!/usr/bin/env bash

set -e

SPACK_REF="v0.19.0"
GITLAB_PREFIX="registry.gitlab.com/cbjh/gromacs-swaxs"  # ${CI_REGISTRY_IMAGE}

if [ "$#" -lt 3 ]; then
    echo "example call: $0 22.04 \"gcc@8.5.0\" base"
    exit 2
fi

set -x

UBUNTU_VERSION="$1"  # "22.04", "20.04", "18.04"

SPACK_IMAGE_NAME="spack"
SPACK_IMAGE_TAG="${UBUNTU_VERSION}.spack-${SPACK_REF}"
SPACK_IMAGE="${GITLAB_PREFIX}/${SPACK_IMAGE_NAME}:${SPACK_IMAGE_TAG}"

if [[ "$3" == "base" ]]; then
  ( cd ${SPACK_IMAGE_NAME} \
      && docker build --build-arg UBUNTU_VERSION="${UBUNTU_VERSION}" --build-arg SPACK_REF="${SPACK_REF}" -t ${SPACK_IMAGE} . \
      && docker push ${SPACK_IMAGE} )

  exit 0
fi

COMPILER="$2"  # "gcc@12.2.0" "gcc@10.4.0" "gcc@8.5.0" "gcc@6.5.0" "gcc@4.9.4" "clang@13.0.1"

COMPILER_IMAGE_NAME="compiler"
COMPILER_IMAGE_TAG="${UBUNTU_VERSION}.spack-${SPACK_REF}.${COMPILER/@/-}"
COMPILER_IMAGE="${GITLAB_PREFIX}/${COMPILER_IMAGE_NAME}:${COMPILER_IMAGE_TAG}"

if [[ "$3" == "compiler" ]]; then
  if grep -q "clang" <<< "${COMPILER}"; then
    DOCKERFILE="clang.Dockerfile"
  else
    DOCKERFILE="Dockerfile"
  fi

  ( cd ${COMPILER_IMAGE_NAME} \
        && docker build --build-arg BASE_IMAGE=${SPACK_IMAGE} --build-arg COMPILER="${COMPILER}" -t "${COMPILER_IMAGE}" -f ${DOCKERFILE} . \
        && docker push ${COMPILER_IMAGE} )

  exit 0
fi

DEPENDENCIES_IMAGE_NAME="dependencies"
DEPENDENCIES_IMAGE_TAG="${UBUNTU_VERSION}.spack-${SPACK_REF}.${COMPILER/@/-}"
DEPENDENCIES_IMAGE="${GITLAB_PREFIX}/${DEPENDENCIES_IMAGE_NAME}:${COMPILER_IMAGE_TAG}"

if [[ "$3" == "dependencies" ]]; then
  if [[ "${COMPILER}" == "gcc@4.9.4" ]]; then
    DOCKERFILE="gcc4.Dockerfile"
  elif [[ "${COMPILER}" == "gcc@6.5.0" ]]; then
    DOCKERFILE="gcc6.Dockerfile"
  else
    DOCKERFILE="Dockerfile"
  fi

  ( cd ${DEPENDENCIES_IMAGE_NAME} \
        && docker build --build-arg BASE_IMAGE=${COMPILER_IMAGE} -t ${DEPENDENCIES_IMAGE} -f ${DOCKERFILE} . \
        && docker push ${DEPENDENCIES_IMAGE} )
fi
