# Development

## CUDA

Ubuntu 22.04

```shell
sudo apt install nvidia-cuda-dev nvidia-cuda-toolkit nvidia-utils-510 nvidia-driver-510 bc
```

## Build

Clean build (slow)

```bash
./build.sh
```

Rebuild quickly after small code changes

```bash
./rebuild.sh
```

## Test

Clones and runs tests

```bash
./tests.sh
```

## Updating test reference

An example how to update `GmxPreprocessTest` test reference

```bash
build/bin/gmxpreprocess-test -ref-data update-changed
```

# SWAXS code

Most of the SWAXS code is located at `src/waxs`. Unfortunatelly, multiple small modifications were necessary in the rest of the source code. Changes can be reviewed by comparing branches. Example tools appropriate for this task are CLion, Meld and GitLab.

For example, one can compare branch `release-2021.swaxs` from https://gitlab.com/cbjh/gromacs-swaxs with `release-2021` from https://gitlab.com/gromacs/gromacs.git.

# Continuous integration

GitLab is using `swaxs/gitlab-ci.yml` for pipeline configuration, because it is specified in [project's CI settings](https://gitlab.com/cbjh/gromacs-saxs/-/settings/ci_cd).

When editing `gitlab-ci.yml`, you should change `swaxs/gitlab-ci.yml` to `swaxs/gitlab-ci.yml@cbjh/gromacs-swaxs:branch-name` in [project's CI settings](https://gitlab.com/cbjh/gromacs-saxs/-/settings/ci_cd), otherwise your changes won't be used. Alternatively commit directly to the default branch.

# Debugging

## Command line example

```bash
gdb -ex=r --args swaxs/dist/bin/gmx check -f saxs_driven.xtc
```

## VSCode example

- Select `gmx` as a target on the status bar and build it.
- Look into `.vscode/launch.json`. Edit existing configuration entry or add a new one by analogy.
- Run edited or added configuration from _Run and debug_ panel.

## Parameters

To get correct `gmx` program parameters, working directory, environmental variables and interactive input add `-v` switch in `swaxs/tests.sh`. Exact commands will appear in the terminal.
